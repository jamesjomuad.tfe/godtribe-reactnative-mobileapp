# GodTribe Mobile

React Native Mobile application of [GodTribe](https://godtribe.com/) for both iOS and Android.

## Requirements

To run the project, please ensure you have the following installed:

- Long-term support version 14.18.0 or higher [Nodejs](https://nodejs.org/en/).
- Version 6.14.15 npm or higher.
- [Expo Cli](https://docs.expo.dev/), run;

```bash
npm install --global expo-cli
```

- [Yarn](https://classic.yarnpkg.com/lang/en/docs/install/#windows-stable), run;

```bash
npm install --global yarn
```

## Installation

Clone the repository and switch branch from master to dev

```bash
git checkout dev
```

Run the following

```bash
npm install
expo install
```

## Usage

Run the project using expo cli command

```bash
expo start
```

If you don't want to use expo cli and want to develop react natively eject fom it using

```bash
expo eject
```

## Contributing

For development stage, please use "dev" branch and not the "master" branch. All updates and code changes are in dev.
