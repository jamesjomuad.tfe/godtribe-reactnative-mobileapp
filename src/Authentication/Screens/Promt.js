import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { WebView } from "react-native-webview";

const Promt = ({ btnType }) => {
  return (
    <>
      {btnType == 1 ? (
        <WebView
          style={styles.container}
          source={{ uri: "https://www.godtribe.com/privacy-policy.html" }}
        />
      ) : (
        <WebView
          style={styles.container}
          source={{ uri: "https://www.godtribe.com/terms-and-condtions.html" }}
        />
      )}
    </>
  );
};

export default Promt;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
