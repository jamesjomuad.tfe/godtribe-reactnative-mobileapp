import { StatusBar } from "expo-status-bar";
import React from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Image,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import {
  Button,
  Subheading,
  Title,
  Dialog,
  Portal,
  Modal,
} from "react-native-paper";
import * as Colors from "../../Common/Styles/Colors";
import { windowHeight } from "../../Common/Utils/Dimentions";
import Promt from "./Promt";

const About = () => {
  const [visible, setVisible] = React.useState(false);
  const [btnType, setBtnType] = React.useState(1);

  const hideDialog = () => setVisible(false);
  const showDialog = (numType) => {
    setBtnType(numType);
    setVisible(true);
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView contentContainerStyle={styles.container}>
        <Portal>
          <Modal
            visible={visible}
            onDismiss={hideDialog}
            contentContainerStyle={styles.containerModal}
          >
            <Promt btnType={btnType} />
          </Modal>
        </Portal>
        <Image
          source={require("../../../assets/GT-logo.png")}
          style={styles.logo}
        />
        <Button
          mode="contained"
          compact={true}
          labelStyle={{ color: "white", fontWeight: "bold", fontSize: 20 }}
          style={{
            width: "30%",
            elevation: 0,
            alignSelf: "center",
            marginTop: -20,
          }}
        >
          BETA
        </Button>
        <Title
          style={{ textAlign: "center", color: "white", marginBottom: 40 }}
        >
          V1.3.4
        </Title>
        <TouchableOpacity onPress={() => showDialog(1)}>
          <Title
            style={{
              textAlign: "center",
              color: "white",
              fontSize: 18,
              marginBottom: 10,
            }}
          >
            Privacy Policy
          </Title>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => showDialog(2)}>
          <Title style={{ textAlign: "center", color: "white", fontSize: 18 }}>
            Terms and Conditions
          </Title>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
};

export default About;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  logo: {
    width: "90%",
    resizeMode: "contain",
    marginTop: "20%",
    alignSelf: "center",
  },
  containerModal: {
    flex: 1,
    margin: 20,
    backgroundColor: "white",
  },
});
