import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView,
  Linking,
  SafeAreaView,
  ImageBackground,
} from "react-native";
import {
  Button,
  IconButton,
  Checkbox,
  Paragraph,
  Surface,
  Divider,
  Title,
  Headline,
} from "react-native-paper";
import { windowHeight, windowWidth } from "../../Common/Utils/Dimentions";
import urls from "../../Common/api/urls";

import { useDispatch } from "react-redux";
import { SET_LOGGED, SET_AUTH, SET_USER } from "../../Common/Redux/Actions";
import { GET_AUTH, GET_CURRENT_USER } from "../../Common/api";
import { SAVE_AUTH_CACHE } from "../Cache";

import FormInput from "../../Common/Components/FormInput";
import { Colors } from "../../Common/Styles/Colors";

const LoginScreen = ({ navigation }) => {
  const dispatch = useDispatch();

  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [errorMsg, setErrorMsg] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const [checked, setChecked] = useState(true);

  const loadInBrowser = () => {
    Linking.openURL(`${urls.baseUrl}/signup/`).catch((err) =>
      console.error("Couldn't load page", err)
    );
  };

  const checkAuth = () => {
    console.log("\nLogin: attempting...");
    const getAuth = async () => {
      setIsLoading(true);

      console.log("Login: proceed to fetch Auth...");
      const auth = await GET_AUTH(email, password);
      /* const auth = await AXIOS_POST_USER_AUTH({
        username: email,
        password: password,
      }); */

      /* if (auth.failed) {
        setErrorMsg("The username or password you entered is incorrect.");
      }

      if (auth.success) {
        setErrorMsg(null);
        console.log(auth.success.data);
        const user = await AXIOS_GET_CURRENT_USER(auth.success.data);
        if (user.failed) {
          setErrorMsg("No user DATA");
        }

        if (user.success) {
          console.log(user.success.data);
        }
      } */

      if (auth.success) {
        console.log("Login: success:", auth.success);

        setErrorMsg(null);
        dispatch(SET_AUTH(auth.success));
        console.log("Login: proceed to fetch user...");

        const user = await GET_CURRENT_USER(auth.success);
        if (user.error) {
          if (user.error.message) {
            console.log(user.error.message);
            setErrorMsg(user.error.message);
          } else {
            console.log("LOGIN: INTERNAL SERVER ERROR!");
          }
        } else {
          if (user.success) {
            console.log(user.success);
            setErrorMsg(null);
            checked && SAVE_AUTH_CACHE(auth.success);
            dispatch(SET_LOGGED(true));
            dispatch(SET_USER(user.success));
          } else {
            setErrorMsg("Error fetching account data.");
          }
        }
      } else {
        if (auth.error.code) {
          console.log("Login: error", auth.error.code);
          setErrorMsg(auth.error.code);
        } else {
          console.log("Login: error", auth.error);
          setErrorMsg("Internal server error");
        }
      }

      setIsLoading(false);
      console.log("Login: Login process ended.\n");
    };

    getAuth();
  };

  const onLogin = () => {
    if ((email, password)) {
      setErrorMsg(null);
      checkAuth();
    } else {
      setErrorMsg("Please complete input fields!");
    }
  };

  useEffect(() => {
    if (errorMsg === "[jwt_auth] invalid_username") {
      setErrorMsg("Can't find username.");
    } else if (errorMsg == "[jwt_auth] incorrect_password") {
      setErrorMsg("The username or password you entered is incorrect.");
    }
  }, [errorMsg]);

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : null}
      style={{ flex: 1 }}
    >
      <View
        style={{
          flex: 1,
          justifyContent: "flex-start",
          backgroundColor: "#2c2c2c",
        }}
      >
        <ScrollView style={{ flex: 1, backgroundColor: Colors.PRIMARY_COLOR }}>
          <ImageBackground
            source={require("../../../assets/godtribe/istockphoto-182724649-170667a.jpg")}
            style={{ width: "100%", height: windowHeight * 0.9 }}
          >
            <Headline
              style={{
                color: "white",
                textAlign: "center",
                paddingHorizontal: 20,
                marginVertical: 50,
              }}
            >
              Welcome to Godtribe a{" "}
              <Headline style={{ fontWeight: "bold", color: "white" }}>
                Christian Community
              </Headline>{" "}
              to grow Godly men
            </Headline>
            <Surface
              style={{
                borderRadius: 5,
                elevation: 2,
                paddingHorizontal: 15,
                height: 400,
                margin: 10,
              }}
            >
              <Text></Text>
              <Image
                source={require("../../../assets/godtribe/gt-logo-orange.png")}
                style={{
                  resizeMode: "contain",
                  height: 60,
                  marginHorizontal: -20,
                }}
              />
              {errorMsg ? (
                <View
                  style={{ alignItems: "center", justifyContent: "center" }}
                >
                  <Text style={{ color: "orange" }}>{errorMsg}</Text>
                </View>
              ) : (
                <View
                  style={{ alignItems: "center", justifyContent: "center" }}
                >
                  <Text style={{ color: "orange" }}> </Text>
                </View>
              )}
              <FormInput
                withIcon={false}
                labelValue={email}
                onChangeText={(userEmail) => setEmail(userEmail)}
                placeholderText="Username"
                iconType="user"
                keyboardType="email-address"
                autoCapitalize="none"
                autoCorrect={false}
                isPass={false}
              />

              <FormInput
                withIcon={false}
                labelValue={password}
                onChangeText={(userPassword) => setPassword(userPassword)}
                placeholderText="Password"
                iconType="lock"
                isPass={true}
              />

              <TouchableOpacity
                onPress={() => setChecked(!checked)}
                style={{
                  with: "100%",
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <Checkbox
                  status={checked ? "checked" : "unchecked"}
                  onPress={() => {
                    setChecked(!checked);
                  }}
                  style={{ color: "white" }}
                />
                <Paragraph style={{ color: Colors.PRIMARY_COLOR }}>
                  REMEMBER ME
                </Paragraph>
              </TouchableOpacity>
              <Button
                style={styles.button}
                labelStyle={{
                  fontSize: 26,
                  paddingVertical: 0,
                  fontWeight: "bold",
                }}
                mode="contained"
                loading={isLoading}
                disabled={isLoading}
                onPress={() => !isLoading && onLogin()}
              >
                GO
              </Button>

              <Divider />
              <Text></Text>
              <Button
                style={[
                  styles.button,
                  {
                    backgroundColor: Colors.GTBLUE,
                    marginBottom: -20,
                    zIndex: 99,
                    marginHorizontal: -15,
                    elevation: 3,
                    borderTopLeftRadius: 0,
                    borderTopRightRadius: 0,
                  },
                ]}
                labelStyle={{
                  fontSize: 26,
                  paddingVertical: 5,
                  fontWeight: "bold",
                }}
                mode="contained"
                disabled={isLoading}
                onPress={loadInBrowser}
              >
                SIGN UP
              </Button>
            </Surface>
          </ImageBackground>
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Paragraph style={{ color: "white" }}>
              I WANT TO KNOW MORE ABOUT DONATIONS
            </Paragraph>
            <Title style={{ color: "white" }}>I WANT TO LEAVE A LEGACY</Title>
          </View>
        </ScrollView>
      </View>
    </KeyboardAvoidingView>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    padding: 20,
    // backgroundColor: "#2c2c2c",
  },
  logo: {
    width: "90%",
    resizeMode: "contain",
    marginTop: "20%",
  },
  text: {
    fontSize: 28,
    marginBottom: 10,
    color: "#051d5f",
  },
  navButton: {
    marginTop: 15,
  },
  forgotButton: {
    marginVertical: 35,
  },
  navButtonText: {
    fontSize: 18,
    fontWeight: "500",
    color: "white",
  },
  loadingContainer: {
    position: "absolute",
    flex: 1,
    zIndex: 999,
    alignItems: "center",
    width: "100%",
    height: "100%",
    top: "32%",
  },
  button: {
    justifyContent: "center",
    elevation: 0,
    marginVertical: 10,
  },
});
