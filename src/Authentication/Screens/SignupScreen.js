/*
  as of the moment -> not used
*/

import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  KeyboardAvoidingView,
  Linking,
} from "react-native";
import { TextInput, Button } from "react-native-paper";
import * as Colors from "../../Common/Styles/Colors";
import { windowHeight, windowWidth } from "../../Common/Utils/Dimentions";
import { SIGN_UP } from "../../Common/api";

const SignupScreen = ({ navigation }) => {
  const [error, setError] = useState();
  const [success, setSuccess] = useState(false);
  const [aError, setAError] = useState();
  const [confirmPassword, setConfirm] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState({
    email: "",
    userName: "",
    password: "",
    firstName: "",
    lastName: "",
    nickname: "",
  });

  const test = {
    email: "johntrial02@gmail.com",
    firstName: "john",
    lastName: "john",
    nickname: "john",
    password: "123",
    userName: "john",
  };

  const nexProcess = () => {
    if (
      !data.email ||
      !data.userName ||
      !data.password ||
      !confirmPassword ||
      !data.firstName ||
      !data.lastName ||
      !data.nickname
    ) {
      /* console.log("Please complete input field."); */
      setError("Please complete input fields!");
      return false;
    } else {
      if (data.password == confirmPassword) {
        /* console.log("password does match."); */
        setError(null);
        return true;
      } else {
        console.log("password does not match.");
        setError("Password does not match!");
        return false;
      }
    }
  };

  const register = () => {
    const signIn = async () => {
      setIsLoading(true);
      const res = await SIGN_UP(data);
      console.log("[SignupScreen] ", res);
      if (res.message) {
        setAError(res.message);
      } else {
        setSuccess(true);
      }
      setIsLoading(false);
    };

    signIn();
  };

  useEffect(() => {
    if (success) {
      navigation.navigate("Login");
    }
  }, [success]);

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={{ flex: 1, paddingBottom: 10 }}
    >
      <ScrollView style={styles.container}>
        <Text style={styles.text}>Create an account</Text>

        {error ? (
          <Text style={{ color: Colors.ERROR_COLOR, alignSelf: "center" }}>
            {error}
          </Text>
        ) : aError ? (
          aError.map((err, index) => (
            <Text
              key={index}
              style={{ color: Colors.ERROR_COLOR, alignSelf: "center" }}
            >
              {err}
            </Text>
          ))
        ) : (
          <Text style={{ color: Colors.ERROR_COLOR, alignSelf: "center" }}>
            {" "}
          </Text>
        )}

        <View marginBottom={10}>
          <TextInput
            mode="outlined"
            value={data.firstName}
            onChangeText={(value) => setData({ ...data, firstName: value })}
            label="First Name"
            autoCapitalize="none"
            autoCorrect={false}
          />
        </View>

        <View marginBottom={10}>
          <TextInput
            mode="outlined"
            value={data.lastName}
            onChangeText={(value) => setData({ ...data, lastName: value })}
            label="Last Name"
            autoCapitalize="none"
            autoCorrect={false}
          />
        </View>

        <View marginBottom={10}>
          <TextInput
            mode="outlined"
            value={data.nickname}
            onChangeText={(value) => setData({ ...data, nickname: value })}
            label="Nickname"
            autoCapitalize="none"
            autoCorrect={false}
          />
        </View>

        <View marginBottom={10}>
          <TextInput
            mode="outlined"
            value={data.userName}
            onChangeText={(value) => setData({ ...data, userName: value })}
            label="User Name"
            autoCapitalize="none"
            autoCorrect={false}
          />
        </View>

        <View marginBottom={10}>
          <TextInput
            mode="outlined"
            value={data.email}
            onChangeText={(value) => setData({ ...data, email: value })}
            label="Email"
            keyboardType="email-address"
            autoCapitalize="none"
            autoCorrect={false}
          />
        </View>

        <View marginBottom={10}>
          <TextInput
            mode="outlined"
            value={data.password}
            onChangeText={(value) => setData({ ...data, password: value })}
            label="Password"
            secureTextEntry={true}
          />
        </View>

        <View marginBottom={10}>
          <TextInput
            mode="outlined"
            value={confirmPassword}
            onChangeText={(value) => setConfirm(value)}
            label="Confirm Password"
            secureTextEntry={true}
          />
        </View>

        <Button
          style={styles.button}
          loading={isLoading}
          mode="contained"
          labelStyle={{ fontSize: 18 }}
          onPress={() => {
            nexProcess() && register(test);
            /* navigation.navigate("SignupProfile", {
              email: data.email,
              username: data.userName,
              password: data.password,
            }); */
          }}
        >
          SIGN UP
        </Button>

        <TouchableOpacity
          style={styles.navButton}
          onPress={() => navigation.navigate("Login")}
        >
          <Text style={styles.navButtonText}>Have an account? Sign In</Text>
        </TouchableOpacity>

        <View style={{ paddingTop: "10%" }}></View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default SignupScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#f9fafd",
    flex: 1,
    padding: 20,
  },
  text: {
    fontSize: 28,
    marginBottom: 10,
    color: "#051d5f",
    alignSelf: "center",
  },
  navButton: {
    margin: 15,
    alignSelf: "center",
  },
  navButtonText: {
    fontSize: 18,
    fontWeight: "500",
    color: "#2e64e5",
  },
  textPrivate: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginVertical: 35,
    justifyContent: "center",
  },
  color_textPrivate: {
    fontSize: 13,
    fontWeight: "400",
    color: "grey",
  },
  button: {
    height: windowHeight / 15,
    justifyContent: "center",
    /* alignItems: "center", */
    elevation: 0,
    marginTop: 10,
    width: "100%",
  },
});
