import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Button } from "react-native-paper";

const TempStackScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Button
        style={styles.navButton}
        onPress={() => navigation.navigate("InboxScreen")}
        mode="outlined"
        icon="mail"
      >
        To Inbox
      </Button>
      <Button
        style={styles.navButton}
        mode="outlined"
        icon="chat"
        onPress={() => console.log("pressed to somewhere!")}
      >
        To Group Discussions
      </Button>
    </View>
  );
};

export default TempStackScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    padding: 20,
  },
  navButton: {
    paddingVertical: 5,
    marginBottom: 5,
  },
});
