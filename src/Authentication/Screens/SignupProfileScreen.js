/*
  as of the moment -> not used
*/

import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import { TextInput, Button } from "react-native-paper";
import * as Colors from "../../Common/Styles/Colors";
import { windowHeight, windowWidth } from "../../Common/Utils/Dimentions";

const SignupProfileScreen = ({ navigation }) => {
  const [about, setAbout] = useState();
  const [error, setError] = useState();
  const [data, setData] = useState({
    username: "" /* route.params.username, */,
    email: "" /* route.params.email, */,
    password: "" /* route.params.password, */,
    firstName: "",
    lastName: "",
    nickname: "",
  });

  /* "username": "johntrial02",
    "firstName": "John",
    "lastName": "Trial02",
    "email": "johntrial02@gmail.om",
    "password": "johntrial02",
    "nickname": "john 02" */

  const onSignUp = async () => {
    /* SIGN_UP(data); */
    navigation.navigate("Login");
  };

  return (
    <ScrollView backgroundColor={"#f9fafd"}>
      <View style={styles.container}>
        <Text style={styles.text}>Profile Details</Text>

        <View>
          <View marginBottom={10}>
            <TextInput
              mode="outlined"
              value={data.firstName}
              onChangeText={(value) => setData({ ...data, firstName: value })}
              label="First Name"
              autoCapitalize="none"
              autoCorrect={false}
            />
          </View>

          <View marginBottom={10}>
            <TextInput
              mode="outlined"
              value={data.lastName}
              onChangeText={(value) => setData({ ...data, lastName: value })}
              label="Last Name"
              autoCapitalize="none"
              autoCorrect={false}
            />
          </View>

          <View marginBottom={10}>
            <TextInput
              mode="outlined"
              value={data.nickname}
              onChangeText={(value) => setData({ ...data, nickname: value })}
              label="Nickname"
              autoCapitalize="none"
              autoCorrect={false}
            />
          </View>

          <View marginBottom={10}>
            <TextInput
              mode="outlined"
              value={about}
              onChangeText={(value) => setAbout(value)}
              label="About Me (optional)"
              autoCapitalize="none"
              multiline={true}
              numberOfLines={7}
              autoCorrect={false}
              height={150}
              txtVertical="top"
            />
          </View>
        </View>

        <Button
          style={styles.button}
          mode="contained"
          onPress={() => onSignUp()}
        >
          <Text style={{ fontSize: 18 }}>SIGN UP</Text>
        </Button>

        <View style={styles.textPrivate}>
          <Text style={styles.color_textPrivate}>
            By registering, you confirm that you accept our{" "}
          </Text>
          <TouchableOpacity onPress={() => alert("Terms Clicked!")}>
            <Text style={[styles.color_textPrivate, { color: "#e88832" }]}>
              Terms of service
            </Text>
          </TouchableOpacity>
          <Text style={styles.color_textPrivate}> and </Text>
          <Text style={[styles.color_textPrivate, { color: "#e88832" }]}>
            Privacy Policy
          </Text>
        </View>

        <TouchableOpacity
          style={styles.navButton}
          onPress={() => navigation.navigate("Login")}
        >
          <Text style={styles.navButtonText}>Have an account? Sign In</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

export default SignupProfileScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#f9fafd",
    flex: 1,
    padding: 20,
  },
  text: {
    fontSize: 28,
    marginBottom: 10,
    color: "#051d5f",
    alignSelf: "center",
  },
  navButton: {
    marginTop: 15,
    alignSelf: "center",
  },
  navButtonText: {
    fontSize: 18,
    fontWeight: "500",
    color: "#2e64e5",
  },
  textPrivate: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginVertical: 30,
    justifyContent: "center",
  },
  color_textPrivate: {
    fontSize: 13,
    fontWeight: "400",
    color: "grey",
  },
  button: {
    height: windowHeight / 15,
    justifyContent: "center",
    alignItems: "center",
    elevation: 0,
    marginTop: 10,
    width: "100%",
  },
});
