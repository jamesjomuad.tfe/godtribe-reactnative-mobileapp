import AsyncStorage from "@react-native-async-storage/async-storage";

export const SAVE_AUTH_CACHE = (data) => {
  let token = JSON.stringify(data);
  AsyncStorage.setItem("AuthToken", token)
    .then(() => {
      console.log("\nCache: token cached.\n");
    })
    .catch((error) => {
      console.log(error);
    });
};

export const GET_AUTH_CACHE = async () => {
  try {
    const value = await AsyncStorage.getItem("AuthToken");
    /* console.log("\nCache: retrieve cached token.\n"); */
    return value;
  } catch (error) {
    console.log(error);
  }
};

export const DELETE_AUTH_CACHE = async () => {
  try {
    const value = await AsyncStorage.removeItem("AuthToken");
    console.log("\nCache: token deleted.\n");
    return value;
  } catch (error) {
    console.log(error);
  }
};
