import React from "react";
import Routes from "./Routes";
import { DefaultTheme, Provider as PaperProvider } from "react-native-paper";
import { SafeAreaView, LogBox } from "react-native";

LogBox.ignoreLogs([
  'The "iframe" tag is a valid HTML element but is not handled by this library. You must register a custom renderer or plugin and make sure its content model is not set to "none". If you don\'t want this tag to be rendered, add it to "ignoredTags" prop array.',
]);

import { Provider } from "react-redux";
import store from "../../Common/Redux/store";

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: "#F55819",
    accent: "#F55819",
    colors: {
      text: "#fff",
    },
  },
};

// wrapping paper provider with redux provider to enable redux in paper components
const Providers = () => {
  return (
    <Provider store={store}>
      <PaperProvider theme={theme}>
        <Routes />
      </PaperProvider>
    </Provider>
  );
};

export default Providers;
