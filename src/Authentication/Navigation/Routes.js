import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, StatusBar, Image } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import * as Colors from "../../Common/Styles/Colors";
import { ActivityIndicator } from "react-native-paper";

import { useSelector, useDispatch } from "react-redux";
import { SET_AUTH, SET_LOGGED, SET_USER } from "../../Common/Redux/Actions";
import { GET_AUTH_CACHE } from "../Cache";
import { GET_CURRENT_USER } from "../../Common/api";

import AppStack from "./AppStack";
import AuthStack from "./AuthStack";
import Inbox from "../../SocialMedia/Screens/Inbox";
import {
  FETCH_USER_ACTIVITIES,
  FETCH_USER_DATA,
  FETCH_USER_GROUPS,
} from "../../Common/Redux/Actions/user";

const Routes = () => {
  const isLogged = useSelector((state) => state.loginState.isLogin);
  const auth = useSelector((state) => state.authState.currentAuth);
  const currentUser = useSelector((state) => state.userState.currentUser);
  const dispatch = useDispatch();

  /* const organizedGroup = useSelector((state) => state.userState.organizedGroup); */

  const [authorized, setAuthorize] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [errorMsg, setErrorMsg] = useState();

  useEffect(() => {
    const getAuthCache = async () => {
      setIsLoading(true);

      const data = await GET_AUTH_CACHE();
      if (data) {
        let auth = JSON.parse(data);
        dispatch(SET_AUTH(auth));
        const user = await GET_CURRENT_USER(auth);
        if (user.error) {
          console.log(user.error.message);
          setErrorMsg(user.error.message);
        } else {
          if (user.success) {
            setErrorMsg(null);
            dispatch(SET_LOGGED(true));
            dispatch(SET_USER(user.success));
            console.log(user.success);
          }
        }
      } else {
        console.log("Routes: No cache data...");
        dispatch(SET_LOGGED(false));
      }

      setIsLoading(false);
    };

    getAuthCache();
  }, []);

  useEffect(() => {
    if (currentUser) {
      dispatch(FETCH_USER_DATA(currentUser.id));
      dispatch(FETCH_USER_GROUPS(currentUser.id));
      dispatch(FETCH_USER_ACTIVITIES(currentUser.id));
    }
  }, [currentUser]);

  useEffect(() => {
    if (auth && currentUser && isLogged) {
      setIsLoading(false);
      setAuthorize(true);
    } else {
      setAuthorize(false);
    }
  }, [currentUser, auth, isLogged]);

  /* return (
    <>
      <StatusBar barStyle="reverse" />
      <AppStack />
    </>
  ); */

  if (isLoading) {
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: Colors.PRIMARY_COLOR,
        }}
      >
        <Image
          source={require("../../../assets/godtribe/SplashScreen02.png")}
          style={{ resizeMode: "contain" }}
        />
        <ActivityIndicator
          animating={true}
          size={50}
          color={Colors.ACCENT_COLOR}
          style={{ position: "absolute", zIndex: 999, top: "50%" }}
        />
      </View>
    );
  } else {
    if (authorized) {
      return (
        <>
          <StatusBar barStyle="reverse" />
          <AppStack />
        </>
      );
    } else {
      return (
        <NavigationContainer>
          <StatusBar hidden={true} />
          <AuthStack />
        </NavigationContainer>
      );
    }
  }
};

export default Routes;

const styles = StyleSheet.create({});
