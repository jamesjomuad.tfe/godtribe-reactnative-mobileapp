import React from "react";
import { View, SafeAreaView } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import FontAwesome from "react-native-vector-icons/FontAwesome";

import LoginScreen from "../Screens/LoginScreen";
import SignupScreen from "../Screens/SignupScreen";
import SignupProfileScreen from "../Screens/SignupProfileScreen";
import About from "../Screens/About";

const AppStack = createStackNavigator();

export default function App() {
  return (
    <AppStack.Navigator initialRouteName={"Login"}>
      <AppStack.Screen
        name="Login"
        component={LoginScreen}
        options={{ headerShown: false }}
      />
      <AppStack.Screen
        name="Signup"
        component={SignupScreen}
        options={({ navigation }) => ({
          title: "",
          headerStyle: {
            backgroundColor: "#f9fafd",
            shadowColor: "#f9fafd",
            elevation: 0,
          },
          headerLeft: () => (
            <View style={{ marginLeft: 10 }}>
              <FontAwesome.Button
                name="long-arrow-left"
                size={25}
                backgroundColor="#f9fafd"
                color="#333"
                onPress={() => navigation.navigate("Login")}
              />
            </View>
          ),
        })}
      />
      <AppStack.Screen
        name="SignupProfile"
        component={SignupProfileScreen}
        options={({ navigation }) => ({
          title: "",
          headerStyle: {
            backgroundColor: "#f9fafd",
            shadowColor: "#f9fafd",
            elevation: 0,
          },
          headerLeft: () => (
            <View style={{ marginLeft: 10 }}>
              <FontAwesome.Button
                name="long-arrow-left"
                size={25}
                backgroundColor="#f9fafd"
                color="#333"
                onPress={() => navigation.navigate("Signup")}
              />
            </View>
          ),
        })}
      />

      <AppStack.Screen
        name="About"
        component={About}
        options={({ navigation }) => ({
          title: "",
          headerStyle: {
            backgroundColor: "#2c2c2c",
            shadowColor: "#2c2c2c",
            elevation: 0,
          },
          headerLeft: () => (
            <View style={{ marginLeft: 10 }}>
              <FontAwesome.Button
                name="long-arrow-left"
                size={25}
                backgroundColor="#2c2c2c"
                color="white"
                onPress={() => navigation.navigate("Login")}
              />
            </View>
          ),
        })}
      />
    </AppStack.Navigator>
  );
}
