import React from "react";
import { View, SafeAreaView } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import * as Colors from "../../Common/Styles/Colors";

import ProfileScreen from "../../SocialMedia/Screens/ProfileScreen";
import AddPostScreen from "../../SocialMedia/Screens/AddPostScreen";
import AddCommentScreen from "../../SocialMedia/Screens/AddCommentScreen";
import ImageScreen from "../../SocialMedia/Screens/ImageScreen";
import AppDrawer from "./AppDrawer";
import ActiveRoomScreen from "../../Campfire/Screens/ActiveRoomScreen";
import ImageBrowserScreen from "../../SocialMedia/Screens/ImageBrowserScreen";
import FavoriteScreen from "../../SocialMedia/Screens/FavoriteScreen";
import CreateCampfire from "../../Campfire/Screens/CreateCampfire";
import MembersScreen from "../../Campfire/Screens/MembersScreen";
import Title from "../../Common/Components/Title";
import ArrowLeft from "../../Common/Components/ArrowLeft";
import Inbox from "../../SocialMedia/Screens/Inbox";
import ChatScreen from "../../SocialMedia/Screens/ChatScreen";
import TempStackScreen from "../Screens/TempStackScreen";
import OwnTab from "../../Campfire/Screens/OwnTab";
import GroupPage from "../../SocialMedia/Screens/GroupPage";
import Notification from "../../SocialMedia/Screens/Notifications";
import SwiperPagerButton from "../../SocialMedia/Screens/SwiperPagerButton";
import SearchScreen from "../../SocialMedia/Screens/SearchScreen";
import FriendScreen from "../../Campfire/Screens/FriendScreen";

const Stack = createStackNavigator();

const AppStack = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Main">
        <Stack.Screen
          name="TempStackScreen"
          component={TempStackScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Main"
          component={AppDrawer}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Profile"
          component={ProfileScreen}
          options={({ navigation }) => ({
            title: "Profile",
            headerTitleStyle: {
              color: "white",
            },
            headerStyle: {
              backgroundColor: Colors.PRIMARY_COLOR,
              shadowColor: Colors.PRIMARY_COLOR,
              elevation: 0,
            },
            headerLeft: () => <ArrowLeft onPress={() => navigation.goBack()} />,
          })}
        />
        <Stack.Screen
          name="AddPost"
          initialParams={{
            item: null,
            photos: null,
            group: null,
            userDetail: null,
          }}
          component={AddPostScreen}
          options={({ navigation }) => ({
            title: "Create Post",
            headerTitleStyle: {
              color: "white",
            },
            headerStyle: {
              backgroundColor: Colors.PRIMARY_COLOR,
              shadowColor: Colors.PRIMARY_COLOR,
              elevation: 0,
            },
            headerLeft: () => <ArrowLeft onPress={() => navigation.goBack()} />,
          })}
        />
        <Stack.Screen
          name="AddComment"
          component={AddCommentScreen}
          options={({ navigation }) => ({
            title: "Comments",
            headerTitleStyle: {
              color: "white",
            },
            headerStyle: {
              backgroundColor: Colors.PRIMARY_COLOR,
              shadowColor: Colors.PRIMARY_COLOR,
              elevation: 0,
            },
            headerLeft: () => <ArrowLeft onPress={() => navigation.goBack()} />,
          })}
        />
        <Stack.Screen
          name="Favorite"
          component={FavoriteScreen}
          options={({ navigation }) => ({
            title: "Likes",
            headerTitleStyle: {
              color: "white",
            },
            headerStyle: {
              backgroundColor: Colors.PRIMARY_COLOR,
              shadowColor: Colors.PRIMARY_COLOR,
              elevation: 0,
            },
            headerLeft: () => (
              <ArrowLeft onPress={() => navigation.navigate("Main")} />
            ),
          })}
        />
        <Stack.Screen
          name="ImageBrowserScreen"
          component={ImageBrowserScreen}
          initialParams={{ item: null }}
          options={({ navigation }) => ({
            title: "",
            headerTitleStyle: {
              color: "white",
            },
            headerStyle: {
              backgroundColor: Colors.PRIMARY_COLOR,
              shadowColor: Colors.PRIMARY_COLOR,
              elevation: 0,
            },
            headerLeft: () => (
              <ArrowLeft
                onPress={() => navigation.navigate("AddPost", { photos: null })}
              />
            ),
          })}
        />
        <Stack.Screen
          name="ImageScreen"
          component={ImageScreen}
          initialParams={{ images: null, item: null }}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="ActiveRoom"
          component={ActiveRoomScreen}
          /* options={{ headerShown: false }} */

          options={({ navigation }) => ({
            title: "",
            headerStyle: {
              backgroundColor: Colors.PRIMARY_COLOR,
              shadowColor: Colors.PRIMARY_COLOR,
              elevation: 0,
            },
            headerLeft: () => (
              <ArrowLeft onPress={() => navigation.replace("Main")} />
            ),
            /* headerTitle: () => (
              <Title
                onPress={() => navigation.replace("Main")}
                title={"Active Room"}
              />
            ), */
          })}
        />

        <Stack.Screen
          name="CreateCampfire"
          component={CreateCampfire}
          initialParams={{ invites: [] }}
          options={({ navigation }) => ({
            title: "Create Campfire",
            headerTitleAlign: "center",
            headerTitleStyle: {
              color: "white",
            },
            headerStyle: {
              backgroundColor: Colors.PRIMARY_COLOR,
              shadowColor: Colors.PRIMARY_COLOR,
              elevation: 0,
            },
            headerLeft: () => (
              <View style={{ marginLeft: 10 }}>
                <FontAwesome.Button
                  name="long-arrow-left"
                  size={25}
                  backgroundColor={Colors.PRIMARY_COLOR}
                  color={"white"}
                  onPress={() => navigation.goBack()}
                />
              </View>
            ),
          })}
        />

        <Stack.Screen
          name="Members"
          component={MembersScreen}
          options={({ navigation }) => ({
            title: "Members",
            headerTitleAlign: "center",
            headerTitleStyle: {
              color: "white",
            },
            headerStyle: {
              backgroundColor: Colors.PRIMARY_COLOR,
              shadowColor: Colors.PRIMARY_COLOR,
              elevation: 0,
            },
            headerLeft: () => (
              <View style={{ marginLeft: 10 }}>
                <FontAwesome.Button
                  name="long-arrow-left"
                  size={25}
                  backgroundColor={Colors.PRIMARY_COLOR}
                  color="white"
                  onPress={() => navigation.goBack("Main")}
                />
              </View>
            ),
          })}
        />

        <Stack.Screen
          name="OwnTab"
          component={OwnTab}
          options={({ navigation }) => ({
            title: "Own Campfires",
            headerTitleAlign: "center",
            headerTitleStyle: {
              color: "white",
            },
            headerStyle: {
              backgroundColor: Colors.PRIMARY_COLOR,
              shadowColor: Colors.PRIMARY_COLOR,
              elevation: 0,
            },
          })}
        />

        <Stack.Screen
          name="InboxScreen"
          component={Inbox}
          options={({ navigation }) => ({
            title: "Inbox",
            headerTitleStyle: {
              color: "white",
            },
            headerStyle: {
              backgroundColor: Colors.PRIMARY_COLOR,
              shadowColor: Colors.PRIMARY_COLOR,
              elevation: 0,
            },
            headerLeft: () => (
              <ArrowLeft onPress={() => navigation.goBack("TempStackScreen")} />
            ),
          })}
        />
        <Stack.Screen
          name="ChatScreen"
          initialParams={{
            chat: null,
            name: null,
            userData: null,
          }}
          component={ChatScreen}
          options={({ navigation }) => ({
            title: "Chat",
            headerTitleStyle: {
              color: "white",
            },
            headerStyle: {
              backgroundColor: Colors.PRIMARY_COLOR,
              shadowColor: Colors.PRIMARY_COLOR,
              elevation: 0,
            },
            headerLeft: () => (
              <ArrowLeft onPress={() => navigation.goBack("InboxScreen")} />
            ),
          })}
        />

        <Stack.Screen
          name="GroupPage"
          initialParams={{ groupDetail: null }}
          component={GroupPage}
          options={({ navigation }) => ({
            title: "",
            headerTitleStyle: {
              color: "white",
            },
            headerStyle: {
              backgroundColor: Colors.PRIMARY_COLOR,
              shadowColor: Colors.PRIMARY_COLOR,
              elevation: 0,
            },
            headerLeft: () => <ArrowLeft onPress={() => navigation.goBack()} />,
          })}
        />

        <Stack.Screen
          name="Notification"
          component={Notification}
          options={({ navigation }) => ({
            title: "Notification",
            headerTitleStyle: {
              color: "white",
            },
            headerStyle: {
              backgroundColor: Colors.PRIMARY_COLOR,
              shadowColor: Colors.PRIMARY_COLOR,
              elevation: 0,
            },
            headerLeft: () => <ArrowLeft onPress={() => navigation.goBack()} />,
          })}
        />

        <Stack.Screen
          name="SearchScreen"
          component={SearchScreen}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="FriendScreen"
          component={FriendScreen}
          options={({ navigation }) => ({
            title: "Friends",
            headerTitleStyle: {
              color: "white",
            },
            headerStyle: {
              backgroundColor: Colors.PRIMARY_COLOR,
              shadowColor: Colors.PRIMARY_COLOR,
              elevation: 0,
            },
            headerLeft: () => <ArrowLeft onPress={() => navigation.goBack()} />,
          })}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppStack;
