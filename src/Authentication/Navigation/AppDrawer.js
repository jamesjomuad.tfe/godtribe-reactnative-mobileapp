import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import HomeScreen from "../../SocialMedia/Screens/HomeScreen";
import MainTab from "../../Campfire/Screens/MainTab";
import { SafeAreaView } from "react-native";
import ListCampfireScreen from "../../Campfire/Screens/ListCampfireScreen";
import HeadTitle from "../../Common/Components/HeadTitle";
import GroupList from "../../SocialMedia/Screens/GroupList";

const Drawer = createDrawerNavigator();

// main screen for navigation and hosted with react navigation drawer
const AppDrawer = () => {
  return (
    <Drawer.Navigator
      initialRouteName="Campfire"
      edgeWidth={-100}
      screenOptions={{ swipeEnabled: false }}
    >
      <Drawer.Screen
        name="Campfire"
        component={ListCampfireScreen}
        options={{ headerShown: false }}
      />
      <Drawer.Screen
        name="Home"
        component={HomeScreen}
        options={{ headerShown: false }}
      />
      <Drawer.Screen
        name="Groups"
        component={GroupList}
        options={{ headerShown: false }}
      />
    </Drawer.Navigator>
  );
};

export default AppDrawer;
