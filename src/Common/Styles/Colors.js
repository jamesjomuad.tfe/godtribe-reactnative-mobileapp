export const PRIMARY_COLOR = "hsl(0, 0%, 17%)";
export const PRIMARY_COLOR_LIGHT = "hsl(0, 0%, 27%)";
export const PRIMARY_COLOR_LIGHTER = "hsl(0, 0%, 37%)";
export const PRIMARY_COLOR_DARK = "hsl(0, 0%, 12%)";
export const PRIMARY_COLOR_DARKER = "hsl(0, 0%, 7%)";
export const ACCENT_COLOR = "#F55819";
export const ACCENT_COLOR_DARK = "hsl(20, 91%, 40%)";
export const ACCENT_COLOR_DARKER = "hsl(15, 90%, 30%)";
export const ACCENT_COLOR_LIGHT = "hsl(22, 81%, 57%)";
export const ACCENT_COLOR_LIGHTER = "hsl(22, 71%, 77%)";
export const WHITE_COLOR_WHITE = "hsl(0, 0%, 0%)";
export const WHITE_COLOR = "hsl(225, 50%, 98%)";
export const WHITE_COLOR_DARK = "hsl(240, 47%, 97%)";
export const WHITE_COLOR_DARKER = "hsl(0, 0%, 80%)";
export const ERROR_COLOR = "hsl(0, 95%, 55%)";
export const WHITE01 = "#F4F4F2";
export const WHITE02 = "#E8E8E8";
export const WHITE03 = "#BBBFCA";
export const WHITE04 = "#495464";
export const BLUE00 = "#2e64e5";
export const BLUE02 = "#051d5f";

export const GRAY50 = "#FAFAFA";
export const GRAY100 = "#F5F5F5";
export const GRAY200 = "#EEEEEE";
export const GRAY300 = "#E0E0E0";
export const GRAY400 = "#BDBDBD";
export const GRAY500 = "#9E9E9E";
// 600 up need white text
export const GRAY600 = "#757575";
export const GRAY700 = "#616161";
export const GRAY800 = "#424242";
export const GRAY900 = "#212121";
export const GTBLUE = "#19B6F5";

export const Colors = {
  PRIMARY_COLOR: "hsl(0, 0%, 17%)",
  PRIMARY_COLOR_LIGHT: "hsl(0, 0%, 27%)",
  PRIMARY_COLOR_LIGHTER: "hsl(0, 0%, 37%)",
  PRIMARY_COLOR_DARK: "hsl(0, 0%, 12%)",
  PRIMARY_COLOR_DARKER: "hsl(0, 0%, 7%)",
  ACCENT_COLOR: "#F55819",
  ACCENT_COLOR_DARK: "hsl(20, 91%, 40%)",
  ACCENT_COLOR_DARKER: "hsl(15, 90%, 30%)",
  ACCENT_COLOR_LIGHT: "hsl(22, 81%, 57%)",
  ACCENT_COLOR_LIGHTER: "hsl(22, 71%, 77%)",
  WHITE_COLOR_WHITE: "hsl(0, 0%, 0%)",
  WHITE_COLOR: "hsl(225, 50%, 98%)",
  WHITE_COLOR_DARK: "hsl(240, 47%, 97%)",
  WHITE_COLOR_DARKER: "hsl(0, 0%, 80%)",
  ERROR_COLOR: "hsl(0, 95%, 55%)",
  WHITE01: "#F4F4F2",
  WHITE02: "#E8E8E8",
  WHITE03: "#BBBFCA",
  WHITE04: "#495464",
  BLUE02: "#051d5f",
  BLUE00: "#2e64e5",
  GRAY50: "#FAFAFA",
  GRAY100: "#F5F5F5",
  GRAY200: "#EEEEEE",
  GRAY300: "#E0E0E0",
  GRAY400: "#BDBDBD",
  GRAY500: "#9E9E9E",
  // 600 up need white text
  GRAY600: "#757575",
  GRAY900: "#212121",
  GRAY800: "#424242",
  GRAY700: "#616161",
  GTBLUE: "#19B6F5",
};
