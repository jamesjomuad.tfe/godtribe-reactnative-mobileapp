import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ScrollView,
  Platform,
} from "react-native";
import { Colors } from "./Colors";

export const generalStyle = StyleSheet.create({
  container: {
    backgroundColor: Colors.GRAY100,
    flex: 1,
  },
  wrapper: {
    flex: 1,
    paddingTop: Platform.OS === "ios" ? 50 : 0,
    backgroundColor: Colors.PRIMARY_COLOR,
  },
});
