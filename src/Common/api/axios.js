import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
import urls from "./urls";

const API = axios.create({
  baseURL: urls.baseUrl,
});

// API.interceptors.request.use((request) => {
//   if (AsyncStorage.getItem("AuthToken")) {
//     request.headers.Authorization = `Bearer ${
//       JSON.parse(AsyncStorage.getItem("AuthToken")).token
//     }`;
//   }
//   return request;
// });
API.interceptors.request.use(
  (config) => {
    config.timeout = 5000;
    // if (AsyncStorage.getItem("AuthToken")) {
    //   config.headers.Authorization = `Bearer ${
    //     JSON.parse(AsyncStorage.getItem("AuthToken")).token
    //   }`;
    // }
    //  config.headers.Authorization = null;
    config.headers["Content-Type"] = "application/json";
    config.headers.accept = "application/json";
    return config;
  },
  (error) => Promise.reject(error)
);

//related all to users
export const SIGN_UP = async (data) => API.post(urls.user.register, data);

export const GET_AUTH = async (data) => API.post(urls.token, data);

export const GET_CURRENT_USER = async (token) =>
  API.get(urls.user.current, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

/* export const FETCH_USER_DATA = async (id) => API.get(`${urls.user.data}/${id}`); */

export const GET_ACTIVITIES_BUDDYBOSS = async () =>
  API.get(urls.activity.getPost);

export const CREATE_POSTS = async (posts, token) =>
  API.post(urls.activity.getPost, posts);

export const UPLOAD_IMAGE = (file, token) =>
  API.post(urls.media, file, {
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "multipart/form-data",
    },
  })
    .then((res) => res.json())
    .then((response) => {
      return response;
    })
    .catch((error) => {
      console.log("errorimage", error);
    });
