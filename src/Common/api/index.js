import urls from "./urls";
import { GET_AUTH_CACHE } from "../../Authentication/Cache";
import { formatTimeURL } from "../Utils/Format";

//used in login
export const GET_AUTH = async (username, password) => {
  try {
    const response = await fetch(`${urls.baseUrl}${urls.token}`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: username,
        password: password,
      }),
    });
    const json = await response.json();
    if (json.token) {
      const data = {
        success: json,
      };
      return data;
    } else {
      const data = {
        error: json,
      };
      return data;
    }
  } catch (error) {
    const data = {
      error: error,
    };
    return data;
  }
};

//used in login
export const GET_CURRENT_USER = async (token) => {
  try {
    /* console.log(`${urls.baseUrl}${urls.user.current}${formatTimeURL(1)}`);
    console.log(`Bearer ${token.token}`); */
    const response = await fetch(
      `${urls.baseUrl}${urls.user.current}${formatTimeURL(1)}`,
      {
        method: "GET",
        headers: {
          Accept: "*/*",
          Authorization: `Bearer ${token.token}`,
        },
      }
    );
    const json = await response.json();

    console.log("data from api", json);

    if (json.id) {
      return (data = {
        success: json,
      });
    } else {
      return (data = {
        error: json,
      });
    }
  } catch (error) {
    const data = {
      error: error,
    };
    return data;
  }
};

export const GET_ACTIVITIES_BUDDYBOSS = async (page, token) => {
  try {
    const response = await fetch(
      `${urls.baseUrl}${urls.activity.getPost}?page=${page}${formatTimeURL(2)}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${token.token}`,
        },
      }
    );
    const json = await response.json();
    return json;
  } catch (error) {
    console.log("api call: error fetching activities...");
    console.log(error);
    return null;
  }
};

export const CREATE_POSTS = (posts, token) => {
  fetch(`${urls.baseUrl}${urls.activity.getPost}`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token.token}`,
    },
    body: JSON.stringify(posts),
  })
    .then((response) => response.json())
    .then((response) => {
      /* return console.log("new post", response); */
    })
    .catch((error) => {
      return console.log("errors", error);
    });
};

/* export const UPDATE_POSTS = (id, posts, token) => {
  fetch(`${urls.baseUrl}${urls.activity.getPost}/${id}`, {
    method: "PATCH",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token.token}`,
    },
    body: JSON.stringify(posts),
  })
    .then((response) => response.json())
    .then((response) => {
    })
    .catch((error) => {
      return console.log("errors", error);
    });
}; */

export const UPDATE_POSTS = async (id, posts, token) => {
  try {
    const res = await fetch(
      `${urls.baseUrl}${urls.activity.getPost}/${id}${formatTimeURL(1)}`,
      {
        method: "PATCH",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${token.token}`,
        },
        body: JSON.stringify(posts),
      }
    );

    return randomToTrigger();
  } catch (error) {
    console.log("Api error", error);
    return false;
  }
};

export const TRASH_IMAGE = async (imageId, token) => {
  try {
    const res = await fetch(
      `${urls.baseUrl}${urls.media}/${imageId}${formatTimeURL(1)}`,
      {
        method: "DELETE",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${token.token}`,
        },
      }
    );
    return randomToTrigger();
  } catch (error) {
    console.log("Api error", error);
    return false;
  }
};

export const SIGN_UP = async (data) => {
  try {
    /* console.log(
      `${urls.baseUrl}${urls.user.register}?username=${data.userName}&password=${data.password}&nickname=${data.nickname}&email=${data.email}&firstName=${data.firstName}&lastName=${data.lastName}`
    ); */
    const response = await fetch(
      `${urls.baseUrl}${urls.user.register}?username=${data.userName}&password=${data.password}&nickname=${data.nickname}&email=${data.email}&firstName=${data.firstName}&lastName=${data.lastName}`,
      {
        method: "POST",
        /* headers: {
        Accept: "application/json",
      } */
      }
    );
    const json = await response.json();
    return json;
  } catch (error) {
    console.log("[index api] error", error);
    return error;
  }
};

export const FETCH_USERS_DATA = async (id) => {
  try {
    const data = await GET_AUTH_CACHE();
    const token = await JSON.parse(data);
    /* console.log(token);
    console.log(`${urls.baseUrl}${urls.user.data}/${id}`); */
    const response = await fetch(
      `${urls.baseUrl}${urls.user.data}/${id}${formatTimeURL(1)}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${token.token}`,
        },
      }
    );
    const json = await response.json();
    return json;
  } catch (error) {
    console.log("[index api] error", error);
  }
};

export const UPLOAD_IMAGE = async (data, token) => {
  try {
    const response = await fetch(`${urls.baseUrl}${urls.media}`, {
      method: "POST",
      headers: {
        "Content-Type": "multipart/form-data; ",
        Authorization: `Bearer ${token.token}`,
        Accept: "application/json",
      },
      body: data,
    });
    const json = await response.json();
    if (json.url) {
      return json;
    } else {
      const data = {
        error: json,
      };
      return data;
    }
  } catch (error) {
    const data = {
      error: error,
    };
    return data;
  }
};

export const ONFAVORITE_POSTS = async (postID, token) => {
  try {
    const data = fetch(
      `${urls.baseUrl}${urls.activity.getPost}/${postID}/favorite`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${token.token}`,
        },
        body: JSON.stringify({ id: postID }),
      }
    );
  } catch (err) {}
};
export const GETCOMMENT_POSTS = async (postID, token) => {
  try {
    const response = await fetch(
      `${urls.baseUrl}${urls.activity.getPost}/${postID}/comment${formatTimeURL(
        1
      )}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${token.token}`,
        },
      }
    );
    const json = await response.json();
    return json.comments;
  } catch (err) {}
};

export const POST_COMMENT = async (postID, content, token) => {
  try {
    const res = await fetch(
      `${urls.baseUrl}${urls.activity.getPost}/${postID}/comment`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${token.token}`,
        },
        body: JSON.stringify({ content: content }),
      }
    );
    return randomToTrigger();
  } catch (err) {
    return false;
  }
};

export const POST_REPLY_COMMENT = async (postID, commentID, content, token) => {
  try {
    const res = await fetch(
      `${urls.baseUrl}${urls.activity.getPost}/${postID}/comment?parent_id=${commentID}`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${token.token}`,
        },
        body: JSON.stringify({ content: content }),
      }
    );
    return randomToTrigger();
  } catch (err) {
    return false;
  }
};

export const POST_COMMENT_DELETE = async (postID, token) => {
  try {
    fetch(`${urls.baseUrl}${urls.activity.getPost}/${postID}/comment`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token.token}`,
      },
      body: JSON.stringify({ content: content }),
    })
      .then((response) => {
        return randomToTrigger();
      })
      .catch((error) => {
        return false;
      });
  } catch (err) {
    return false;
  }
};

export const POST_DELETE = async (postID, token) => {
  try {
    const res = await fetch(
      `${urls.baseUrl}${urls.activity.getPost}/${postID}`,
      {
        method: "DELETE",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${token.token}`,
        },
      }
    );
    return randomToTrigger();
  } catch (err) {
    return false;
  }
};

const randomToTrigger = () => {
  return Math.floor(Math.random() * 100000000);
};

export const GET_GROUPS = async (userId, page) => {
  try {
    const data = await GET_AUTH_CACHE();
    const token = await JSON.parse(data);
    const response = await fetch(
      `${urls.baseUrl}${
        urls.group
      }?user_id=${userId}&page=${page}&per_page=50${formatTimeURL(2)}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${token.token}`,
        },
      }
    );
    const json = await response.json();
    return json;
  } catch (err) {
    return false;
  }
};

export const GET_MyACTIVITIES_BUDDYBOSS = async (id, page, scope) => {
  try {
    const data = await GET_AUTH_CACHE();
    const token = await JSON.parse(data);
    /* console.log(
      `${urls.baseUrl}${urls.activity.getPost}?user_id=${id}&page=${page}`
    ); */
    var apiUrl;
    if (scope) {
      apiUrl = `${urls.baseUrl}${
        urls.activity.getPost
      }?page=${page}&scope=${scope}${formatTimeURL(2)}`;
    } else {
      apiUrl = `${urls.baseUrl}${
        urls.activity.getPost
      }?user_id=${id}&page=${page}${formatTimeURL(2)}`;
    }
    /* console.log(apiUrl); */
    const response = await fetch(apiUrl, {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token.token}`,
      },
    });
    const json = await response.json();
    return json;
  } catch (error) {
    console.log("api call: error fetching activities...");
    console.log(error);
    return null;
  }
};

export const GET_ALL_GROUPS_BUDDYBOSS = async (page) => {
  try {
    const data = await GET_AUTH_CACHE();
    const token = await JSON.parse(data);
    const json = await fetch(
      `${urls.baseUrl}${urls.group}?page=${page}&per_page=${50}${formatTimeURL(
        2
      )}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${token.token}`,
        },
      }
    );
    const res = await json.json();

    if (typeof res === "object") {
      return { success: res };
    } else {
      return { failed: res };
    }
  } catch (error) {
    console.log(error);
    return { failed: null };
  }
};

export const GET_GROUPS_MEMBERS = async (id, page) => {
  try {
    const data = await GET_AUTH_CACHE();
    const token = await JSON.parse(data);
    const json = await fetch(
      `${urls.baseUrl}${
        urls.group
      }/${id}/members?page=${page}&per_page=${50}${formatTimeURL(2)}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${token.token}`,
        },
      }
    );
    const res = await json.json();

    if (typeof res === "object") {
      return { success: res };
    } else {
      return { failed: res };
    }
  } catch (error) {
    console.log(error);
    return { failed: null };
  }
};

export const GET_GROUP_ACTIVITIES_BUDDYBOSS = async (id, page) => {
  try {
    const data = await GET_AUTH_CACHE();
    const token = await JSON.parse(data);
    const response = await fetch(
      `${urls.baseUrl}${
        urls.activity.getPost
      }?group_id=${id}&page=${page}${formatTimeURL(2)}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${token.token}`,
        },
      }
    );
    const json = await response.json();
    if (typeof json === "object") {
      return { success: json };
    } else {
      return { failed: json };
    }
  } catch (error) {
    console.log("api call: error fetching activities...");
    console.log(error);
    return { failed: error };
  }
};

export const POST_JOIN = async (id, role, group_id) => {
  try {
    const data = await GET_AUTH_CACHE();
    const token = await JSON.parse(data);
    /* console.log(
      `${urls.baseUrl}${urls.group}/${group_id}/members?role=${role}&user_id=${id}`
    ); */
    const response = await fetch(
      `${urls.baseUrl}${urls.group}/${group_id}/members?role=${role}&user_id=${id}`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${token.token}`,
        },
      }
    );
    const json = await response.json();
    if (typeof json === "object") {
      return { success: json };
    } else {
      return { failed: json };
    }
  } catch (error) {
    console.log("api call: error fetching activities...");
    console.log(error);
    return { failed: error };
  }
};

export const POST_REQUEST_ACCESS = async (id, group_id, message) => {
  try {
    const data = await GET_AUTH_CACHE();
    const token = await JSON.parse(data);
    /* console.log(
      `${urls.baseUrl}${urls.requestAccess}?user_id=${id}&group_id=${group_id}&message=${message}`
    ); */
    const response = await fetch(
      `${urls.baseUrl}${urls.requestAccess}?user_id=${id}&group_id=${group_id}&message=${message}`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${token.token}`,
        },
      }
    );
    const json = await response.json();
    if (typeof json === "object") {
      return { success: json };
    } else {
      return { failed: json };
    }
  } catch (error) {
    console.log("api call: error fetching activities...");
    console.log(error);
    return { failed: error };
  }
};

export const GET_NOTIFICATION = async (id, page) => {
  try {
    const data = await GET_AUTH_CACHE();
    const token = await JSON.parse(data);
    /* console.log(
      `${urls.baseUrl}${urls.notif}?user_id=${id}&page${page}&per_page=100`
    ); */
    const response = await fetch(
      `${urls.baseUrl}${
        urls.notif
      }?user_id=${id}&page=${page}&per_page=100${formatTimeURL(2)}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${token.token}`,
        },
      }
    );
    const json = await response.json();
    if (typeof json === "object") {
      return { success: json };
    } else {
      return { failed: json };
    }
  } catch (error) {
    /* console.log("api call: error fetching activities...");
    console.log(error); */
    return { failed: error };
  }
};

export const GET_MESSAGES = async (id) => {
  try {
    const data = await GET_AUTH_CACHE();
    const token = await JSON.parse(data);
    var url;
    if (id) {
      url = `${urls.baseUrl}${urls.messages}/${id}${formatTimeURL(1)}`;
    } else {
      url = `${urls.baseUrl}${urls.messages}${formatTimeURL(1)}`;
    }
    const response = await fetch(url, {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token.token}`,
      },
    });
    const json = await response.json();
    if (typeof json === "object") {
      return { success: json };
    } else {
      return { failed: json };
    }
  } catch (error) {
    console.log("api call: error fetching messages...");
    console.log(error);
    return { failed: error };
  }
};

export const POST_MESSAGE = async (sendData) => {
  try {
    const data = await GET_AUTH_CACHE();
    const token = await JSON.parse(data);
    var apiUrl;
    if (sendData.id == null) {
      apiUrl = `${urls.baseUrl}${urls.messages}?message=${sendData.message}&sender_id=${sendData.sender_id}`;
    } else {
      apiUrl = `${urls.baseUrl}${urls.messages}?id=${sendData.id}&message=${sendData.message}&sender_id=${sendData.sender_id}`;
    }
    const response = await fetch(apiUrl, {
      method: "POST",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token.token}`,
      },
    });
    const json = await response.json();
    if (typeof json === "object") {
      return { success: json };
    } else {
      return { failed: json };
    }
  } catch (error) {
    console.log("api call: sending chat...");
    console.log(error);
    return { failed: error };
  }
};

export const GET_USER = async (id) => {
  try {
    const data = await GET_AUTH_CACHE();
    const token = await JSON.parse(data);
    const response = await fetch(
      `${urls.baseUrl}${urls.user.get}/${id}${formatTimeURL(1)}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${token.token}`,
        },
      }
    );
    const json = await response.json();
    if (typeof json === "object") {
      return { success: json };
    } else {
      return { failed: json };
    }
  } catch (error) {
    console.log("api call: error fetching messages...");
    console.log(error);
    return { failed: error };
  }
};

export const GET_USER_FRIENDS = async (id, page) => {
  try {
    const data = await GET_AUTH_CACHE();
    const token = await JSON.parse(data);
    const response = await fetch(
      `${urls.baseUrl}${
        urls.friend
      }?user_id=${id}&page=${page}&per_page=50${formatTimeURL(2)}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${token.token}`,
        },
      }
    );
    const json = await response.json();
    if (typeof json === "object") {
      return { success: json };
    } else {
      return { failed: json };
    }
  } catch (error) {
    console.log("api call: error fetching friends...");
    console.log(error);
    return { failed: error };
  }
};

export const POST_FRIENDSHIP = async (initiator_id, friend_id) => {
  try {
    const data = await GET_AUTH_CACHE();
    const token = await JSON.parse(data);
    const response = await fetch(
      `${urls.baseUrl}${urls.friend}?initiator_id=${initiator_id}&friend_id=${friend_id}`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${token.token}`,
        },
      }
    );
    const json = await response.json();
    if (typeof json === "object") {
      return { success: json };
    } else {
      return { failed: json };
    }
  } catch (error) {
    console.log("api call: error posting friendShips...");
    console.log(error);
    return { failed: error };
  }
};

export const UPDATE_FRIENDSHIP = async (id) => {
  try {
    const data = await GET_AUTH_CACHE();
    const token = await JSON.parse(data);
    const response = await fetch(`${urls.baseUrl}${urls.friend}/${id}`, {
      method: "PATCH",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token.token}`,
      },
    });
    const json = await response.json();
    if (typeof json === "object") {
      return { success: json };
    } else {
      return { failed: json };
    }
  } catch (error) {
    console.log("api call: error fetching friends...");
    console.log(error);
    return { failed: error };
  }
};

export const DELETE_FRIENDSHIP = async (id) => {
  try {
    const data = await GET_AUTH_CACHE();
    const token = await JSON.parse(data);
    const response = await fetch(
      `${urls.baseUrl}${urls.friend}?friend_id=${id}`,
      {
        method: "DELETE",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${token.token}`,
        },
      }
    );
    const json = await response.json();
    if (typeof json === "object") {
      return { success: json };
    } else {
      return { failed: json };
    }
  } catch (error) {
    console.log("api call: error deleting friends...");
    console.log(error);
    return { failed: error };
  }
};

export const CANCEL_FRIENDSHIP = async (id) => {
  try {
    const data = await GET_AUTH_CACHE();
    const token = await JSON.parse(data);
    const response = await fetch(`${urls.baseUrl}${urls.friend}/${id}`, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token.token}`,
      },
    });
    const json = await response.json();
    if (typeof json === "object") {
      return { success: json };
    } else {
      return { failed: json };
    }
  } catch (error) {
    console.log("api call: error deleting friends...");
    console.log(error);
    return { failed: error };
  }
};

export const FOLLOW_USER = async (id, action) => {
  try {
    const data = await GET_AUTH_CACHE();
    const token = await JSON.parse(data);
    const response = await fetch(
      `${urls.baseUrl}${urls.user.data}/action/${id}?action=${action}`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${token.token}`,
        },
      }
    );
    const json = await response.json();
    if (typeof json === "object") {
      return { success: json };
    } else {
      return { failed: json };
    }
  } catch (error) {
    console.log("api call: error deleting friends...");
    console.log(error);
    return { failed: error };
  }
};

export const GET_SEARCH_MEMBERS = async (searchString, page) => {
  try {
    const data = await GET_AUTH_CACHE();
    const token = await JSON.parse(data);
    const response = await fetch(
      `${urls.baseUrl}${
        urls.user.data
      }?per_page=20&page=${page}&search=${encodeURIComponent(
        searchString
      )}${formatTimeURL(2)}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${token.token}`,
        },
        /* body: JSON.stringify({
        per_page: 20,
        page: page,
        search: searchString,
      }), */
      }
    );
    const json = await response.json();
    if (typeof json === "object") {
      return { success: json };
    } else {
      return { failed: json };
    }
  } catch (error) {
    console.log("api call: error searching members...");
    console.log(error);
    return { failed: error };
  }
};

export const GET_SEARCH_ACTIVITIES = async (searchString, page) => {
  try {
    const data = await GET_AUTH_CACHE();
    const token = await JSON.parse(data);
    const response = await fetch(
      `${urls.baseUrl}${
        urls.activity.getPost
      }?per_page=20&page=${page}&search=${encodeURIComponent(
        searchString
      )}${formatTimeURL(2)}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${token.token}`,
        },
        /* body: JSON.stringify({
          per_page: 20,
          page: page,
          search: searchString,
        }), */
      }
    );
    const json = await response.json();
    if (typeof json === "object") {
      return { success: json };
    } else {
      return { failed: json };
    }
  } catch (error) {
    console.log("api call: error searching activities...");
    console.log(error);
    return { failed: error };
  }
};

export const GET_SEARCH_THREAD = async (user_id, recipient_id) => {
  try {
    const data = await GET_AUTH_CACHE();
    const token = await JSON.parse(data);
    const response = await fetch(
      `${urls.baseUrl}${
        urls.searchMessages
      }?user_id=${user_id}&recipient_id=${recipient_id}${formatTimeURL(2)}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${token.token}`,
        },
      }
    );
    const json = await response.json();
    if (typeof json === "object") {
      return { success: json };
    } else {
      return { failed: json };
    }
  } catch (error) {
    console.log("api call: error searching activities...");
    console.log(error);
    return { failed: error };
  }
};

export const POST_NEW_THREAD = async (message, recipients, sender_id) => {
  try {
    const data = await GET_AUTH_CACHE();
    const token = await JSON.parse(data);
    const response = await fetch(`${urls.baseUrl}${urls.messages}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${token.token}`,
      },
      body: JSON.stringify({
        message: message,
        recipients: recipients,
        sender_id: sender_id,
      }),
    });
    const json = await response.json();
    if (typeof json === "object") {
      return { success: json };
    } else {
      return { failed: json };
    }
  } catch (error) {
    console.log("api call: error searching activities...");
    console.log(error);
    return { failed: error };
  }
};
