import { API_GT_PROD, API_GT_DEV, API_CAMPFIRE } from "@env";

export default {
  baseUrl: API_GT_DEV,

  base: {
    dev: API_GT_DEV,
    prod: API_GT_PROD,
  },
  campfireBaseUrl: API_CAMPFIRE,
  token: "/wp-json/jwt-auth/v1/token",
  user: {
    current: "/wp-json/wp/v1/login",
    register: "/wp-json/wp/v1/user",
    get: "/wp-json/wp/v1/user",
    data: "/wp-json/buddyboss/v1/members",
  },
  activity: {
    getPost: "/wp-json/buddyboss/v1/activity",
  },
  media: "/wp-json/wp/v1/media",
  group: "/wp-json/buddyboss/v1/groups",
  requestAccess: "/wp-json/buddyboss/v1/groups/membership-requests",
  notif: "/wp-json/buddyboss/v1/notifications",
  messages: "/wp-json/buddyboss/v1/messages",
  friend: "/wp-json/buddyboss/v1/friends",
  searchMessages: "/wp-json/buddyboss/v1/messages/search-thread",
};
