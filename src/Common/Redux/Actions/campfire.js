import * as api from "../../../Campfire/api/axios";
import {
  CAMPFIRES_STATE_CHANGE,
  PUBLIC_ROOM_CHANGE,
  PRIVATE_ROOM_CHANGE,
  OWN_ROOM_CHANGE,
} from "../constants";

export const SET_CAMPFIRES = () => async (dispatch) => {
  try {
    const campfires = await api.GET_CAMPFIRES();
    if (campfires) {
      const notHiddenRoom = campfires.filter(
        (campfire) => campfire.hidden === false
      );

      if (notHiddenRoom.length > 0) {
        const publicRoom = notHiddenRoom.filter(
          (campfire) => campfire.openTo === "Everyone"
        );
        if (publicRoom.length > 0) {
          await dispatch({ type: PUBLIC_ROOM_CHANGE, payload: publicRoom });
        }

        const privateRoom = notHiddenRoom.filter(
          (campfire) => campfire.openTo === "Invite Only"
        );
        if (privateRoom.length > 0) {
          await dispatch({ type: PRIVATE_ROOM_CHANGE, payload: privateRoom });
        }
      }
    }
  } catch (error) {
    console.log("[ACTION] error", error);
  }
};

export const SET_OWN_CAMPFIRES = (uid) => async (dispatch) => {
  try {
    const campfires = await api.GET_OWN_CAMPFIRES(uid);
    if (campfires) {
      await dispatch({ type: OWN_ROOM_CHANGE, payload: campfires });
    }
  } catch (error) {
    console.log(error);
  }
};
