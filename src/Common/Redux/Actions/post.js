import * as api from "../../api/axios";
import {
  USER_POST_STATE_CHANGE,
  POST_CHANGE,
  COMMENT_CHANGE,
} from "../constants";
import { POST_DELETE } from "../../api";

export const createPosts = (data) => async (dispatch) => {
  try {
    const data = await api.CREATE_POSTS(data);
    await dispatch({ type: USER_POST_STATE_CHANGE, posts: data });
  } catch (error) {
    console.log(error);
  }
};
export const deletePost = (data) => async (dispatch) => {
  try {
    await dispatch({ type: POST_CHANGE, deleting: data });
  } catch (error) {
    console.log(error);
  }
};

export const refreshComment = (data) => async (dispatch) => {
  try {
    await dispatch({ type: COMMENT_CHANGE, status: data });
  } catch (error) {
    console.log(error);
  }
};
