import * as api from "../../api/axios";
import {
  FETCH_USERS_DATA,
  GET_GROUPS,
  GET_MyACTIVITIES_BUDDYBOSS,
} from "../../api";
import {
  LOGIN_STATE_CHANGE,
  AUTH_STATE_CHANGE,
  USER_STATE_CHANGE,
  USERDETAIL_STATE_CHANGE,
  GROUP_CHANGE,
  CLEAR_DATA,
  ORGANIZED_GROUP_CHANGE,
  MEMBERED_GROUP_CHANGE,
  USER_ACTIVITIES_STATE_CHANGE,
} from "../constants";

export const SET_LOGGED = (login) => async (dispatch) => {
  try {
    await dispatch({ type: LOGIN_STATE_CHANGE, isLogin: login });
  } catch (error) {}
};

// export const GET_AUTH = (data) => async (dispatch) => {
//   try {
//     dispatch({ type: AUTH_STATE_CHANGE, currentAuth: json });
//   } catch (error) {}
// };

export const SET_AUTH = (token) => async (dispatch) => {
  try {
    await dispatch({ type: AUTH_STATE_CHANGE, currentAuth: token });
  } catch (error) {}
};

export const GET_USER = () => async (dispatch) => {
  try {
    const data = await api.GET_USER();
    await dispatch({ type: USER_STATE_CHANGE, currentUser: data });
  } catch (error) {}
};

export const SET_USER = (user) => async (dispatch) => {
  try {
    await dispatch({ type: USER_STATE_CHANGE, currentUser: user });
  } catch (error) {}
};

export const FETCH_USER_DATA = (id) => async (dispatch) => {
  try {
    const data = await FETCH_USERS_DATA(id);
    await dispatch({
      type: USERDETAIL_STATE_CHANGE,
      userDetail: data,
    });
  } catch (err) {
    console.log("error action: fetchuseData", err);
  }
};

export const FETCH_USER_GROUPS = (id) => async (dispatch) => {
  try {
    const data = await GET_GROUPS(id, 1);
    if (data) {
      await dispatch({ type: GROUP_CHANGE, groups: data });

      const organizedGroup = data.filter((group) => group?.role == "Organizer");
      await dispatch({
        type: ORGANIZED_GROUP_CHANGE,
        organized: organizedGroup,
      });

      const memberedGroup = data.filter((group) => group?.role == "Member");
      await dispatch({ type: MEMBERED_GROUP_CHANGE, membered: memberedGroup });
    }
  } catch (err) {
    console.log("error action: FETCH_USER_GROUPS", err);
  }
};

export const FETCH_USER_ACTIVITIES = (id) => async (dispatch) => {
  try {
    const res = await GET_MyACTIVITIES_BUDDYBOSS(id, 1);
    await dispatch({ type: USER_ACTIVITIES_STATE_CHANGE, activities: res });
  } catch (err) {
    console.log("error action: fetchuser activities", err);
  }
};

/* export const WIPE_DATA = () => async (dispatch) => {
  try {
    await dispatch({ type: CLEAR_DATA });
  } catch (error) {}
}; */
