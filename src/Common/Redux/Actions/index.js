import {
  CAMPFIRES_STATE_CHANGE,
  USER_STATE_CHANGE,
  LOGIN_STATE_CHANGE,
  AUTH_STATE_CHANGE,
  TOKEN_STATE_CHANGE,
  CLEAR_DATA,
  USER_POST_STATE_CHANGE,
  USERDETAIL_STATE_CHANGE,
  USERS_FAVORITES_STATE_CHANGE,
  USERS_COMMENTS_STATE_CHANGE,
} from "../constants";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { GET_ACTIVITIES_BUDDYBOSS } from "../../api/axios";
import { FETCH_USERS_DATA } from "../../api";

/* export function WIPE_DATA() {
  return (dispatch) => {
    dispatch({ type: CLEAR_DATA });
  };
} */

export const WIPE_DATA = () => async (dispatch) => {
  try {
    await dispatch({ type: CLEAR_DATA });
  } catch (error) {}
};

export function SET_LOGGED(login) {
  console.log("action: changing log state - ", login);
  return (dispatch) => {
    dispatch({ type: LOGIN_STATE_CHANGE, isLogin: login });
  };
}

export function SET_AUTH(token) {
  return (dispatch) => {
    dispatch({ type: AUTH_STATE_CHANGE, currentAuth: token });
  };
}

export function SET_USER(user) {
  return (dispatch) => {
    dispatch({ type: USER_STATE_CHANGE, currentUser: user });
  };
}

export function GET_USER(token) {
  return (dispatch) => {
    fetch("https://staging.godtribe.com/wp-json/wp/v2/users/current", {
      method: "GET",
      headers: {
        Authorization: `${token.token_type} ${token.jwt_token}`,
      },
    })
      .then((response) => response.json())
      .then((json) => {
        let data = json[0];
        if (data === undefined || data === null) {
        } else {
          dispatch({ type: USER_STATE_CHANGE, currentUser: data });
        }
      })
      .catch((error) => {
        console.log("error getting user:", error);
      });
  };
}

export function GET_AUTH(username, password) {
  return (dispatch) => {
    fetch("https://staging.godtribe.com/wp-json/api/v1/token/", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: username,
        password: password,
      }),
    })
      .then((response) => response.json())
      .then((json) => {
        let token = JSON.stringify(json);
        AsyncStorage.setItem("AuthToken", token)
          .then(() => {
            dispatch({ type: AUTH_STATE_CHANGE, currentAuth: json });
          })
          .catch((error) => {
            console.log("actions_ error:", error);
          });
      })
      .catch((error) => {
        console.error(error);
      });
  };
}

export const createPosts = (data) => async (dispatch) => {
  try {
    await dispatch({ type: USER_POST_STATE_CHANGE, posts: data });
  } catch (err) {
    console.log("error action: fetchuseData", err);
  }
};

/* export const fetchUserData = (id) => async (dispatch) => {
  try {
    const data = await FETCH_USER_DATA(id);
    await dispatch({ type: USERDETAIL_STATE_CHANGE, userDetail: data });
  } catch (err) {
    console.log("error action: fetchuseData", err);
  }
}; */

export const onFavoritePost = (id) => async (dispatch) => {
  try {
    const data = await FETCH_USERS_DATA(id);
    await dispatch({ type: USERS_FAVORITES_STATE_CHANGE, userDetail: data });
  } catch (err) {
    console.log("error action: fetchuseData", err);
  }
};
