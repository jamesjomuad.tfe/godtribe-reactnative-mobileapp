import AsyncStorage from "@react-native-async-storage/async-storage";
import { DELETE_AUTH_CACHE } from "../../../Authentication/Cache";
import { AUTH_STATE_CHANGE, CLEAR_DATA } from "../constants";

const initialState = {
  currentAuth: null,
};

export const auth = (state = initialState, action) => {
  switch (action.type) {
    case AUTH_STATE_CHANGE:
      return {
        ...state,
        currentAuth: action.currentAuth,
      };
    case CLEAR_DATA:
      return initialState;
    default:
      return state;
  }
};
