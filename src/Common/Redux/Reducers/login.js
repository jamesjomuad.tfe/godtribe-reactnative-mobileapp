import {
  LOGIN_STATE_CHANGE,
  LOADING_STATE_CHANGE,
  ERROR_STATE_CHANGE,
  CLEAR_DATA,
} from "../constants";

const initialState = {
  isLogin: false,
  isLoading: true,
  isError: null,
};

export const login = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_STATE_CHANGE:
      return {
        ...state,
        isLogin: action.isLogin,
      };
    case LOADING_STATE_CHANGE:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case ERROR_STATE_CHANGE:
      return {
        ...state,
        isError: action.isError,
      };
    case CLEAR_DATA:
      return initialState;
    default:
      return state;
  }
};
