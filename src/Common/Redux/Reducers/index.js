import { combineReducers } from "redux";
/* import { campfires } from './reducers/campfires' */
import { user } from "./user";
import { login } from "./login";
import { auth } from "./auth";
import { campfire } from "./campfire";

const Reducers = combineReducers({
  loginState: login,
  authState: auth,
  userState: user,
  campfireState: campfire,
});

export default Reducers;
