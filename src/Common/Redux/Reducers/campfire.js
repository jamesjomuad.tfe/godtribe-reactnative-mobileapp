import {
  CAMPFIRES_STATE_CHANGE,
  PUBLIC_ROOM_CHANGE,
  PRIVATE_ROOM_CHANGE,
  OWN_ROOM_CHANGE,
} from "../constants";

const initialState = {
  campfires: [],
  privateRoom: [],
  publicRoom: [],
  ownRoom: null,
};

export const campfire = (state = initialState, action) => {
  switch (action.type) {
    case PUBLIC_ROOM_CHANGE:
      return {
        ...state,
        publicRoom: action.payload,
      };
    case PRIVATE_ROOM_CHANGE:
      return {
        ...state,
        privateRoom: action.payload,
      };
    case OWN_ROOM_CHANGE:
      return {
        ...state,
        ownRoom: action.payload,
      };
    default:
      return state;
  }
};
