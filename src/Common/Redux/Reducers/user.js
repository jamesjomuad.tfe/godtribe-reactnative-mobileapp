import AsyncStorage from "@react-native-async-storage/async-storage";
import {
  USER_STATE_CHANGE,
  CLEAR_DATA,
  USER_POST_STATE_CHANGE,
  USERDETAIL_STATE_CHANGE,
  USERS_FAVORITES_STATE_CHANGE,
  USERS_COMMENTS_STATE_CHANGE,
  POST_CHANGE,
  COMMENT_CHANGE,
  GROUP_CHANGE,
  ORGANIZED_GROUP_CHANGE,
  MEMBERED_GROUP_CHANGE,
  USER_ACTIVITIES_STATE_CHANGE,
} from "../constants";

const initialState = {
  currentUser: null,
  userDetail: null,
  posts: [],
  comments: [],
  favoritesCount: 0,
  deleting: null,
  commenting: null,
  userGroups: null,
  organizedGroup: [],
  memberedGroup: [],
  activities: [],
};

export const user = (state = initialState, action) => {
  switch (action.type) {
    case USER_STATE_CHANGE:
      return {
        ...state,
        currentUser: action.currentUser,
      };
    case USERDETAIL_STATE_CHANGE:
      return {
        ...state,
        userDetail: action.userDetail,
      };
    case USER_POST_STATE_CHANGE:
      return {
        ...state,
        posts: action.posts,
      };
    case USERS_FAVORITES_STATE_CHANGE:
      return {
        ...state,
        favoritesCount: action.favoritesCount,
      };

    case USERS_COMMENTS_STATE_CHANGE:
      return {
        ...state,
        comments: action.comments,
      };
    case POST_CHANGE:
      return {
        ...state,
        deleting: action.deleting,
      };
    case COMMENT_CHANGE:
      return {
        ...state,
        commenting: action.status,
      };
    case GROUP_CHANGE:
      return {
        ...state,
        userGroups: action.groups,
      };
    case ORGANIZED_GROUP_CHANGE:
      return {
        ...state,
        organizedGroup: action.organized,
      };
    case MEMBERED_GROUP_CHANGE:
      return {
        ...state,
        memberedGroup: action.membered,
      };
    case USER_ACTIVITIES_STATE_CHANGE:
      return {
        ...state,
        activities: action.activities,
      };
    case CLEAR_DATA:
      return initialState;
    default:
      return state;
  }
};
