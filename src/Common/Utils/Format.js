import { Platform } from "react-native";

export const capitalizeFirstLetter = (string) => {
  if (string != undefined || string != null) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  } else {
    return null;
  }
};

export const convertBoolean = (string) => {
  if (string) {
    switch (string?.toLowerCase().trim()) {
      case "true":
      case "yes":
      case "1":
        return true;

      case "false":
      case "no":
      case "0":
      case null:
        return false;

      default:
        return Boolean(string);
    }
  }
};

export const formatGroupRole = (string) => {
  if (string) {
    if (string === "Organizer") return `You're an Organizer`;
    if (string === "Member") return `You're a Member`;

    return null;
  } else {
    return null;
  }
};

export const formatTimeURL = (type) => {
  const r = Math.floor(Math.random() * 100000000);
  const t = new Date();
  const tt = t.toISOString();
  if (Platform.OS === "ios") {
    return "";
  } else {
    if (type == 1) {
      return `/?${tt}${r}`;
    } else {
      return `&${tt}${r}`;
    }
  }
};

export const m = (n, d) => {
  (x = ("" + n).length), (p = Math.pow), (d = p(10, d));
  x -= x % 3;
  return Math.round((n * d) / p(10, x)) / d + " kMGTPE"[x / 3];
};
