import crypto from "crypto-js";
import { REACT_APP_PASSPHRASE } from "@env";

export const cipherText = (txt) => {
  /* console.log(REACT_APP_PASSPHRASE); */
  const res = encodeURI(
    crypto.AES.encrypt(
      JSON.stringify(txt),
      REACT_APP_PASSPHRASE || ""
    ).toString()
  );

  return res;
};

export const decipherText = (txt) => {
  console.log(REACT_APP_PASSPHRASE);
  try {
    const bytes = crypto.AES.decrypt(
      decodeURI(txt),
      REACT_APP_PASSPHRASE || ""
    );
    /* console.log("bytes", bytes);
    console.log("bytes2", bytes.toString(crypto.enc.Utf8)); */
    const decryptedData = JSON.parse(bytes.toString(crypto.enc.Utf8));
    return decryptedData;
  } catch (error) {
    /* console.log(error); */
    return null;
  }
};
