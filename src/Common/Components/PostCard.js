import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  useWindowDimensions,
  TouchableOpacity,
  Platform,
  Alert,
} from "react-native";
import RenderHtml from "react-native-render-html";
import * as Colors from "../Styles/Colors";
import {
  Avatar,
  ActivityIndicator,
  Button,
  Menu,
  Divider,
  Dialog,
  Portal,
  Paragraph,
  IconButton,
  Surface,
} from "react-native-paper";
import moment from "moment";
import { useSelector, useDispatch } from "react-redux";
import PhotoGrid from "react-native-thumbnail-grid";
import Ionicons from "react-native-vector-icons/Ionicons";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { ONFAVORITE_POSTS, POST_DELETE } from "../api/index";
import { deletePost } from "../Redux/Actions/post";
import { windowWidth, windowHeight } from "../Utils/Dimentions";
import { WebView } from "react-native-webview";
import { Picker } from "@react-native-picker/picker";
import { UPDATE_POSTS } from "../api/index";
import ProfileImage from "./ProfileImage";
import { m } from "../Utils/Format";

const PostCard = ({
  item,
  commentPress,
  imgPress,
  deletePress,
  onDelete,
  viewProfile,
  editPress,
  feedLoad,
  hidden,
  fromSearch,
}) => {
  const [postImage, setPostImage] = useState();
  const auth = useSelector((state) => state.authState.currentAuth);
  const currentUser = useSelector((state) => state.userState.currentUser);
  const { width } = useWindowDimensions();
  const [isLiked, setIsLiked] = useState(item?.favorited);
  const [baseLiked, setBaseLiked] = useState(false);
  const [numLike, setNumLike] = useState(item?.favorite_count);
  const [likeText, setLikeText] = useState();
  const [likeIcon, setLikeIcon] = useState("thumb-up-outline");
  const [likeIconColor, setLikeIconColor] = useState("#333");
  const [iFramed, setIFramed] = useState();
  const [gif, setGif] = useState();
  const [toPassItem, setToPassItem] = useState();
  const [postPrivacy, setPostPrivacy] = useState(item.privacy);
  const [privacyButton, setPrivacyButton] = useState();

  const [post, setPost] = useState({
    user_id: currentUser?.id,
    bp_media_ids: [],
    type: "activity_update",
    component: "activity",
    content: "",
    privacy: "public",
  });

  const [dialogVisible, setDialogVisible] = useState(false);
  const showDialog = () => setDialogVisible(true);
  const hideDialog = () => setDialogVisible(false);

  const [selectedStatus, setSelectedStatus] = useState("public");
  const [visible, setVisible] = useState(false);
  const openMenu = () => setVisible(true);
  const closeMenu = () => setVisible(false);

  const updatePost = (updatePost) => {
    const savePosts = async (updatePost) => {
      try {
        const res = await UPDATE_POSTS(item.id, updatePost, auth);
      } catch (error) {
        console.log("[PostCard] update privacy error", error);
      }
    };
    savePosts(updatePost);
  };

  const changePrivacy = (title) => {
    if (title != postPrivacy) {
      let media_ids = [];
      if (item.bp_media_ids) {
        toEdit.bp_media_ids.map((item) => {
          media_ids.push(item.attachment_id);
        });
      }

      let updatedPrivacyPost = {
        ...post,
        user_id: item.user_id,
        content: item.content.rendered,
        privacy: title,
        bp_media_ids: media_ids,
      };

      updatePost(updatedPrivacyPost);
      setPostPrivacy(title);
    }
    setVisible(false);
  };

  const dispatch = useDispatch();

  let commentText = "";
  let sLikeNumber = item?.favorite_count;
  let baseIsLiked = item?.favorited;

  const numberTreatment = (num) => {
    return m(num, 1);
  };

  if (item.comment_count == 1) {
    commentText = "1 Comment";
  } else if (item.comment_count > 1) {
    let num = numberTreatment(item.comment_count);
    commentText = `${num} Comments`;
  } else {
    commentText = "Comment";
  }

  const postUserId = item.user_id;

  let postsSource;
  if (item.content) {
    postsSource = {
      html: `${item.content.rendered}`,
    };
  }

  const accountTitle = {
    html: `${item.title}`,
  };

  const settingText = (num) => {
    setNumLike(num);
    if (num == 1) {
      setLikeText("1 Like");
    } else if (num > 1) {
      let treatedNum = numberTreatment(num);
      setLikeText(`${treatedNum} Likes`);
    } else {
      setLikeText("Like");
    }
  };

  const onFavorite = async () => {
    if (!isLiked) {
      if (baseIsLiked) {
        settingText(parseInt(sLikeNumber));
      } else {
        settingText(parseInt(sLikeNumber) + 1);
      }
    } else {
      if (baseIsLiked) {
        settingText(parseInt(sLikeNumber) - 1);
      } else {
        settingText(parseInt(sLikeNumber));
      }
    }

    ONFAVORITE_POSTS(item.id, auth);

    setIsLiked(!isLiked);
  };

  /* useEffect(() => {
    setVidError(
      item.content_stripped &&
        item.content_stripped.includes("https://www.youtube.com")
    ) || item.content_stripped.includes("https://vimeo.com");
    console.log(
      (item.content_stripped &&
        item.content_stripped.includes("https://www.youtube.com")) ||
        item.content_stripped.includes("https://vimeo.com")
    );
  }, []); */
  /* if (item.content_stripped) {
    const res =
      item.content_stripped.includes("https://www.youtube.com") ||
      item.content_stripped.includes("https://vimeo.com");
    setVidError(res);
  } */

  useEffect(() => {
    if (isLiked) {
      setLikeIcon("thumb-up");
      setLikeIconColor("#2e64e5");
    } else {
      setLikeIcon("thumb-up-outline");
      setLikeIconColor(Colors.GRAY600);
    }
  }, [isLiked]);

  const deleting = () => {
    console.log("[PostCard] deleting", item.id);
    hideDialog();
    const deletingPost = async () => {
      feedLoad(true);
      try {
        const res = await POST_DELETE(item.id, auth);
        console.log(res);
        dispatch(deletePost(res));
        feedLoad(false);
      } catch (error) {
        console.log("[PostCart] deleting error", error);
        feedLoad(false);
      }
    };

    deletingPost();
  };

  useEffect(() => {
    postPrivacy == "public" && setPrivacyButton("earth");
    postPrivacy == "onlyme" && setPrivacyButton("lock");
  }, [postPrivacy]);

  useEffect(() => {
    if (item.bp_media_ids) {
      const img = item.bp_media_ids.map((value) => ({
        uri: value.attachment_data.full,
        data: value,
      }));

      setPostImage(img);

      /* const toPass = {
        images: img,
        postItem: item,
      };

      setToPassItem(toPass); */
    }

    settingText(sLikeNumber);

    if (postsSource) {
      if (postsSource.html.includes("iframe")) {
        let frame = postsSource.html.split('src="').pop().split('"')[0];
        setIFramed(frame);
      }
    }

    if (item.media_gif) {
      let frame = item.media_gif.rendered.split('src="').pop().split('"')[0];
      setGif(frame);
    }
  }, []);

  const postTime = moment.utc(item.date).local().startOf("seconds").fromNow();
  return (
    <Surface style={styles.card}>
      <Portal>
        <Dialog visible={dialogVisible} onDismiss={hideDialog}>
          <Dialog.Title>DELETE POST</Dialog.Title>
          <Dialog.Content>
            <Paragraph>Are you sure you want to delete this post?</Paragraph>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={hideDialog}>CANCEL</Button>
            <Button onPress={deleting}>YES</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
      <View style={styles.userInfo}>
        {item.user_avatar && item.user_avatar.thumb ? (
          <TouchableOpacity onPress={() => viewProfile(postUserId)}>
            <ProfileImage userData={item} />
          </TouchableOpacity>
        ) : (
          <Avatar.Text size={50} label="XD" style={styles.userImg} />
        )}
        <View style={[styles.userInfoText, { width: "85%" }]}>
          {item.title ? (
            <RenderHtml
              contentWidth={width}
              source={accountTitle}
              enableExperimentalMarginCollapsing={true}
              tagsStyles={tagsStyles.contentHead}
              renderersProps={renderersProps}
            />
          ) : item.name ? (
            <Text style={styles.userName}>{item.name}</Text>
          ) : (
            <Text style={styles.userName}>No Name</Text>
          )}
          <View
            style={{
              flexDirection: "row",
              flex: 1,
            }}
          >
            <View>
              <Text style={styles.postTime}>{postTime}</Text>
              <Text style={styles.postTime}>{postPrivacy}</Text>
            </View>
            {currentUser && currentUser?.id == item.user_id && (
              <Menu
                visible={visible}
                onDismiss={closeMenu}
                anchor={
                  <IconButton
                    onPress={openMenu}
                    icon={privacyButton}
                    color={Colors.GRAY500}
                    size={20}
                    style={{ padding: 0 }}
                  />
                }
              >
                <Menu.Item
                  icon="earth"
                  onPress={() => changePrivacy("public")}
                  title="public"
                />
                <Menu.Item
                  icon="lock"
                  onPress={() => changePrivacy("onlyme")}
                  title="onlyme"
                />
                {/* <Divider />
              <Menu.Item onPress={() => {}} title="Item 3" /> */}
              </Menu>
            )}
          </View>
        </View>
      </View>
      {/* {item.content_stripped &&
      (item.content_stripped.includes("https://www.youtube.com") ||
        item.content_stripped.includes("https://vimeo.com")) &&
      vidError ? (
        <WebView
          style={{ height: 300 }}
          source={{ uri: `${item.content_stripped}` }}
          onError={() => setVidError(false)}
        />
      ) : (
        <View style={styles.postText}>
          <RenderHtml
            ignoredTags={["iframe"]}
            contentWidth={width}
            source={postsSource}
            enableExperimentalMarginCollapsing={true}
          />
        </View>
      )} */}
      <View style={styles.postText}>
        <RenderHtml
          contentWidth={width}
          source={postsSource}
          enableExperimentalMarginCollapsing={true}
          tagsStyles={tagsStyles.contentRendered}
        />
      </View>

      {iFramed && (
        <WebView
          style={{ height: windowHeight / 3, backgroundColor: "#f8f8f8" }}
          source={{
            uri: `${iFramed}`,
          }}
        />
      )}

      {gif && (
        <WebView
          style={{
            height: windowHeight / 2,
            backgroundColor: "#f8f8f8",
            opacity: 0.99,
          }}
          useWebKit={true}
          originWhitelist={["*"]}
          allowsInlineMediaPlayback={true}
          javaScriptEnabledAndroid={true}
          javaScriptEnabled={true}
          source={{
            html: `<!DOCTYPE html><html><head><style>
            .video-container {
              position: absolute;
              top: 0;
              bottom: 0;
              width: 97%;
              height: auto; 
              overflow: hidden;
            }
            .video-container video {
              /* Make video to at least 100% wide and tall */
              min-width: 100%; 
              min-height: auto;
            
              /* Setting width & height to auto prevents the browser from stretching or squishing the video */
              width: auto;
              height: auto;
            
              /* Center the video */
              position: absolute;
              top: 50%;
              left: 50%;
              transform: translate(-50%,-50%);
            }
            </style>
            </head>
            <div class="video-container" >
            <body><video playsinline controls autoplay loop muted src="${gif}" ></video>
            </div>
            </body></html>`,
          }}
          scalesPageToFit={Platform.OS === "ios" ? false : true}
        />
      )}
      {/* html: `<video playsinline controls autoplay loop muted src="${gif}" ></video>`, */}

      <View style={{ marginVertical: 5 }}>
        {postImage ? (
          <PhotoGrid
            width={windowWidth}
            source={postImage}
            onPressImage={() => imgPress(postImage)}
          />
        ) : (
          <View style={styles.divider}></View>
        )}
      </View>
      <TouchableOpacity
        style={{ marginLeft: 15, flexDirection: "row" }}
        onPress={() => console.log("likes pressed")}
      >
        {numLike > 0 && (
          <Text style={styles.upperText}>
            {isLiked ? (numLike > 1 ? likeText : "You like this") : likeText}
          </Text>
        )}
        {item.comment_count > 0 && numLike > 0 && (
          <Text style={styles.upperText}> | </Text>
        )}
        {item.comment_count > 0 && (
          <Text style={styles.upperText}>{commentText}</Text>
        )}
      </TouchableOpacity>
      <View style={styles.interactionWrapper}>
        <TouchableOpacity style={styles.interaction} onPress={onFavorite}>
          <Icon name={likeIcon} size={20} style={{ color: likeIconColor }} />
          {/* <Text style={[styles.interactionText, { color: likeIconColor }]}>
            {isLiked ? "Unlike" : "Like"}
          </Text> */}
        </TouchableOpacity>
        <TouchableOpacity style={styles.interaction} onPress={commentPress}>
          <Icon
            name="comment-text-outline"
            size={20}
            style={{ color: Colors.GRAY600 }}
          />
          {/* <Text style={[styles.interactionText, { color: Colors.GRAY600 }]}>
            Comment
          </Text> */}
        </TouchableOpacity>
        {!hidden && currentUser && currentUser?.id == item.user_id && (
          <TouchableOpacity style={styles.interaction} onPress={editPress}>
            <Icon
              name="comment-edit-outline"
              size={20}
              style={{ color: Colors.GRAY600 }}
            ></Icon>
            {/* <Text style={[styles.interactionText, { color: Colors.GRAY600 }]}>
              Edit
            </Text> */}
          </TouchableOpacity>
        )}
        {!hidden && currentUser && currentUser?.id == item.user_id && (
          <TouchableOpacity
            style={styles.interaction}
            /* onPress={() => {
              deleting();
            }} */
            onPress={showDialog}
          >
            <Icon
              name="trash-can-outline"
              size={20}
              style={{ color: Colors.GRAY600 }}
            ></Icon>
            {/* <Text style={[styles.interactionText, , { color: Colors.GRAY600 }]}>
              Delete
            </Text> */}
          </TouchableOpacity>
        )}
        <View style={{ flex: 1 }}></View>
      </View>
    </Surface>
  );
};

export default PostCard;

const tagsStyles = {
  contentHead: {
    body: {
      whiteSpace: "normal",
      color: "gray",
    },
    a: {
      color: "black",
      fontWeight: "bold",
      textDecorationLine: "none",
    },
  },
  contentRendered: {
    blockquote: {
      backgroundColor: Colors.GRAY200,
      margin: 0,
      padding: 10,
      fontStyle: "italic",
      fontSize: 20,
    },
    p: {
      color: "black",
    },
  },
};

const renderersProps = {
  a: {
    onPress: onPress,
  },
};

function onPress(event, href) {}

const styles = StyleSheet.create({
  card: {
    width: "100%",
    backgroundColor: "white",
    marginBottom: 5,
    elevation: 3,
  },
  userInfo: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "flex-start",
    padding: 15,
  },
  userImg: {
    width: 50,
    height: 50,
    borderRadius: 5,
  },
  userInfoText: {
    flexDirection: "column",
    marginLeft: 10,
    marginRight: 15,
    width: windowWidth * 0.8,
  },
  userName: {
    fontSize: 14,
    fontWeight: "bold",
    paddingRight: 5,
  },
  postTime: {
    fontSize: 12,
    color: "#666",
  },
  postText: {
    paddingHorizontal: 15,
  },
  divider: {
    borderBottomColor: "#dddddd",
    borderBottomWidth: 1,
    width: "92%",
    alignSelf: "center",
  },
  interactionWrapper: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    paddingTop: 5,
    paddingBottom: 15,
    paddingLeft: 15,
  },
  interaction: {
    flexDirection: "row",
    justifyContent: "center",
    borderRadius: 2,
    paddingVertical: 2,
    paddingHorizontal: 15,
  },
  interactionText: {
    fontSize: 12,
    fontWeight: "bold",
    marginTop: 5,
    marginLeft: 5,
    textAlignVertical: "center",
  },
  activityType: {
    fontSize: 14,
    color: "#666",
    justifyContent: "flex-end",
  },
  upperText: {
    fontSize: 12,
    color: Colors.GRAY600,
  },
});
