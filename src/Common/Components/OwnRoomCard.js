import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import * as Colors from "../Styles/Colors";
import moment from "moment";
import PhotoGrid from "react-native-thumbnail-grid";

import {
  Button,
  TouchableRipple,
  Dialog,
  Portal,
  Paragraph,
  Badge,
} from "react-native-paper";
import { DELETE_CAMPFIRE, GET_CAMPFIRES_MEMBERS } from "../../Campfire/api";
import { useDispatch, useSelector } from "react-redux";
import { SET_CAMPFIRES, SET_OWN_CAMPFIRES } from "../Redux/Actions/campfire";
import { windowWidth } from "../Utils/Dimentions";
import ProfileImage from "./ProfileImage";

const OwnRoomCard = ({ room, joinRoom, refresh, onMember, closing }) => {
  const [members, setMembers] = useState([]);
  const [otherTxt, setOtherTxt] = useState();
  const [pendingMemberNum, setPendingMemberNum] = useState(null);
  const [users, setUsers] = useState();
  const [postImage, setPostImage] = useState();
  const [thisRoom, setThisRoom] = useState(room);
  const date = new Date(Date.parse(room.scheduleToStart));
  const roomTime = moment(date).calendar();
  const options = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
  };
  const stringDate = date.toLocaleDateString("en-US", options);
  const [apiMsg, setApiMsg] = useState();
  const [delLoading, setDelLoading] = useState();

  const imgWidth = windowWidth / 4;

  const [visible, setVisible] = React.useState(false);
  const showDialog = () => setVisible(true);
  const dialogCancel = () => setVisible(false);

  const currentUser = useSelector((state) => state.userState.currentUser);
  const dispatch = useDispatch();

  var myVar;

  function autoRefresh() {
    var tme = Math.floor(Math.random() * 1001) + 1000;
    myVar = setInterval(myTimer, tme);
  }

  function stopRefresh() {
    console.log("stopping refresh: ", thisRoom._id);
    clearInterval(myVar);
  }

  const callRefresh = () => {
    if (currentUser) {
      dispatch(SET_OWN_CAMPFIRES(currentUser.id));
    }
    dispatch(SET_CAMPFIRES());
  };

  const onDelete = () => {
    stopRefresh();
    setDelLoading(true);
    const letsDelete = async () => {
      try {
        const res = await DELETE_CAMPFIRE(thisRoom._id);
        if (res) {
          if (res.message) {
            setApiMsg(res.message);
            callRefresh();
          } else {
          }
        }
      } catch (error) {
        autoRefresh();
        console.log("[OwnRoom]", error);
      }
      setDelLoading(false);
    };

    setTimeout(letsDelete, 1000);
  };

  const dialogDone = () => {
    onDelete();
    setVisible(false);
  };

  function myTimer() {
    const getMembers = async () => {
      try {
        const res = await GET_CAMPFIRES_MEMBERS(thisRoom._id);
        if (res.length > 0) {
          setMembers(res);
        } else {
          setMembers([]);
        }
      } catch (error) {
        console.log("[OwnRoomCard]", error);
      }
    };
    getMembers();
  }

  useEffect(() => {
    if (closing) {
      stopRefresh();
    }
  }, [closing]);

  useEffect(() => {
    autoRefresh();
    return () => {
      stopRefresh();
    };
  }, []);

  useEffect(() => {
    if (members.length > 0) {
      const img = members.map((value) => ({
        uri: value.profileUrl,
        data: value,
      }));

      const pendingCount = members.filter((value) => value.status == "pending");

      if (pendingCount.length > 0) {
        setPendingMemberNum(pendingCount.length.toString());
      } else {
        setPendingMemberNum(null);
      }

      setPostImage(img);

      if (members.length == 1) {
        setOtherTxt("other");
      } else if (members.length > 1) {
        setOtherTxt("others");
      }
    } else {
      setPostImage(null);
      setOtherTxt(" ");
      setPendingMemberNum(null);
    }
  }, [members]);

  const transferMember = (currentRoom) => onMember(currentRoom);

  const memberPressed = () => {
    if (thisRoom) {
      let rm = thisRoom;
      transferMember(rm);
    }
  };

  return (
    <View style={styles.container}>
      <Portal>
        <Dialog visible={visible} onDismiss={dialogCancel}>
          <Dialog.Title>Warning!</Dialog.Title>
          <Dialog.Content>
            <Paragraph>
              Are you sure you want to delete campfire room with topic of{" "}
              <Text style={{ fontWeight: "bold" }}>{thisRoom.topic}</Text>{" "}
            </Paragraph>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={dialogCancel}>Cancel</Button>
            <Button onPress={dialogDone}>Confirm</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
      <View>
        <View style={styles.thumbnail}>
          <View style={styles.imgHolder}>
            {/* <Image
              source={{ uri: thisRoom.creator.profileUrl }}
              style={styles.img}
            /> */}
            <ProfileImage userData={room.creator} size={windowWidth / 4} />
          </View>
          {members.length > 0 && postImage && (
            <View style={styles.participants}>
              {/* <Text style={styles.count}>{room.members.length}</Text>
            <Text style={styles.txt}>{otherTxt}</Text> */}
              <PhotoGrid
                source={postImage}
                width={imgWidth}
                height={imgWidth}
                style={{ borderRadius: 5 }}
              />
            </View>
          )}
        </View>
        {members.length > 0 ? (
          <Text style={{ textAlign: "center" }}>
            <Text
              style={{ fontWeight: "bold" }}
            >{`${thisRoom.creator.name} `}</Text>
            <Text
              style={{ color: Colors.WHITE03 }}
            >{`and ${members.length} ${otherTxt}`}</Text>
          </Text>
        ) : (
          <Text
            style={{ textAlign: "center", fontWeight: "bold" }}
          >{`${thisRoom.creator.name}`}</Text>
        )}
      </View>
      <View style={styles.content}>
        <View
          style={{
            backgroundColor: "white",
            flexDirection: "row",
            justifyContent: "center",
          }}
        >
          <Text style={styles.topic} numberOfLines={2} ellipsizeMode="tail">
            {thisRoom.topic}
          </Text>
          <Text> </Text>
          {pendingMemberNum && (
            <Badge
              style={{ backgroundColor: Colors.ACCENT_COLOR, marginLeft: 10 }}
              size={20}
              style={{ alignSelf: "flex-start" }}
            >
              {pendingMemberNum}
            </Badge>
          )}
        </View>
        <Text style={[styles.details, { marginVertical: 5 }]}>
          {`SCHEDULE: ${roomTime}`}
        </Text>
        <ScrollView nestedScrollEnabled={true} style={styles.description}>
          <Text style={styles.details}>{thisRoom.description}</Text>
        </ScrollView>
      </View>

      <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
        <Button
          style={styles.btn}
          labelStyle={{ fontSize: 14, color: "white" }}
          onPress={() => joinRoom(thisRoom)}
          loading={false}
          icon="login"
        >
          Enter
        </Button>
        <Button
          style={styles.btn}
          labelStyle={{ fontSize: 14, color: "white" }}
          loading={false}
          icon="account-supervisor"
          onPress={memberPressed}
        >
          Members
        </Button>
        <Button
          style={styles.btn}
          labelStyle={{ fontSize: 14, color: "white" }}
          onPress={showDialog}
          loading={delLoading}
          icon="trash-can-outline"
        >
          Delete
        </Button>
      </View>
    </View>
  );
};

export default OwnRoomCard;

var styles = StyleSheet.create({
  container: {
    marginVertical: 5,
    backgroundColor: "white",
    paddingTop: 20,
    elevation: 1,
    borderRadius: 5,
  },
  thumbnail: {
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "center",
    backgroundColor: Colors.WHITE_COLOR,
    padding: 5,
    borderRadius: 5,
  },
  imgHolder: {
    width: windowWidth / 4,
    height: windowWidth / 4,
  },
  img: {
    resizeMode: "cover",
    width: "100%",
    height: "100%",
    borderRadius: 5,
  },
  participants: {
    justifyContent: "center",
    alignItems: "center",
    width: windowWidth / 4,
    height: windowWidth / 4,
    marginLeft: 5,
    borderRadius: 5,
  },
  count: {
    fontSize: 20,
    textAlign: "center",
    color: Colors.PRIMARY_COLOR_LIGHTER,
  },
  txt: {
    fontSize: 16,
    textAlign: "center",
    color: Colors.PRIMARY_COLOR_LIGHTER,
  },
  content: {
    padding: 15,
  },
  topic: {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 20,
  },
  details: {
    textAlign: "center",
  },
  description: {
    maxHeight: 95,
  },
  btn: {
    paddingVertical: 5,
    borderRadius: 0,
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  btnTxt: {
    fontSize: 18,
    color: "white",
  },
});
