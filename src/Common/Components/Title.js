import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";

const Title = ({ title, onPress }) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View style={{ flexDirection: "row" }}>
        <View style={{ justifyContent: "center", alignItems: "center" }}>
          <Image
            style={{ width: 50, height: 50, marginRight: 5 }}
            source={require("../../../assets/GT-icon.png")}
          />
        </View>
        <View style={{ justifyContent: "center" }}>
          <Text
            style={{
              color: "white",
              fontSize: 20,
              justifyContent: "flex-end",
            }}
          >
            <Text style={{ fontWeight: "bold" }}>GOD</Text>
            <Text>TRIBE</Text>
          </Text>
          <Text style={{ color: "white", justifyContent: "flex-start" }}>
            {title}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default Title;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
});
