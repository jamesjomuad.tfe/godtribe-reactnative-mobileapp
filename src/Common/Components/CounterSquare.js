import React from "react";
import { StyleSheet, Text, View } from "react-native";
import * as Colors from "../Styles/Colors";

const CounterSquare = ({ counter }) => {
  return (
    <Text
      style={{
        textAlign: "center",
        textAlignVertical: "center",
        color: "white",
        fontWeight: "bold",
        fontSize: 14,
        paddingVertical: 5,
        paddingHorizontal: 10,
        backgroundColor: Colors.ACCENT_COLOR,
        borderRadius: 5,
      }}
    >
      {counter}
    </Text>
  );
};

export default CounterSquare;

const styles = StyleSheet.create({});
