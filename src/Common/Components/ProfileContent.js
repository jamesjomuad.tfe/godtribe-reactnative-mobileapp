import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Button } from "react-native-paper";
import {
  GET_GROUPS,
  GET_MyACTIVITIES_BUDDYBOSS,
  GET_USER_FRIENDS,
} from "../api";
import * as Colors from "../Styles/Colors";
import { windowWidth } from "../Utils/Dimentions";
import AddPostCard from "./AddPostCard";
import FriendCard from "./FriendCard";
import GroupCard from "./GroupCard";
import PostCard from "./PostCard";
import ProfileDetails from "./ProfileDetails";

const ProfileContent = ({ contentKey, userDetail, isOwned, navigation }) => {
  const [activities, setActivities] = useState([]);
  const [page, setPage] = useState(0);
  const [page2, setPage2] = useState(0);
  const [page3, setPage3] = useState(0);
  const [yAxis, setYAxis] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [groups, setGroups] = useState([]);
  const [friends, setFriends] = useState([]);

  const getUserGroups = async (more) => {
    setIsLoading(true);
    try {
      const p = more ? page3 : 0;
      /* console.log("getting groups", userDetail.id); */
      const res = await GET_GROUPS(userDetail.id, p + 1);
      /* console.log("ProfileContent - getUserGroups", res); */
      if (res) {
        if (more) {
          let newGroups = groups.concat(res);
          setGroups(newGroups);
        } else {
          setGroups(res);
        }
        setPage3(p + 1);
      }
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  const getUserActivity = async (more) => {
    setIsLoading(true);
    /* console.log(userDetail); */
    if (userDetail.friendship_status == "is_friend") {
      try {
        const p = more ? page : 0;
        const acts = await GET_MyACTIVITIES_BUDDYBOSS(userDetail.id, p + 1);
        if (acts) {
          if (more) {
            let newActs = activities.concat(acts);
            setActivities(newActs);
          } else {
            setActivities(acts);
          }
          setPage(p + 1);
        }
      } catch (error) {
        console.log(error);
      }
    }

    if (isOwned) {
      try {
        const p = more ? page : 0;
        const acts = await GET_MyACTIVITIES_BUDDYBOSS(
          userDetail.id,
          p + 1,
          "just-me"
        );
        if (acts) {
          if (more) {
            let newActs = activities.concat(acts);
            setActivities(newActs);
          } else {
            setActivities(acts);
          }
          setPage(p + 1);
        }
      } catch (error) {
        console.log(error);
      }
    }
    setIsLoading(false);
  };

  const getUserFriends = async (more) => {
    setIsLoading(true);
    try {
      const p = more ? page2 : 0;
      const res = await GET_USER_FRIENDS(userDetail.id, p + 1);
      if (res.success) {
        if (more) {
          let newF = friends.concat(res.success);
          setFriends(newF);
        } else {
          setPage2(0);
          setFriends(res.success);
        }
        setPage2(p + 1);
      }
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  /*  useEffect(() => {
    console.log(activities.length);
  }, [activities]); */

  useEffect(() => {
    contentKey == "ACTIVITY" && getUserActivity(false);
    contentKey == "GROUPS" && getUserGroups(false);
    contentKey == "FRIENDS" && getUserFriends(false);
  }, []);

  return (
    <View style={[styles.card]}>
      {contentKey == "ACTIVITY" && (
        <View style={{ width: "100%" }}>
          <View style={{ width: "100%" }}>
            {isOwned && (
              <AddPostCard
                user={userDetail}
                titlePlaceholder={`Tell everyone what's on your mind`}
                addPost={() => navigation.navigate("AddPost")}
                viewProfile={() => console.log("Profile")}
              />
            )}
            {/*  <AddPostCard
              user={userDetail}
              titlePlaceholder={`Write something to ${userDetail.name}`}
              addPost={() =>
                navigation.navigate("AddPost", { userDetail: userDetail })
              }
              viewProfile={() => {}}
            /> */}
            {activities.length > 0 &&
              activities.map((item, index) => (
                <PostCard
                  key={index}
                  item={item}
                  commentPress={() =>
                    navigation.navigate("AddComment", { item })
                  }
                  imgPress={(images) =>
                    navigation.navigate("ImageScreen", {
                      images: images,
                      item,
                    })
                  }
                  onDelete={() => console.log("handleRefresh")}
                  viewProfile={(id) => navigation.navigate("Profile", { id })}
                  editPress={() => navigation.navigate("AddPost", { item })}
                  feedLoad={() => console.log("setIsRefresh")}
                />
              ))}
          </View>
          <View style={{ padding: 10, alignItems: "center", marginBottom: 50 }}>
            <Button
              mode="outlined"
              style={{
                borderColor: Colors.GTBLUE,
                borderRadius: 20,
                backgroundColor: "white",
              }}
              labelStyle={{ color: Colors.GTBLUE, fontSize: 14 }}
              loading={isLoading}
              disabled={isLoading}
              onPress={() => getUserActivity(true)}
            >
              {isLoading ? "LOADING..." : "LOAD MORE"}
            </Button>
          </View>
        </View>
      )}
      {contentKey == "PROFILE" && <ProfileDetails detail={userDetail} />}
      {contentKey == "GROUPS" && (
        <View>
          {groups.length > 0 &&
            groups.map((item, index) => (
              <GroupCard
                navigation={navigation}
                item={item}
                key={index}
                setAllGroups={getUserGroups}
              />
            ))}

          <View style={{ padding: 10, alignItems: "center", marginBottom: 50 }}>
            <Button
              mode="outlined"
              style={{
                borderColor: Colors.GTBLUE,
                borderRadius: 20,
                backgroundColor: "white",
              }}
              labelStyle={{ color: Colors.GTBLUE, fontSize: 14 }}
              loading={isLoading}
              disabled={isLoading}
              onPress={() => getUserGroups(true)}
            >
              {isLoading ? "LOADING..." : "LOAD MORE"}
            </Button>
          </View>
        </View>
      )}
      {contentKey == "FRIENDS" && (
        <View>
          {friends.length > 0 &&
            friends.map((friend, index) => (
              <FriendCard key={index} navigation={navigation} friend={friend} />
            ))}
          <View style={{ padding: 10, alignItems: "center", marginBottom: 50 }}>
            <Button
              mode="outlined"
              style={{
                borderColor: Colors.GTBLUE,
                borderRadius: 20,
                backgroundColor: "white",
              }}
              labelStyle={{ color: Colors.GTBLUE, fontSize: 14 }}
              loading={isLoading}
              disabled={isLoading}
              onPress={() => getUserFriends(true)}
            >
              {isLoading ? "LOADING..." : "LOAD MORE"}
            </Button>
          </View>
        </View>
      )}
      <View style={{ marginBottom: 50 }}></View>
    </View>
  );
};

export default ProfileContent;

const styles = StyleSheet.create({
  card: {
    width: windowWidth - 10,
    minHeight: 300,
    marginHorizontal: 5,
    borderRadius: 10,
  },
});
