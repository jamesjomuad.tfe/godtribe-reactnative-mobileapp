import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
} from "react-native";
import * as Colors from "../Styles/Colors";
import { IconButton, Badge } from "react-native-paper";
import { windowWidth } from "../Utils/Dimentions";
import { GET_NOTIFICATION } from "../api";
import { useSelector } from "react-redux";

var RefreshVar;

const HeadTitle = ({ onPress, active, title, navigation }) => {
  const currentUser = useSelector((state) => state.userState.currentUser);
  const [notifications, setNotifications] = useState([]);

  const getNotifs = async () => {
    try {
      const res = await GET_NOTIFICATION(currentUser.id, 1);
      if (res.success) {
        setNotifications(res.success);
        /*  console.log(res.success); */
      }
    } catch (error) {}
  };

  function autoRefresh() {
    var tme = Math.floor(Math.random() * 1001) + 5000;
    RefreshVar = setInterval(getNotifs, tme);
    /* console.log("refreshing start id:", RefreshVar); */
  }

  function stopRefresh() {
    console.log("stop fetching notifs");
    console.log("stopping refresh id:", RefreshVar);
    clearInterval(RefreshVar);
  }

  useEffect(() => {
    autoRefresh();
    return () => {
      stopRefresh();
    };
  }, []);

  return (
    <SafeAreaView>
      <View style={[styles.container]}>
        <TouchableOpacity
          activeOpacity={1}
          onPress={onPress}
          style={{
            width: "100%",
            flexDirection: "row",
            justifyContent: "space-between",
            backgroundColor: Colors.ACCENT_COLOR,
            borderRadius: 5,
            height: 50,
          }}
        >
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Image
              style={{
                width: windowWidth * 0.7,
                height: 50,
                marginRight: -20,
                marginLeft: -40,
              }}
              resizeMode="cover"
              source={require("../../../assets/godtribe/GT_LogoSecondary_Wht_RGB-01.png")}
            />
          </View>
          <View
            style={{
              flexDirection: "row",
            }}
          >
            <TouchableOpacity
              style={{
                flexDirection: "row",
                alignItems: "center",
              }}
              onPress={() => navigation.navigate("SearchScreen")}
            >
              <IconButton
                icon={"magnify"}
                color={"white"}
                style={{
                  backgroundColor: Colors.ACCENT_COLOR,
                  borderRadius: 5,
                  margin: 0,
                  paddingRight: 0,
                }}
                size={25}
                onPress={() => navigation.navigate("SearchScreen")}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                flexDirection: "row",
                alignItems: "center",
              }}
              onPress={() => navigation.navigate("InboxScreen")}
            >
              <IconButton
                icon={"chat"}
                color={"white"}
                style={{
                  backgroundColor: Colors.ACCENT_COLOR,
                  borderRadius: 5,
                  margin: 0,
                  paddingRight: 0,
                }}
                size={25}
                onPress={() => navigation.navigate("InboxScreen")}
              />
              {/* {notifications.length > 0 && (
                <Badge
                  style={{
                    alignSelf: "flex-start",
                    marginLeft: -25,
                    marginTop: 5,
                    backgroundColor: Colors.GTBLUE,
                  }}
                >
                  {notifications.length}
                </Badge>
              )} */}
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                flexDirection: "row",
                alignItems: "center",
              }}
              onPress={() => navigation.navigate("Notification")}
            >
              <IconButton
                icon={notifications.length > 0 ? "bell-ring" : "bell"}
                color={"white"}
                style={{
                  backgroundColor: Colors.ACCENT_COLOR,
                  borderRadius: 5,
                  margin: 0,
                  paddingRight: 0,
                }}
                size={25}
                onPress={() => navigation.navigate("Notification")}
              />
              {notifications.length > 0 && (
                <Badge
                  style={{
                    alignSelf: "flex-start",
                    marginLeft: -15,
                    marginTop: 5,
                    backgroundColor: Colors.GTBLUE,
                  }}
                >
                  {notifications.length}
                </Badge>
              )}
            </TouchableOpacity>
            <View style={{ justifyContent: "center" }}>
              <IconButton
                icon={active ? "close" : "menu"}
                color={"white"}
                style={{
                  backgroundColor: Colors.ACCENT_COLOR,
                  borderRadius: 5,
                  margin: 0,
                  paddingLeft: 0,
                  marginLeft: 0,
                }}
                size={30}
                onPress={onPress}
              />
            </View>
          </View>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default HeadTitle;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: Colors.ACCENT_COLOR,
    padding: 5,
  },
});
