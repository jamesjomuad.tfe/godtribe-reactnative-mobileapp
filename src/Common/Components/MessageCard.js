import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import * as Colors from "../Styles/Colors";
import { windowWidth, windowHeight } from "../Utils/Dimentions";
import { Paragraph, Caption, ActivityIndicator } from "react-native-paper";
import PhotoGrid from "react-native-thumbnail-grid";
import { useSelector } from "react-redux";
import RenderHtml from "react-native-render-html";

const MessageCard = ({ messageData, usersData }) => {
  const currentUser = useSelector((state) => state.userState.currentUser);

  const owned = currentUser.id == messageData.sender_id ? true : false;

  /* console.log(currentUser); */
  /*  console.log(messageData); */

  function insert(str, index, value) {
    return str.substr(0, index) + value + str.substr(index);
  }

  const renderText = `<div style="word-wrap: break-word;">${insert(
    messageData.message.rendered,
    2,
    ` style="word-wrap: break-word;"`
  )}</div>`;

  return (
    <View style={{ flexDirection: "row" }}>
      <Image
        source={{
          uri: owned
            ? currentUser.avatar
            : usersData[messageData.sender_id]?.user_avatars?.full,
        }}
        style={{ width: 30, height: 30, borderRadius: 5, margin: 5 }}
      />

      <View style={owned ? styles.toStyle : styles.fromStyle}>
        {/* {messageData.message && (
          <Paragraph>{messageData.message.raw}</Paragraph>
        )}
        {messageData.message && (
          <RenderHtml
            contentWidth={windowWidth}
            source={{ html: messageData.message.rendered }}
            style={{ width: windowWidth - 20 }}
            tagsStyles={tagsStyles.contentHead}
            enableExperimentalMarginCollapsing={true}
          />
        )} */}
        {messageData.message && (
          <View style={{ maxHeight: windowWidth * 0.8 }}>
            <RenderHtml
              contentWidth={windowWidth}
              source={{
                html: messageData.message.rendered,
              }}
              enableExperimentalMarginCollapsing={true}
              tagsStyles={tagsStyles.contentRendered}
            />
          </View>
        )}

        <View style={{ alignItems: "center" }}>
          {messageData?.images && (
            <TouchableOpacity onPress={() => console.log(messageData.images)}>
              <PhotoGrid
                source={messageData.images}
                width={windowWidth * 0.8}
                height={windowHeight / 2}
              />
            </TouchableOpacity>
          )}
        </View>
        {messageData.display_date ? (
          <Caption style={{ alignSelf: "flex-end" }}>
            {messageData.display_date}
          </Caption>
        ) : (
          <ActivityIndicator size={10} style={{ alignSelf: "flex-end" }} />
        )}
      </View>
    </View>
  );
};

const tagsStyles = {
  contentRendered: {
    p: {
      display: "flex",
      flexWrap: "wrap",
      color: "black",
      flexDirection: "row",
      marginVertical: -5,
      maxWidth: windowWidth * 0.83,
    },
    img: {
      width: 20,
      height: 20,
      marginHorizontal: 2,
      paddingHorizontal: 2,
    },
  },
};

export default MessageCard;

const styles = StyleSheet.create({
  fromStyle: {
    padding: 15,
    backgroundColor: Colors.WHITE02,
    alignSelf: "flex-start",
    borderRadius: 10,
    borderTopLeftRadius: 0,
    marginVertical: 5,
    maxWidth: windowWidth * 0.84,
    paddingBottom: 5,
  },
  toStyle: {
    padding: 15,
    backgroundColor: Colors.ACCENT_COLOR_LIGHTER,
    alignSelf: "flex-start",
    borderRadius: 10,
    borderTopLeftRadius: 0,
    marginVertical: 5,
    maxWidth: windowWidth * 0.84,
    paddingBottom: 5,
  },
});
