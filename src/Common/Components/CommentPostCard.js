import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import * as Colors from "../../Common/Styles/Colors";
import { useSelector } from "react-redux";
import RenderHtml from "react-native-render-html";
import { windowWidth } from "../Utils/Dimentions";
import { WebView } from "react-native-webview";
import moment from "moment";
import ProfileImage from "./ProfileImage";

const CommentReplyPostCard = ({ item, onReply }) => {
  return (
    <View>
      {item
        ? item.map((item, index) => {
            return (
              <CommentPostCard
                onReply={() => onReply(item)}
                onDeleteComment={(value) => {}}
                key={index}
                item={item}
                isMargin={true}
              />
            );
          })
        : null}
    </View>
  );
};

const CommentPostCard = ({ isMargin, item, onReply }) => {
  const currentUser = useSelector((state) => state.userState.currentUser);
  const auth = useSelector((state) => state.authState.currentAuth);
  const [gif, setGif] = useState();

  /* console.log(item); */

  const comment = {
    html: `${item.content.rendered}`,
  };

  const commentTime = moment
    .utc(item.date)
    .local()
    .startOf("seconds")
    .fromNow();

  useEffect(() => {
    if (item.media_gif) {
      let frame = item.media_gif.rendered.split('src="').pop().split('"')[0];
      setGif(frame);
    }
  }, []);

  return (
    <View style={styles.cardContainer}>
      <View style={[!isMargin ? styles.card : styles.replyCard]}>
        {/* <Image
          style={styles.mainImg}
          source={{ uri: item.user_avatar.thumb }}
          backgroundColor={Colors.WHITE_COLOR}
        /> */}
        <View style={styles.mainImg}>
          <ProfileImage userData={item} />
        </View>
        <View style={{ paddingTop: 5, flex: 1, marginRight: 10 }}>
          <Text style={{ fontWeight: "bold", fontSize: 16 }}>
            {item.name}
            <Text
              style={{
                fontWeight: "normal",
                fontSize: 14,
                color: Colors.WHITE_COLOR_DARKER,
              }}
            >
              {" "}
              {commentTime}
            </Text>
          </Text>
          <View
            style={{
              paddingLeft: 10,
              paddingRight: 10,
              paddingTop: 5,
              backgroundColor: Colors.WHITE_COLOR,
              flex: 1,
              borderRadius: 2,
              overflow: "hidden",
            }}
          >
            <RenderHtml
              contentWidth={windowWidth}
              source={comment}
              enableExperimentalMarginCollapsing={true}
              tagsStyles={tagsStyles}
            />
            {gif && (
              <WebView
                style={{
                  height: 300,
                  backgroundColor: "#f8f8f8",
                  overflow: "hidden",
                  opacity: 0.99,
                }}
                useWebKit={true}
                originWhitelist={["*"]}
                allowsInlineMediaPlayback={true}
                javaScriptEnabledAndroid={true}
                javaScriptEnabled={true}
                source={{
                  html: `<!DOCTYPE html><html><head><style>
            .video-container {
              position: absolute;
              top: 0;
              bottom: 0;
              width: 100%;
              height: 100%; 
              overflow: hidden;
            }
            .video-container video {
              /* Make video to at least 100% wide and tall */
              min-width: 100%; 
              min-height: auto;
            
              /* Setting width & height to auto prevents the browser from stretching or squishing the video */
              width: auto;
              height: auto;
            
              /* Center the video */
              position: absolute;
              top: 50%;
              left: 50%;
              transform: translate(-50%,-50%);
            }
            </style>
            <div class="video-container" >
            </head><body><video playsinline controls autoplay loop muted src="${gif}" ></video>
            </div>
            </body></html>`,
                }}
                scalesPageToFit={Platform.OS === "ios" ? false : true}
              />
            )}
          </View>
        </View>
      </View>

      <View style={styles.cardInner}>
        <TouchableOpacity
          onPress={() => onReply(item)}
          style={[styles.interactionCon, { marginLeft: isMargin ? 100 : 50 }]}
        >
          <Text style={styles.interactionTxt}>REPLY</Text>
        </TouchableOpacity>
      </View>

      <CommentReplyPostCard
        item={item.comments}
        onReply={(value) => onReply(value)}
      />
    </View>
  );
};

export default CommentPostCard;

const tagsStyles = {
  p: {
    margin: 0,
    padding: 0,
  },
};

const styles = StyleSheet.create({
  cardContainer: {
    backgroundColor: "white",
    flex: 1,
  },
  card: {
    flexDirection: "row",
    marginBottom: 5,
  },
  replyCard: {
    flexDirection: "row",
    marginLeft: 50,
    flex: 1,
  },
  cardInner: {
    alignItems: "flex-start",
  },

  mainImg: {
    width: 50,
    height: 50,
    margin: 5,
    borderRadius: 5,
  },
  repImg: {
    width: 40,
    height: 40,
    margin: 5,
  },
  interactionCon: {
    padding: 5,
    paddingHorizontal: 15,
  },
  interactionTxt: {
    fontWeight: "bold",
    color: Colors.WHITE03,
    fontSize: 12,
    alignSelf: "flex-end",
  },

  writeTaskWrapper: {
    flexDirection: "row",
  },
  input: {
    width: "80%",
    paddingVertical: 15,
    paddingHorizontal: 15,
    backgroundColor: "#FFF",
    borderRadius: 60,
    borderColor: "#C0C0C0",
    borderWidth: 1,
  },
  addWrapper: {
    width: 60,
    height: 60,
    backgroundColor: "#FFF",
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#C0C0C0",
    borderWidth: 1,
    borderRadius: 60,
  },
});
