import React from "react";
import { StyleSheet, Text, View, ScrollView, SafeAreaView } from "react-native";
import {
  Placeholder,
  PlaceholderMedia,
  PlaceholderLine,
  Fade,
  Shine,
} from "rn-placeholder";
import * as Colors from "../Styles/Colors";

const LoadingProfile = () => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Placeholder
        Animation={Shine}
        style={{
          flex: 1,
          backgroundColor: "white",
        }}
      >
        <View
          resizeMode="cover"
          style={{
            width: "100%",
            height: 200,
            backgroundColor: Colors.WHITE_COLOR_DARKER,
          }}
        />
        <View
          style={{
            flex: 1,
            width: "100%",
            height: "100%",
            position: "absolute",
            top: 0,
            right: 0,
          }}
        >
          <ScrollView>
            <View
              style={{
                paddingTop: "35%",
              }}
            >
              <View
                style={{
                  padding: 1,
                  backgroundColor: "white",
                  zIndex: 999,
                  alignSelf: "center",
                }}
              >
                <PlaceholderMedia
                  style={{ width: 140, height: 130, opacity: 0.9 }}
                />
              </View>
              <View
                style={{
                  backgroundColor: "white",
                  alignItems: "center",
                  marginTop: 10,
                  opacity: 0.7,
                }}
              >
                <PlaceholderLine width={20} style={{ paddingTop: 5 }} />
                <PlaceholderLine width={30} />
              </View>
            </View>

            <View style={{ padding: 15, backgroundColor: "white" }}>
              <PlaceholderLine width={20} style={{ opacity: 0.6 }} />
              <View style={{ paddingLeft: 15, opacity: 0.5 }}>
                <View>
                  <PlaceholderLine width={80} />
                  <PlaceholderLine width={60} />
                  <PlaceholderLine width={70} />
                  <PlaceholderLine width={30} />
                </View>
              </View>
              <View>
                <PlaceholderLine
                  width={20}
                  style={{ marginTop: 15, opacity: 0.5 }}
                />
                <View style={{ paddingLeft: 15, opacity: 0.3 }}>
                  <PlaceholderLine width={100} />
                  <PlaceholderLine width={100} />
                  <PlaceholderLine width={100} />
                  <PlaceholderLine width={30} />
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </Placeholder>
    </SafeAreaView>
  );
};

export default LoadingProfile;

const styles = StyleSheet.create({
  HeadTitle: {
    paddingVertical: 10,
    fontSize: 18,
    color: Colors.BLUE02,
    fontWeight: "bold",
  },
  detailContent: {
    fontSize: 16,
    color: Colors.PRIMARY_COLOR,
    marginBottom: 5,
    marginLeft: 15,
  },
  detailTxt: {
    marginBottom: 7,
    color: Colors.PRIMARY_COLOR_LIGHTER,
  },
});
