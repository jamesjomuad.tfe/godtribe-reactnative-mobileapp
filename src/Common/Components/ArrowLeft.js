import React from "react";
import { StyleSheet, Text, View } from "react-native";
import * as Colors from "../Styles/Colors";
import FontAwesome from "react-native-vector-icons/FontAwesome";

const ArrowLeft = ({ onPress, whiteBg }) => {
  return (
    <View style={{ alignItems: "center", justifyContent: "center" }}>
      <FontAwesome.Button
        name="long-arrow-left"
        size={25}
        backgroundColor={whiteBg ? "white" : Colors.PRIMARY_COLOR}
        color={whiteBg ? Colors.PRIMARY_COLOR : "white"}
        onPress={onPress}
      />
    </View>
  );
};

export default ArrowLeft;

const styles = StyleSheet.create({});
