import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import * as Colors from "../Styles/Colors";
import moment from "moment";
import PhotoGrid from "react-native-thumbnail-grid";

import { Button } from "react-native-paper";
import { DELETE_CAMPFIRE, ADD_MEMBER_PUBLIC } from "../../Campfire/api";
import { useDispatch, useSelector } from "react-redux";
import { SET_CAMPFIRES, SET_OWN_CAMPFIRES } from "../Redux/Actions/campfire";
import { windowWidth } from "../Utils/Dimentions";
import ProfileImage from "./ProfileImage";

const RoomCard = ({ room, joinRoom, navigation }) => {
  const [users, setUsers] = useState();
  const [postImage, setPostImage] = useState();
  const date = new Date(Date.parse(room.scheduleToStart));
  const stringDate = date.toLocaleDateString();
  const [isLoading, setIsLoading] = useState(false);
  const [success, setSuccess] = useState(false);
  const [btnTxt, setBtnTxt] = useState("JOIN NOW");

  const [isOpen, setIsOpen] = useState(date <= new Date());
  const roomTime = moment(date).calendar();

  const imgWidth = windowWidth / 4;

  const currentUser = useSelector((state) => state.userState.currentUser);
  const dispatch = useDispatch();

  const onAdd = (member) => {
    const addMember = async (member) => {
      setIsLoading(true);
      try {
        const res = await ADD_MEMBER_PUBLIC(member);
        if (res) {
          /* console.log("[RoomCard] add result", res); */
          dispatch(SET_CAMPFIRES());
          if (currentUser) {
            console.log("[OwnRoom] current user id:", currentUser.id);
            dispatch(SET_OWN_CAMPFIRES(currentUser.id));
          }
          setSuccess(true);
        }
      } catch (error) {
        console.log("[RoomCard] add error", error);
        setSuccess(false);
      }
      setIsLoading(false);
    };

    addMember(member);
  };

  const setAdd = () => {
    let mbr = {
      member: {
        profileUrl: currentUser.avatar,
        name: `${currentUser.firstName} ${currentUser.lastName}`,
        campfire: `${room._id}`,
        uid: `${currentUser.id}`,
      },
      id: `${room._id}`,
    };

    return mbr;
  };

  const goRoom = (room) => joinRoom(room);

  const join = () => {
    const joined = checkIfJoined();
    if (joined == 1) {
      let rm = room;
      data = {
        room: rm,
        status: 1,
      };
      goRoom(data);
    } else if (joined == -1) {
      onAdd(setAdd());
    } else if (joined == 0) {
      let rm = room;
      data = {
        room: rm,
        status: 0,
      };
      goRoom(data);
    }
  };

  let otherTxt = "";

  if (room.members) {
    if (room.members.length == 1) {
      otherTxt = "other";
    } else if (room.members.length > 1) {
      otherTxt = "others";
    }
  }

  const checkIfJoined = () => {
    if (currentUser) {
      if (currentUser.id.toString() == room.creator.uid.toString()) {
        setBtnTxt("ENTER");
        return 0;
      } else {
        setBtnTxt("JOIN NOW");
        const isPresent = room.members.filter(
          (member) => `${member.uid}` === `${currentUser.id}`
        );
        if (isPresent.length > 0) {
          return 1;
        } else {
          return -1;
        }
      }
    }
  };

  useEffect(() => {
    if (room.members) {
      const img = room.members.map((value) => ({
        uri: value.profileUrl,
        data: value,
      }));

      const imgs = img.filter((img) => img.data.status == "invited");
      const tmp = checkIfJoined();

      setPostImage(imgs);
    }
  }, []);

  useEffect(() => {
    if (success) {
      data = {
        room: room,
        status: 1,
      };
      goRoom(data);
    }
  }, [success]);

  return (
    <View style={styles.container}>
      <View style={styles.thumbnail}>
        <View style={styles.imgHolder}>
          {/* <Image source={{ uri: room.creator.profileUrl }} style={styles.img} /> */}
          <ProfileImage userData={room.creator} size={windowWidth / 4} />
        </View>
        {room.members && room.members.length > 0 && postImage && (
          <View style={styles.participants}>
            <PhotoGrid
              source={postImage}
              width={imgWidth}
              height={imgWidth}
              imageStyle={{ borderRadius: 5 }}
            />
          </View>
        )}
      </View>
      {room.members && room.members.length > 0 ? (
        <Text style={{ textAlign: "center" }}>
          <Text style={{ fontWeight: "bold" }}>{`${room.creator.name} `}</Text>
          <Text
            style={{ color: Colors.WHITE03 }}
          >{`and ${room.members.length} ${otherTxt}`}</Text>
        </Text>
      ) : (
        <Text
          style={{ textAlign: "center", fontWeight: "bold" }}
        >{`${room.creator.name}`}</Text>
      )}
      <View style={styles.content}>
        <Text style={styles.topic} numberOfLines={2} ellipsizeMode="tail">
          {room.topic}
        </Text>
        <Text style={[styles.details, { marginVertical: 5 }]}>
          {`${isOpen ? "STARTED:" : "SCHEDULE:"}  ${roomTime}`}
        </Text>
        <ScrollView nestedScrollEnabled={true} style={styles.description}>
          <Text style={styles.details}>{room.description}</Text>
        </ScrollView>
      </View>
      <Button
        style={[
          styles.btn,
          {
            backgroundColor: isOpen
              ? Colors.PRIMARY_COLOR
              : Colors.WHITE_COLOR_DARKER,
          },
        ]}
        labelStyle={{
          fontSize: 18,
          color: isOpen ? "white" : Colors.PRIMARY_COLOR,
        }}
        onPress={join}
        loading={isLoading}
        icon={isOpen ? require("../../../assets/godtribe/audio.png") : ""}
        disabled={!isOpen}
      >
        {isOpen ? btnTxt : "UPCOMING"}
      </Button>
    </View>
  );
};

export default RoomCard;

var styles = StyleSheet.create({
  container: {
    marginVertical: 5,
    marginHorizontal: 20,
    backgroundColor: "white",
    paddingTop: 20,
    elevation: 1,
    borderRadius: 5,
  },
  thumbnail: {
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "center",
    backgroundColor: Colors.WHITE_COLOR,
    padding: 5,
    borderRadius: 5,
  },
  imgHolder: {
    width: windowWidth / 4,
    height: windowWidth / 4,
    borderRadius: 5,
  },
  img: {
    resizeMode: "cover",
    width: "100%",
    height: "100%",
    borderRadius: 5,
  },
  participants: {
    justifyContent: "center",
    alignItems: "center",
    width: windowWidth / 4,
    height: windowWidth / 4,
    marginLeft: 5,
    borderRadius: 5,
  },
  count: {
    fontSize: 20,
    textAlign: "center",
    color: Colors.PRIMARY_COLOR_LIGHTER,
  },
  txt: {
    fontSize: 16,
    textAlign: "center",
    color: Colors.PRIMARY_COLOR_LIGHTER,
  },
  content: {
    padding: 15,
  },
  topic: {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 20,
  },
  details: {
    textAlign: "center",
  },
  description: {
    maxHeight: 95,
  },
  btn: {
    padding: 5,
    borderRadius: 0,
  },
  btnTxt: {
    fontSize: 18,
    color: "white",
  },
});
