import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import * as Colors from "../Styles/Colors";
import {
  Badge,
  Subheading,
  Caption,
  Paragraph,
  Divider,
} from "react-native-paper";
import { useSelector } from "react-redux";
import { windowWidth } from "../Utils/Dimentions";
import RenderHTML from "react-native-render-html";

const ChatCard = ({ chat, navigation }) => {
  const currentUser = useSelector((state) => state.userState.currentUser);

  const [data, setData] = useState(null);

  /* console.log(chat.message); */

  useEffect(() => {
    if (currentUser) {
      var d = chat;
      delete d.recipients[currentUser?.id];
      setData(d);
    }
  }, [currentUser]);

  return (
    <TouchableOpacity
      style={styles.mainContainer}
      onPress={() =>
        navigation.navigate("ChatScreen", {
          chat: chat,
          name:
            chat.group_name ||
            (data && data?.recipients[Object.keys(data.recipients)[0]]?.name),
        })
      }
    >
      <View style={styles.container}>
        <View style={styles.leftItem}>
          <View style={styles.profileHolder}>
            {chat.avatar[0].full ? (
              <Image
                style={styles.profileImage}
                source={{ uri: chat.avatar[0].full }}
              />
            ) : (
              <Image
                source={require("../../../assets/default-img.jpg")}
                style={styles.profileImage}
              />
            )}
          </View>
          <View style={styles.chatDetails}>
            <Subheading style={styles.chatName}>
              {chat.group_name ||
                (data &&
                  data?.recipients[Object.keys(data.recipients)[0]]?.name)}
            </Subheading>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginVertical: -3,
                paddingVertical: -3,
              }}
            >
              <Caption
                style={styles.chatContent}
                numberOfLines={1}
                ellipsizeMode="tail"
              >
                {currentUser.id == chat.last_sender_id
                  ? "You: "
                  : `${chat?.recipients[chat?.last_sender_id].name}: `}
              </Caption>
              <RenderHTML
                contentWidth={windowWidth}
                source={{
                  html: `<div>${chat.message.rendered}</div>`,
                }}
                enableExperimentalMarginCollapsing={true}
                tagsStyles={tagsStyles.contentRendered}
              />
            </View>
          </View>
        </View>
        {/* {images && images.length > 0 && (
          <PhotoGrid width={100} source={images} />
        )} */}
        <View style={styles.rightItem}>
          <Paragraph style={styles.chatTime}>
            {new Date(chat?.date).toLocaleDateString()}
          </Paragraph>
          {/* {chat?.count && (
            <Badge style={styles.countBadge}>{chat?.count}</Badge>
          )} */}
        </View>
      </View>
      <Divider />
    </TouchableOpacity>
  );
};

const tagsStyles = {
  contentRendered: {
    div: { flexDirection: "row", alignItems: "center" },
    p: {
      /* display: "flex",
      flexWrap: "wrap", */
      color: "black",
      flexDirection: "row",
      marginVertical: -5,
      maxWidth: windowWidth * 0.7,
      fontSize: 12,
      alignItems: "center",
      color: Colors.GRAY600,
    },
    img: {
      width: 10,
      height: 10,
      marginHorizontal: 1,
      paddingHorizontal: 1,
      paddingVertical: 2.5,
    },
  },
};

export default ChatCard;

const styles = StyleSheet.create({
  mainContainer: {
    marginBottom: 5,
  },
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 5,
  },
  profileHolder: {
    width: 60,
    justifyContent: "center",
  },
  leftItem: {
    flexDirection: "row",
    alignItems: "center",
    flex: 9,
  },
  profileImage: {
    width: 60,
    height: 60,
    resizeMode: "cover",
    borderRadius: 5,
  },
  chatDetails: {
    paddingLeft: 10,
  },
  chatName: {
    fontWeight: "bold",
    margin: 0,
  },
  chatContent: {
    margin: 0,
  },
  rightItem: {
    flex: 2,
    paddingTop: 5,
    alignContent: "flex-end",
  },
  chatTime: {
    fontWeight: "bold",
    textAlign: "right",
  },
  countBadge: {
    backgroundColor: Colors.ACCENT_COLOR,
  },
});
