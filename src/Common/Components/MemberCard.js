import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  Button,
  Caption,
  Paragraph,
  Subheading,
  Surface,
} from "react-native-paper";
import {
  CANCEL_FRIENDSHIP,
  DELETE_FRIENDSHIP,
  FETCH_USERS_DATA,
  GET_USER,
  POST_FRIENDSHIP,
  UPDATE_FRIENDSHIP,
} from "../api";
import ProfileImage from "./ProfileImage";
import { windowWidth } from "../Utils/Dimentions";
import { Colors } from "../Styles/Colors";
import { useSelector, useDispatch } from "react-redux";

const MemberCard = ({ navigation, friend }) => {
  const currentUser = useSelector((state) => state.userState.currentUser);

  const [friendDetails, setFriendDetails] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const [isFriend, setIsFriend] = useState(false);
  const [friendshipStatus, setFriendshipStatus] = useState(null);
  const [frLoading, setFrLoading] = useState(false);

  const [isYou, setIsYou] = useState(false);
  const [id, setId] = useState(null);

  const getFriendDetails = async () => {
    setIsLoading(true);
    try {
      /* console.log(friend); */
      /* setFriendDetails(null); */
      const res = await FETCH_USERS_DATA(friend.id);
      if (res) {
        setFriendDetails(res);
        /* console.log("FETCH_USERS_DATA", res); */
      }
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  const postAddFriend = async () => {
    setFrLoading(true);
    if (friendshipStatus) {
      var res;

      try {
        if (friendshipStatus == "Add Friend") {
          res = await POST_FRIENDSHIP(currentUser.id, friendDetails.id);
        }
        if (friendshipStatus == "Confirm") {
          res = await UPDATE_FRIENDSHIP(friendDetails.friendship_id);
        }
        if (friendshipStatus == "Friends") {
          res = await DELETE_FRIENDSHIP(friendDetails.id);
        }
        if (friendshipStatus == "Pending") {
          /* console.log(friendDetails); */
          res = await CANCEL_FRIENDSHIP(friendDetails.friendship_id);
        }

        if (res?.success) {
          await getFriendDetails();
        }
      } catch (error) {
        console.log("[ProfileScreen]", error);
      }
    }
    setFrLoading(false);
  };

  useEffect(() => {
    if (friendDetails) {
      /* console.log(friendDetails.friendship_status); */
      if (friendDetails.friendship_status == "not_friends") {
        setFriendshipStatus("Add Friend");
      }
      if (friendDetails.friendship_status == "pending") {
        setFriendshipStatus("Pending");
      }
      if (friendDetails.friendship_status == "awaiting_response") {
        setFriendshipStatus("Confirm");
      }
      if (friendDetails.friendship_status == "is_friend") {
        setFriendshipStatus("Friends");
      }

      if (currentUser.id == friendDetails.id) {
        setIsYou(true);
      } else {
        setIsYou(false);
      }

      setId(friendDetails.id);
    }
  }, [friendDetails]);

  /* useEffect(() => {
    if (friendshipStatus) {
      console.log(friendshipStatus);
    }
  }, [friendshipStatus]); */

  useEffect(() => {
    getFriendDetails();
  }, []);

  return (
    <Surface
      style={{
        borderRadius: 10,
        marginHorizontal: 0,
        marginBottom: 5,
        elevation: 3,
        padding: 0,
        width: windowWidth - 10,
      }}
    >
      {friendDetails && (
        <TouchableOpacity
          disabled={!friendDetails}
          onPress={() => id && navigation.navigate("Profile", { id })}
          style={{
            marginHorizontal: 0,
            padding: 10,
            flexDirection: "row",
            width: windowWidth - 10,
            position: "relative",
          }}
        >
          <View>
            <ProfileImage userData={friend} />
          </View>
          <View style={{ marginLeft: 10, flex: 1 }}>
            <Subheading>{friend?.name}</Subheading>
            <Caption style={{ marginTop: 0 }}>
              {friend?.xprofile?.groups[1]?.fields[1]?.value?.raw}{" "}
              {friend?.xprofile?.groups[1]?.fields[2]?.value?.raw}{" "}
              {friend?.xprofile?.groups[1]?.fields[3]?.value?.raw}
            </Caption>
          </View>
          {/* <Paragraph
            style={{
              margin: 10,
              padding: 0,
              position: "absolute",
              top: 5,
              right: 5,
              zIndex: 999,
              fontSize: 12,
              color:
                friendDetails?.friendship_status == "not_friends"
                  ? Colors.ACCENT_COLOR
                  : Colors.GTBLUE,
            }}
          >
            {isYou ? "YOU" : friendshipStatus}
          </Paragraph> */}
          <Button
            disabled={frLoading || isYou}
            loading={frLoading}
            mode="text"
            style={{
              margin: 0,
              padding: 0,
              position: "absolute",
              top: 5,
              right: 5,
              zIndex: 999,
            }}
            labelStyle={{
              margin: 0,
              padding: 0,
              fontSize: 12,
              color:
                friendDetails?.friendship_status == "not_friends"
                  ? Colors.ACCENT_COLOR
                  : Colors.GTBLUE,
            }}
            disabled={frLoading || isYou}
            onPress={postAddFriend}
          >
            {isYou ? "YOU" : friendshipStatus}
          </Button>
        </TouchableOpacity>
      )}
    </Surface>
  );
};

export default MemberCard;

const styles = StyleSheet.create({});
