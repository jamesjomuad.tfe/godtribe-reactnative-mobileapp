import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList,
} from "react-native";
import * as Colors from "../Styles/Colors";
import { useSelector } from "react-redux";
import CommentPostCard from "./CommentPostCard";
const CommentReplyPostCard = ({ item, onReply }) => {
  const currentUser = useSelector((state) => state.userState.currentUser);
  return (
    <View>
      {item
        ? item.map((item, index) => {
            return (
              <CommentPostCard
                key={index}
                item={item}
                onReply={(value) => {}}
                onDeleteComment={(value) => {}}
                isMargin={true}
              />
            );
          })
        : null}
    </View>
  );
};

export default CommentReplyPostCard;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  cardContainer: {
    backgroundColor: "#f8f8f8",
  },
  card: {
    flexDirection: "row",
    backgroundColor: "#f8f8f8",
    marginBottom: 5,
    marginLeft: "20%",
  },
  replyCard: {
    flexDirection: "row",
    marginLeft: 50,
  },
  cardInner: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  userImg: {
    width: 50,
    height: 50,
  },
  writeTaskWrapper: {
    flexDirection: "row",
  },
  input: {
    width: "80%",
    paddingVertical: 15,
    paddingHorizontal: 15,
    backgroundColor: "#FFF",
    borderRadius: 60,
    borderColor: "#C0C0C0",
    borderWidth: 1,
  },
  addWrapper: {
    width: 60,
    height: 60,
    backgroundColor: "#FFF",
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#C0C0C0",
    borderWidth: 1,
    borderRadius: 60,
  },
});
