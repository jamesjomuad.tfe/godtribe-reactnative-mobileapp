import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  ScrollView,
  SafeAreaView,
  Linking,
  TouchableOpacity,
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import * as Colors from "../Styles/Colors";
import * as api from "../api/axios";
import RenderHtml from "react-native-render-html";
import { windowWith } from "../Utils/Dimentions";
import LoadingProfile from "./LoadingProfile";
import { Button, Paragraph, Title, Surface, Divider } from "react-native-paper";
import urls from "../api/urls";

const ProfileDetails = ({ detail }) => {
  if (detail) {
    return (
      <Surface
        style={{
          width: "100%",
          elevation: 3,
          borderRadius: 10,
        }}
      >
        <View
          style={{ padding: 15, backgroundColor: "white", borderRadius: 10 }}
        >
          <Text style={styles.HeadTitle}>Details</Text>
          <View style={{ paddingLeft: 15, flexDirection: "row" }}>
            <View style={{ justifyContent: "center" }}>
              <Text style={styles.detailTxt}>First Name:</Text>
              <Text style={styles.detailTxt}>Last Name:</Text>
              <Text style={styles.detailTxt}>Nickname:</Text>
              <Text style={styles.detailTxt}>Birthday:</Text>
            </View>
            <View>
              <Text style={styles.detailContent}>
                {detail.xprofile.groups[1].fields[1] == undefined
                  ? ""
                  : detail.xprofile.groups[1].fields[1].value.raw}
              </Text>
              <Text style={styles.detailContent}>
                {detail.xprofile.groups[1].fields[2] == undefined
                  ? ""
                  : detail.xprofile.groups[1].fields[2].value.raw}
              </Text>
              <Text style={styles.detailContent}>
                {detail.xprofile.groups[1].fields[3] == undefined
                  ? ""
                  : detail.xprofile.groups[1].fields[3].value.raw}
              </Text>
              <Text style={styles.detailContent}>
                {detail.xprofile.groups[1].fields[4] == undefined
                  ? ""
                  : new Date(
                      detail.xprofile.groups[1].fields[4].value.raw.replace(
                        " ",
                        "T"
                      )
                    ).toLocaleDateString()}
              </Text>
            </View>
          </View>
          {detail.xprofile.groups[2].fields && (
            <View>
              <Text style={styles.HeadTitle}>About Me</Text>
              <View style={{ paddingLeft: 15 }}>
                <RenderHtml
                  contentWidth={200}
                  source={{
                    html: detail.xprofile.groups[2].fields[5].value.rendered,
                  }}
                  enableExperimentalMarginCollapsing={true}
                  tagsStyles={tagsStyles}
                />
              </View>
            </View>
          )}
        </View>
      </Surface>
    );
  } else {
    return <View></View>;
  }
};

const tagsStyles = {
  p: {
    margin: 0,
    padding: 0,
    fontSize: 16,
    textAlign: "justify",
  },
};

export default ProfileDetails;

const styles = StyleSheet.create({
  HeadTitle: {
    paddingVertical: 10,
    fontSize: 18,
    color: Colors.BLUE02,
    fontWeight: "bold",
  },
  detailContent: {
    fontSize: 16,
    color: Colors.PRIMARY_COLOR,
    marginBottom: 5,
    marginLeft: 15,
  },
  detailTxt: {
    marginBottom: 7,
    color: Colors.PRIMARY_COLOR_LIGHTER,
  },
  followers: {
    flexDirection: "row",
    flex: 1,
    justifyContent: "space-around",
    alignItems: "center",
    backgroundColor: "white",
    padding: 10,
  },
  choices: {
    paddingVertical: 5,
  },
});
