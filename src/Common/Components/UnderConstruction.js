import React from "react";
import { StyleSheet, Text, View, Image, SafeAreaView } from "react-native";
import * as Colors from "../Styles/Colors";

const UnderConstruction = () => {
  return (
    <SafeAreaView style={styles.container}>
      <Text
        style={{ fontSize: 24, fontWeight: "bold", color: Colors.ACCENT_COLOR }}
      >
        UNDER CONSTRUCTION
      </Text>
    </SafeAreaView>
  );
};

export default UnderConstruction;

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.PRIMARY_COLOR,
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});
