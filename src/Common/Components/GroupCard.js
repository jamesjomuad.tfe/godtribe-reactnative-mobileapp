import React, { useEffect, useState } from "react";
import {
  ImageBackground,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
} from "react-native";
import {
  Avatar,
  Button,
  Headline,
  IconButton,
  Paragraph,
  Subheading,
  Surface,
  Title,
} from "react-native-paper";
import { useSelector, useDispatch } from "react-redux";
import { GET_GROUPS_MEMBERS, POST_JOIN, POST_REQUEST_ACCESS } from "../api";
import { Colors } from "../Styles/Colors";
import {
  capitalizeFirstLetter,
  convertBoolean,
  formatGroupRole,
} from "../Utils/Format";
import { FETCH_USER_GROUPS } from "../Redux/Actions/user";

const GroupCard = ({ item, navigation, setAllGroups }) => {
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.userState.currentUser);
  const [page, setPage] = useState(1);
  const [members, setMembers] = useState([]);

  const [joinStatus, setJoinStatus] = useState(null);
  const [isJoining, setIsJoining] = useState(false);

  const attemptJoin = async () => {
    setIsJoining(true);
    if (joinStatus == "Join Group") {
      try {
        const res = await POST_JOIN(currentUser.id, "member", item.id);
        if (res.success) {
          setJoinStatus(formatGroupRole("Member"));
          /* console.log(res.success); */
          setAllGroups();
          dispatch(FETCH_USER_GROUPS(currentUser.id));
        }
      } catch (error) {}
    }
    if (joinStatus == "Request Access") {
      try {
        const res = await POST_REQUEST_ACCESS(currentUser.id, item.id, "");
        if (res.success) {
          setJoinStatus("Request Sent");
          /* console.log(res.success); */
          setAllGroups();
          dispatch(FETCH_USER_GROUPS(currentUser.id));
        }
      } catch (error) {}
    }
    setIsJoining(false);
  };

  const decideJoinStatus = () => {
    if (item.status == "public") {
      if (!item.is_member) {
        setJoinStatus("Join Group");
      } else {
        setJoinStatus(formatGroupRole(item.role));
      }
    }

    if (item.status == "private") {
      if (!item.is_member) {
        if (!item.request_id) {
          setJoinStatus("Request Access");
        } else {
          setJoinStatus("Request Sent");
        }
      } else {
        setJoinStatus(formatGroupRole(item.role));
      }
    }

    /* console.log(item.name, item.status, item.is_member, item.can_join); */
  };

  const getMembers = async () => {
    try {
      const res = await GET_GROUPS_MEMBERS(item.id, page);
      if (res.success) {
        setMembers(res.success);
        /* console.log(res.success); */
      }
    } catch (error) {}
  };

  useEffect(() => {
    getMembers();
    decideJoinStatus();
  }, []);

  return (
    <View style={{ marginHorizontal: 10, marginTop: 5, padding: 5 }}>
      <Surface style={{ elevation: 1, borderRadius: 5, marginHorizontal: 5 }}>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate("GroupPage", { groupDetail: item })
          }
        >
          <View style={{ borderTopLeftRadius: 5, borderTopRightRadius: 5 }}>
            <Image
              source={{
                uri:
                  item.cover_url ||
                  "https://images.unsplash.com/photo-1612178537253-bccd437b730e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1374&q=80",
              }}
              resizeMode="cover"
              style={{
                width: "100%",
                height: 200,
                borderTopLeftRadius: 5,
                borderTopRightRadius: 5,
              }}
            ></Image>
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              width: "100%",
            }}
          >
            <Surface
              style={{
                marginTop: -55,
                elevation: 1,
                width: 105,
                height: 105,
                borderRadius: 5,
                backgroundColor: "white",
                margin: 15,
                padding: 5,
              }}
            >
              <Image
                source={{
                  uri:
                    item?.avatar_urls?.full ||
                    "https://images.unsplash.com/photo-1506869640319-fe1a24fd76dc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
                }}
                style={{
                  width: "100%",
                  height: "100%",
                  borderRadius: 5,
                }}
              ></Image>
            </Surface>
            {/* <TouchableOpacity
              style={{ paddingHorizontal: 15, paddingVertical: 5 }}
            >
              <Title
                style={{
                  textAlignVertical: "center",
                  color: "dodgerblue",
                }}
              >
                {joinStatus}
              </Title>
            </TouchableOpacity> */}
            <Button
              labelStyle={{ color: "dodgerblue", fontSize: 18 }}
              uppercase={false}
              disabled={
                isJoining ||
                (joinStatus != "Join Group" && joinStatus != "Request Access")
              }
              onPress={attemptJoin}
              loading={isJoining}
            >
              {joinStatus}
            </Button>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          oonPress={() =>
            navigation.navigate("GroupPage", { groupDetail: item })
          }
          style={{ paddingHorizontal: 15, marginBottom: 20 }}
        >
          <Headline>{item.name}</Headline>
          <Subheading style={{ color: Colors.GRAY600 }}>
            {capitalizeFirstLetter(item.status)} / Group
          </Subheading>
        </TouchableOpacity>
        <View
          style={{
            flexDirection: "row",
            marginLeft: 20,
            marginTop: -10,
            marginBottom: 15,
          }}
        >
          {members !== null &&
            members !== undefined &&
            typeof members === "object" &&
            members?.map((member) => (
              <Avatar.Image
                key={member?.id}
                size={25}
                source={{
                  uri:
                    member?.avatar_urls?.full ||
                    "https://staging.godtribe.com/app/plugins/buddyboss-platform/bp-core/images/mystery-group.png",
                }}
                style={{ marginLeft: -5 }}
              />
            ))}
        </View>
      </Surface>
    </View>
  );
};

export default GroupCard;

const styles = StyleSheet.create({});
