import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import { Avatar, Surface } from "react-native-paper";
import Ionicons from "react-native-vector-icons/Ionicons";
import * as Colors from "../Styles/Colors";
import ProfileImage from "./ProfileImage";

const AddPostCard = ({ user, addPost, viewProfile, titlePlaceholder }) => {
  return (
    <Surface style={styles.card}>
      <View style={styles.userInfo}>
        <TouchableOpacity onPress={viewProfile}>
          {/* <Image
            style={styles.userImg}
            source={{ uri: user.avatar }}
            backgroundColor={Colors.WHITE_COLOR}
          /> */}
          <ProfileImage userData={user} />
        </TouchableOpacity>
        <View style={styles.userInfoText}>
          {titlePlaceholder === undefined && (
            <TouchableOpacity onPress={viewProfile}>
              <Text style={styles.userName}>
                {`${user.firstName} ${user.lastName}`}
              </Text>
            </TouchableOpacity>
          )}
          <TouchableOpacity onPress={addPost}>
            <View
              style={{
                flexDirection: "row",
                backgroundColor: "white",
                borderRadius: 5,
                alignItems: "center",
                justifyContent: "space-between",
                height: 50,
              }}
            >
              <Text
                style={{ color: Colors.PRIMARY_COLOR_LIGHTER, marginLeft: 20 }}
              >
                {titlePlaceholder === undefined
                  ? "Write something"
                  : titlePlaceholder}
              </Text>
              {titlePlaceholder === undefined && (
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <View style={{ padding: 2 }}>
                    <Ionicons
                      name="image-outline"
                      size={30}
                      color={Colors.WHITE02}
                    ></Ionicons>
                  </View>
                  <View style={{ padding: 2, marginRight: 5 }}>
                    <Ionicons
                      name="camera-outline"
                      size={35}
                      color={Colors.WHITE02}
                    ></Ionicons>
                  </View>
                </View>
              )}
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </Surface>
  );
};

export default AddPostCard;

const styles = StyleSheet.create({
  card: {
    flex: 1,
    backgroundColor: "white",
    marginBottom: 5,
    elevation: 3,
  },
  userInfo: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start",
    padding: 15,
  },
  userName: {
    fontSize: 16,
    fontWeight: "bold",
    paddingRight: 5,
    marginBottom: 5,
  },
  userInfoText: {
    flexDirection: "column",
    marginLeft: 10,
    flex: 1,
  },
  userImg: {
    width: 50,
    height: 50,
    borderRadius: 5,
  },
});
