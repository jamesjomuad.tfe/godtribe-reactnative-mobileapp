import React, { useState, useEffect } from "react";
import {
  LayoutAnimation,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  UIManager,
  View,
  Image,
} from "react-native";
import { Divider, Subheading, Surface, Title } from "react-native-paper";
import * as Colors from "../Styles/Colors";

if (Platform.OS === "android") {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}

export default function CollapsibleHeader1({
  choices,
  setChosen,
  chosen,
  withPicture,
}) {
  /* console.log(withPicture); */
  return (
    <Surface
      style={{
        elevation: 5,
        margin: 15,
        padding: 0,
        borderRadius: 5,
        backgroundColor: "white",
      }}
    >
      {withPicture && (
        <Image
          source={{ uri: withPicture }}
          style={{
            width: "100%",
            height: 200,
            resizeMode: "contain",
            borderRadius: 5,
            borderWidth: 3,
            borderColor: "white",
          }}
        />
      )}
      <Item choices={choices} chosen={chosen} setChosen={setChosen} />
    </Surface>
  );
}

function Item({ choices, setChosen, chosen }) {
  const [open, setopen] = useState(false);
  const onPress = () => {
    LayoutAnimation.easeInEaseOut();
    setopen(!open);
  };
  return (
    <View style={styles.item} activeOpacity={1}>
      <TouchableOpacity style={styles.row} onPress={onPress}>
        <Title
          style={{
            textAlign: "center",
            fontWeight: "bold",
            letterSpacing: 1,
            color: "white",
          }}
        >
          {chosen}
        </Title>
      </TouchableOpacity>
      {open && <Divider style={{ marginBottom: 10 }} />}

      {open &&
        choices &&
        choices.map((choice, index) => (
          <TouchableOpacity
            key={index}
            style={styles.subItem}
            onPress={() => {
              setChosen(choice);
              onPress();
            }}
          >
            <Subheading
              style={{
                textAlign: "center",
                color: chosen == choice ? Colors.PRIMARY_COLOR : Colors.GRAY500,
                fontWeight: chosen == choice ? "bold" : "normal",
              }}
            >
              {choice}
            </Subheading>
          </TouchableOpacity>
        ))}

      {open && <View style={{ marginBottom: 10 }} />}
    </View>
  );
}

const styles = StyleSheet.create({
  item: {
    width: "100%",
    overflow: "hidden",
  },
  subItem: {
    paddingBottom: 5,
  },
  row: {
    width: "100%",
    paddingVertical: 10,
    backgroundColor: Colors.ACCENT_COLOR,
    borderRadius: 5,
  },
});
