import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, Image, ScrollView } from "react-native";
import * as Colors from "../Styles/Colors";
import moment from "moment";
import PhotoGrid from "react-native-thumbnail-grid";

import { Button, Dialog, Portal, Paragraph } from "react-native-paper";
import { ADD_MEMBER_PRIVATE } from "../../Campfire/api";
import { useDispatch, useSelector } from "react-redux";
import { windowWidth } from "../Utils/Dimentions";
import ProfileImage from "./ProfileImage";

const PrivateRoomCard = ({ room, joinRoom }) => {
  const [postImage, setPostImage] = useState();
  const date = new Date(Date.parse(room.scheduleToStart));
  const [stats, setStats] = useState(-1);

  const [isOpen, setIsOpen] = useState(date <= new Date());
  const roomTime = moment(date).calendar();

  const imgWidth = windowWidth / 4;

  const currentUser = useSelector((state) => state.userState.currentUser);
  const dispatch = useDispatch();

  const [visible, setVisible] = useState(false);

  const showDialog = () => setVisible(true);

  const hideDialog = () => setVisible(false);

  const goRoom = (room) => (isOpen ? joinRoom(room) : showDialog());

  const onAdd = (member) => {
    const addMember = async (member) => {
      try {
        const res = await ADD_MEMBER_PRIVATE(member);
        if (res) {
          setStats(0);
        }
      } catch (error) {
        console.log("[RoomCard] add error", error);
      }
    };

    addMember(member);
  };

  const checkIfJoined = () => {
    if (currentUser) {
      if (currentUser.id.toString() == room.creator.uid.toString()) {
        return 2;
      } else {
        const isPresent = room.members.filter(
          (member) => `${member.uid}` === `${currentUser.id}`
        );
        if (isPresent.length > 0) {
          if (isPresent[0].status == "invited") {
            return 1;
          } else {
            return 0;
          }
        } else {
          return -1;
        }
      }
    }
  };

  const setAdd = () => {
    let mbr = {
      member: {
        profileUrl: currentUser.avatar,
        name: `${currentUser.firstName} ${currentUser.lastName}`,
        campfire: `${room._id}`,
        uid: `${currentUser.id}`,
      },
      id: `${room._id}`,
    };

    return mbr;
  };

  const join = () => {
    const joined = checkIfJoined();
    if (joined == 1) {
      let rm = room;
      data = {
        room: rm,
        status: 1,
      };
      goRoom(data);
    } else if (joined == -1) {
      onAdd(setAdd());
    } else if (joined == 2) {
      let rm = room;
      data = {
        room: rm,
        status: 0,
      };
      goRoom(data);
    }
  };

  let otherTxt = "";

  if (room.members) {
    if (room.members.length == 1) {
      otherTxt = "other";
    } else if (room.members.length > 1) {
      otherTxt = "others";
    }
  }

  useEffect(() => {
    if (room.members) {
      const img = room.members.map((value) => ({
        uri: value.profileUrl,
        data: value,
      }));

      const imgs = img.filter((img) => img.data.status == "invited");

      setPostImage(imgs);
      if (currentUser) {
        const res = checkIfJoined();
        setStats(res);
      }
    }
  }, []);

  return (
    <View style={styles.container}>
      <Portal>
        <Dialog visible={visible} onDismiss={hideDialog}>
          <Dialog.Title>Notice</Dialog.Title>
          <Dialog.Content>
            <Paragraph>Room is not open yet.</Paragraph>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={hideDialog}>OK</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
      <View style={styles.thumbnail}>
        <View style={styles.imgHolder}>
          {/* <Image source={{ uri: room.creator.profileUrl }} style={styles.img} /> */}
          <ProfileImage userData={room.creator} size={windowWidth / 4} />
        </View>
        {room.members && room.members.length > 0 && postImage && (
          <View style={styles.participants}>
            <PhotoGrid source={postImage} width={imgWidth} height={imgWidth} />
          </View>
        )}
      </View>
      {room.members && room.members.length > 0 ? (
        <Text style={{ textAlign: "center" }}>
          <Text style={{ fontWeight: "bold" }}>{`${room.creator.name} `}</Text>
          <Text
            style={{ color: Colors.WHITE03 }}
          >{`and ${room.members.length} ${otherTxt}`}</Text>
        </Text>
      ) : (
        <Text
          style={{ textAlign: "center", fontWeight: "bold" }}
        >{`${room.creator.name}`}</Text>
      )}
      <View style={styles.content}>
        <Text style={styles.topic} numberOfLines={2} ellipsizeMode="tail">
          {room.topic}
        </Text>
        <Text style={[styles.details, { marginVertical: 5 }]}>
          {`${isOpen ? "STARTED:" : "SCHEDULE:"}  ${roomTime}`}
        </Text>
        <ScrollView nestedScrollEnabled={true} style={styles.description}>
          <Text style={styles.details}>{room.description}</Text>
        </ScrollView>
      </View>
      <View>
        <Button
          style={
            (stats == -1 && styles.btn) ||
            (stats == 0 ? styles.btn0 : styles.btn1)
          }
          labelStyle={
            (stats == -1 && styles.btnTxt) ||
            (stats == 0 ? styles.btnTxt0 : styles.btnTxt1)
          }
          onPress={join}
          loading={false}
          icon={
            stats == 1 || stats == 2
              ? require("../../../assets/godtribe/audio.png")
              : ""
          }
        >
          {(stats == -1 && "ASK TO JOIN") ||
            (stats == 0 && "WAITING FOR APPROVAL") ||
            (stats == 2 ? "ENTER" : "JOIN NOW")}
        </Button>
      </View>
    </View>
  );
};

export default PrivateRoomCard;

var styles = StyleSheet.create({
  container: {
    marginVertical: 5,
    marginHorizontal: 20,
    backgroundColor: "white",
    paddingTop: 20,
    elevation: 1,
    borderRadius: 5,
  },
  thumbnail: {
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "center",
    backgroundColor: Colors.WHITE_COLOR,
    padding: 5,
    borderRadius: 5,
  },
  imgHolder: {
    width: windowWidth / 4,
    height: windowWidth / 4,
    borderRadius: 5,
  },
  img: {
    resizeMode: "cover",
    width: "100%",
    height: "100%",
    borderRadius: 5,
  },
  participants: {
    justifyContent: "center",
    alignItems: "center",
    width: windowWidth / 4,
    height: windowWidth / 4,
    marginLeft: 5,
    borderRadius: 5,
  },
  count: {
    fontSize: 20,
    textAlign: "center",
    color: Colors.PRIMARY_COLOR_LIGHTER,
  },
  txt: {
    fontSize: 16,
    textAlign: "center",
    color: Colors.PRIMARY_COLOR_LIGHTER,
  },
  content: {
    padding: 15,
  },
  topic: {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 20,
  },
  details: {
    textAlign: "center",
  },
  description: {
    maxHeight: 95,
  },
  btn: {
    padding: 5,
    borderRadius: 0,
    backgroundColor: Colors.WHITE02,
  },
  btnTxt: {
    fontSize: 18,
    color: Colors.PRIMARY_COLOR,
  },
  btn0: {
    padding: 5,
    borderRadius: 0,
    backgroundColor: Colors.WHITE03,
  },
  btnTxt0: {
    fontSize: 18,
    color: "white",
  },
  btn1: {
    padding: 5,
    borderRadius: 0,
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  btnTxt1: {
    fontSize: 18,
    color: "white",
  },
});
