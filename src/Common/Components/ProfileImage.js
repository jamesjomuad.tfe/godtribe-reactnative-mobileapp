import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { FETCH_USERS_DATA, GET_USER } from "../api";
import { Colors } from "../Styles/Colors";
import { Avatar } from "react-native-paper";

const ProfileImage = ({ userData, size }) => {
  const [role, setRole] = useState([]);
  const [roleType, setRoleType] = useState(null);
  const [user, setUser] = useState(null);
  const [color, setColor] = useState(Colors.ACCENT_COLOR);

  const determineRole = () => {
    var r = role;
    var found = r.find((element) => element == "administrator");
    if (found) {
      setColor(Colors.ACCENT_COLOR);
    } else {
      found = r.find((element) => element == "founder");
      if (found) {
        setColor(Colors.GTBLUE);
      } else {
        found = r.find((element) => element == "pioneer");
        if (found) {
          setColor(Colors.PRIMARY_COLOR);
        } else {
          found = r.find((element) => element == "basic");
          if (found) {
            setColor(Colors.GRAY200);
          }
        }
      }
    }

    /* if (found) {
      console.log(user?.username, found);
    } else {
      console.log(user?.username, r);
    } */

    setRoleType(found);
  };

  const getUserData = async (id) => {
    try {
      /* const res = await FETCH_USERS_DATA(userData.user_id); */
      /* console.log("ghghghgh", id); */
      if (role.length < 1) {
        const res = await GET_USER(id);
        if (res.success.role) {
          /* console.log(res.success); */
          setUser(res.success);
          setRole(res.success.role);
        }
      }
    } catch (error) {}
  };

  useEffect(() => {
    if (userData) {
      if (userData?.role) {
        setRole(userData.role);
      }
    }
  }, []);

  useEffect(() => {
    if (userData) {
      if (role.length > 0) {
        determineRole();
      } else {
        const id = userData?.user_id || userData?.id || userData?.ui;
        if (id) {
          getUserData(id);
        } else {
          console.log(userData);
        }
      }
    }
  }, [role]);

  return (
    <View style={{ position: "relative" }}>
      <Image
        style={{
          borderRadius: 5,
          width: size,
          height: size,
          resizeMode: "cover",
        }}
        source={{
          uri:
            userData?.profileUrl ||
            userData?.avatar_urls?.full ||
            userData?.avatar ||
            user?.user_avatar?.full ||
            user?.avatar,
        }}
        backgroundColor={Colors.WHITE_COLOR}
      />
      {/* {roleType && (
        <Avatar.Text
          size={size * 0.33}
          label={roleType[0].toUpperCase()}
          style={{
            backgroundColor: color,
            position: "absolute",
            left: -0.01,
            bottom: -0.01,
            borderRadius: 5,
            elevation: 0,
          }}
        />
      )} */}
      {roleType && roleType != "basic" && (
        <View
          style={{
            width: size * 0.3,
            height: size * 0.3,
            backgroundColor: color,
            position: "absolute",
            left: 0,
            bottom: 0,
            borderRadius: 5,
          }}
        >
          <Image
            source={require("../../../assets/godtribe/whiteIconOnly.png")}
            style={{
              resizeMode: "contain",
              width: size * 0.29,
              height: size * 0.29,
            }}
          />
        </View>
      )}
    </View>
  );
};

ProfileImage.defaultProps = {
  size: 50,
};

export default ProfileImage;

const styles = StyleSheet.create({
  userImg: {
    borderRadius: 5,
  },
});
