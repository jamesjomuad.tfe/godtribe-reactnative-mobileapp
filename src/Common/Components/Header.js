import React, { useEffect, useState } from "react";
import {
  LayoutAnimation,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  UIManager,
  View,
  Linking,
  SafeAreaView,
  Image,
} from "react-native";

import HeadTitle from "./HeadTitle";
import HeadSpacer from "./HeadSpacer";
import * as Colors from "../Styles/Colors";
import { color } from "react-native-reanimated";
import { useDispatch, useSelector } from "react-redux";
import { WIPE_DATA } from "../Redux/Actions";
import { DELETE_AUTH_CACHE } from "../../Authentication/Cache/index";
import urls from "../api/urls";
import { Divider, Paragraph, Title } from "react-native-paper";
import { FETCH_USER_DATA } from "../Redux/Actions/user";

if (Platform.OS === "android") {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}

export default function CollapsibleHeader({
  screen,
  navigateTo,
  toProfile,
  navigation,
}) {
  const dispatch = useDispatch();
  const [active, setActive] = useState(false);

  const currentUser = useSelector((state) => state.userState.currentUser);
  /* const userDetail = useSelector((state) => state.userState.userDetail); */

  /* console.log(currentUser); */

  const onPress = () => {
    LayoutAnimation.easeInEaseOut();
    setActive(!active);
  };

  const open = active;

  const onLogout = async () => {
    setActive(false);
    try {
      const res = await DELETE_AUTH_CACHE();
      dispatch(WIPE_DATA());
      console.log("[Header] Logout", res);
    } catch (exception) {
      console.log(exception);
    }
  };

  const loadInBrowser = () => {
    Linking.openURL(`${urls.baseUrl}/donate/`).catch((err) =>
      console.error("Couldn't load page", err)
    );
  };

  const closeFirst = (toScreen) => {
    onPress();
    setTimeout(function () {
      navigateTo(toScreen);
    }, 500);
  };

  useEffect(() => {
    if (currentUser.id) {
      dispatch(FETCH_USER_DATA(currentUser.id));
    }
  }, []);

  return (
    <SafeAreaView>
      <View
        style={[
          styles.item,
          {
            borderBottomLeftRadius: active ? 10 : 0,
            borderBottomRightRadius: active ? 10 : 0,
          },
        ]}
        activeOpacity={1}
      >
        <HeadTitle
          onPress={onPress}
          active={active}
          title={screen}
          navigation={navigation}
        />
        {open && (
          <View
            style={{
              backgroundColor: Colors.ACCENT_COLOR,
              borderBottomLeftRadius: 10,
              borderBottomRightRadius: 10,
            }}
          >
            <TouchableOpacity
              onPress={() => {
                closeFirst("Campfire");
                /* navigateTo("Campfire"); */
              }}
            >
              <Text
                style={
                  screen === "Campfire" ? styles.subItemActive : styles.subItem
                }
              >
                CAMPFIRES
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                closeFirst("Home");
                /* navigateTo("Home"); */
              }}
            >
              <Text
                style={
                  screen === "Home" ? styles.subItemActive : styles.subItem
                }
              >
                HOME
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                closeFirst("Groups");
                /* navigateTo("Home"); */
              }}
            >
              <Text
                style={
                  screen === "Groups" ? styles.subItemActive : styles.subItem
                }
              >
                GROUPS
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={loadInBrowser}>
              <Text style={styles.subItem}>DONATE</Text>
            </TouchableOpacity>
            {/* <Text style={styles.subItem}>PROFILE</Text> */}
            {/* <View
              style={{
                borderColor: "white",
                width: "100%",
                borderWidth: 1,
                marginTop: 15,
                marginBottom: 10,
              }}
            /> */}
            {/* <TouchableOpacity
              onPress={toProfile}
              style={{
                marginTop: 15,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Image
                source={{ uri: currentUser.avatar }}
                style={{
                  width: 60,
                  height: 55,
                  resizeMode: "cover",
                  borderRadius: 5,
                }}
              />
              <Title style={{ color: "white" }}>
                {currentUser.firstName} {currentUser.lastName}
              </Title>
            </TouchableOpacity> */}
            <TouchableOpacity onPress={onLogout}>
              <Text
                style={[styles.subItem, { marginBottom: 15, fontSize: 20 }]}
              >
                LOGOUT
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    top: 0,
    zIndex: 999,
    width: "100%",
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  item: {
    width: "100%",
    overflow: "hidden",
    backgroundColor: Colors.ACCENT_COLOR,
  },
  subItem: {
    fontSize: 20,
    padding: 10,
    color: "white",
    textAlign: "center",
    backgroundColor: Colors.ACCENT_COLOR,
  },
  subItemActive: {
    fontSize: 24,
    padding: 10,
    color: "white",
    textAlign: "center",
    backgroundColor: Colors.GRAY600,
    fontWeight: "bold",
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
});
