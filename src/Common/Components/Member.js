import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import * as Colors from "../Styles/Colors";

const Member = ({ member, remove, accept }) => {
  const [subStats, setSubStats] = useState(member.status);

  const accepting = () => {
    setSubStats("invited");
  };

  return (
    <View style={styles.container}>
      <Image
        source={{ uri: member.profileUrl }}
        style={{ width: 40, height: 40, resizeMode: "contain", margin: 5 }}
      />
      <View style={{ flex: 3, justifyContent: "center" }}>
        <Text style={{ fontWeight: "bold", fontSize: 16 }}>{member.name}</Text>
        <Text>{member.role}</Text>
      </View>
      {subStats == "invited" ? (
        <View
          style={{
            flex: 2,
            alignItems: "flex-end",
            justifyContent: "space-between",
          }}
        >
          <Text style={{ textAlignVertical: "bottom" }}>{subStats}</Text>
          <TouchableOpacity onPress={() => remove(member)}>
            <Text
              style={{
                textAlignVertical: "center",
                paddingVertical: 5,
                color: Colors.ERROR_COLOR,
              }}
            >
              REMOVE
            </Text>
          </TouchableOpacity>
        </View>
      ) : (
        <View
          style={{
            flex: 2,
            justifyContent: "space-between",
            marginRight: 5,
          }}
        >
          <Text style={{ textAlignVertical: "bottom", textAlign: "right" }}>
            {member.status}
          </Text>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <TouchableOpacity
              onPress={() => {
                accepting();
                accept(member);
              }}
            >
              <Text style={{ textAlignVertical: "center", color: "green" }}>
                ACCEPT
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => remove(member)}>
              <Text
                style={{
                  textAlignVertical: "center",
                  color: Colors.ERROR_COLOR,
                }}
              >
                DECLINE
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      )}
    </View>
  );
};

export default Member;

const styles = StyleSheet.create({
  container: {
    height: 60,
    width: "100%",
    flexDirection: "row",
    padding: 5,
    backgroundColor: "white",
    borderBottomWidth: 1,
    borderBottomColor: Colors.WHITE01,
  },
});
