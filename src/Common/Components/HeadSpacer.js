import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView,
} from "react-native";
import * as Colors from "../Styles/Colors";

const HeadSpacer = () => {
  return (
    <View style={styles.container}>
      <View
        style={{ justifyContent: "center", alignItems: "center", height: 50 }}
      ></View>
      <View style={{ justifyContent: "center" }}>
        <Text
          style={{ color: "white", fontSize: 20, justifyContent: "flex-end" }}
        >
          <Text style={{ fontWeight: "bold" }}></Text>
          <Text></Text>
        </Text>
        <Text style={{ color: "white", justifyContent: "flex-start" }}></Text>
      </View>
    </View>
  );
};

export default HeadSpacer;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: Colors.PRIMARY_COLOR,
    paddingVertical: 5,
  },
});
