import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from "react-native";

import { windowHeight, windowWidth } from "../Utils/Dimentions";
import Icon from "react-native-vector-icons/MaterialIcons";
import AntDesign from "react-native-vector-icons/AntDesign";
import * as Colors from "../Styles/Colors";

const FormInput = ({
  withIcon,
  labelValue,
  placeholderText,
  iconType,
  height,
  txtVertical,
  isPass,
  ...rest
}) => {
  const [privacy, setPrivacy] = useState(isPass);

  return (
    <View style={[styles.inputContainer, { height: height }]}>
      {withIcon && (
        <View style={styles.iconStyle}>
          <AntDesign name={iconType} size={25} color="#666" />
        </View>
      )}
      <TextInput
        style={[styles.input, { textAlignVertical: txtVertical }]}
        value={labelValue}
        placeholder={placeholderText}
        placeholderTextColor="#666"
        numberOfLines={1}
        secureTextEntry={privacy}
        {...rest}
      />
      {isPass && (
        <TouchableOpacity
          style={{ paddingRight: 10 }}
          onPress={() => setPrivacy(!privacy)}
        >
          {privacy ? (
            <Icon name={"visibility-off"} size={20} color={"#666"} />
          ) : (
            <Icon name={"visibility"} size={20} color={"#666"} />
          )}
        </TouchableOpacity>
      )}
    </View>
  );
};

FormInput.defaultProps = {
  withIcon: true,
  height: windowHeight / 15,
  isPass: false,
};

export default FormInput;

const styles = StyleSheet.create({
  inputContainer: {
    marginTop: 5,
    marginBottom: 10,
    width: "100%",
    borderColor: "#ccc",
    borderRadius: 5,
    borderWidth: 1,
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  iconStyle: {
    padding: 10,
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    borderRightColor: "#ccc",
    borderRightWidth: 1,
    width: 50,
  },
  input: {
    padding: 10,
    flex: 1,
    fontSize: 16,
    color: "#333",
    justifyContent: "center",
    alignItems: "center",
  },
  inputField: {
    padding: 10,
    marginTop: 5,
    marginBottom: 10,
    width: windowWidth / 1.5,
    height: windowHeight / 15,
    fontSize: 16,
    borderRadius: 8,
    borderWidth: 1,
  },
});
