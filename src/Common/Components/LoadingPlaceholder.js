import React from "react";
import { SafeAreaView, StyleSheet, Text, View } from "react-native";
import {
  Placeholder,
  PlaceholderMedia,
  PlaceholderLine,
  Fade,
  Shine,
} from "rn-placeholder";

const LoadingPlaceholder = () => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.card}>
        <View style={{ padding: 15 }}>
          <Placeholder Left={PlaceholderMedia} Animation={Shine}>
            <PlaceholderLine width={80} />
            <PlaceholderLine />
            <PlaceholderLine width={30} />
          </Placeholder>
        </View>
      </View>
      <View style={styles.card}>
        <View style={{ padding: 15 }}>
          <Placeholder Left={PlaceholderMedia} Animation={Shine}>
            <PlaceholderLine width={80} />
            <PlaceholderLine />
            <PlaceholderLine />
            <PlaceholderLine width={30} />
          </Placeholder>
        </View>
      </View>
      <View style={[styles.card, { opacity: 0.5 }]}>
        <View style={{ padding: 15 }}>
          <Placeholder Left={PlaceholderMedia} Animation={Shine}>
            <PlaceholderLine width={80} />
            <PlaceholderLine />
          </Placeholder>
        </View>
      </View>
      <View style={[styles.card, { opacity: 0.3 }]}>
        <View style={{ padding: 15 }}>
          <Placeholder Left={PlaceholderMedia} Animation={Shine}>
            <PlaceholderLine width={80} />
            <PlaceholderLine />
            <PlaceholderLine width={80} />
            <PlaceholderLine width={80} />
          </Placeholder>
        </View>
      </View>
      <View style={[styles.card, { opacity: 0.1 }]}>
        <View style={{ padding: 15 }}>
          <Placeholder Left={PlaceholderMedia} Animation={Shine}>
            <PlaceholderLine width={80} />
          </Placeholder>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default LoadingPlaceholder;

const styles = StyleSheet.create({
  card: {
    flex: 1,
    /* backgroundColor: "#f8f8f8", */
    marginBottom: 10,
  },
});
