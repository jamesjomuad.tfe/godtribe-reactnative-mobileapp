import React from "react";
import { Text, TouchableOpacity, StyleSheet } from "react-native";
import { windowHeight, windowWidth } from "../Utils/Dimentions";

const FormButton = ({
  buttonTitle,
  width,
  backgroundColor,
  color,
  ...rest
}) => {
  let bgColor = backgroundColor;
  let txtColor = color;
  return (
    <TouchableOpacity
      style={[styles.buttonContainer, { backgroundColor: bgColor }]}
      {...rest}
    >
      <Text style={[styles.buttonText, { color: txtColor }]}>
        {buttonTitle}
      </Text>
    </TouchableOpacity>
  );
};

FormButton.defaultProps = {
  backgroundColor: "#2e64e5",
  color: "#ffffff",
};

export default FormButton;

const styles = StyleSheet.create({
  buttonContainer: {
    marginTop: 10,
    width: "100%",
    height: windowHeight / 15,
    backgroundColor: "#2e64e5",
    padding: 10,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 3,
  },
  buttonText: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#ffffff",
  },
});
