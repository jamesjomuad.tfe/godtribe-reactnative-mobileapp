import React, { useState, useEffect, useCallback } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ImageBackground,
  Platform,
  Image,
  RefreshControl,
  TouchableOpacity,
} from "react-native";
import {
  Divider,
  Searchbar,
  Surface,
  Title,
  FAB,
  Portal,
} from "react-native-paper";
import HeadSpacer from "../../Common/Components/HeadSpacer";
import Header from "../../Common/Components/Header";
import { windowWidth, windowHeight } from "../../Common/Utils/Dimentions";
import * as Colors from "../../Common/Styles/Colors";
import CounterSquare from "../../Common/Components/CounterSquare";
import { useDispatch, useSelector } from "react-redux";
import {
  SET_CAMPFIRES,
  SET_OWN_CAMPFIRES,
} from "../../Common/Redux/Actions/campfire";
import RoomCard from "../../Common/Components/RoomCard";
import PrivateRoomCard from "../../Common/Components/PrivateRoomCard";

const wait = (timeout) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

var RefreshVar;

const ListCampfireScreen = ({ navigation }) => {
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.userState.currentUser);
  const userGroups = useSelector((state) => state.userState.userGroups);
  const publicRoom = useSelector((state) => state.campfireState.publicRoom);
  const privateRoom = useSelector((state) => state.campfireState.privateRoom);
  const ownRoom = useSelector((state) => state.campfireState.ownRoom);
  const [currentRooms, setCurrentRooms] = useState([]);
  const [baseRoom, setBaseRoom] = useState([]);
  const [live, setLive] = useState([]);
  const [upComing, setUpComing] = useState([]);
  const [activeCampfire, setActiveCampfire] = useState();
  const [state, setState] = useState({ open: false });

  const [isRefreshing, setIsRefreshing] = useState(true);
  const [searchCount, setSearchCount] = useState(0);

  /* useEffect(() => {
    if (userGroups) {
      console.log(userGroups.length);
    }
  }, [userGroups]); */

  const [refreshing, setRefreshing] = useState(false);
  const onRefresh = useCallback(() => {
    setRefreshing(true);

    setData();

    wait(2000).then(() => setRefreshing(false));
  }, []);

  const [searchQuery, setSearchQuery] = useState();
  const onChangeSearch = (query) => setSearchQuery(query);

  const setData = () => {
    /* console.log("fetching!"); */
    dispatch(SET_CAMPFIRES());
    if (currentUser) {
      dispatch(SET_OWN_CAMPFIRES(currentUser?.id));
    }
  };

  function autoRefresh() {
    var tme = Math.floor(Math.random() * 1001) + 5000;
    RefreshVar = setInterval(setData, tme);
    /* console.log("refreshing start id:", RefreshVar); */
  }

  function stopRefresh() {
    /* console.log("stop refresh");
    console.log("stopping refresh id:", RefreshVar); */
    clearInterval(RefreshVar);
  }

  const onStateChange = ({ open }) => setState({ open });
  const { open } = state;

  const joinRoom = (room) => navigation.replace("ActiveRoom", room);

  const dynamicSort = (property) => {
    var sortOrder = 1;
    if (property[0] === "-") {
      sortOrder = -1;
      property = property.substr(1);
    }
    return function (a, b) {
      /* next line works with strings and numbers,
       * and you may want to customize it to your needs
       */
      var result =
        a[property] < b[property] ? -1 : a[property] > b[property] ? 1 : 0;
      return result * sortOrder;
    };
  };

  const setRooms = (rooms) => {
    /* console.log("rooms:", rooms.length); */
    if (rooms.length > 0) {
      let locLive = [];
      let locUpcoming = [];

      rooms.forEach((room) => {
        const date = new Date(Date.parse(room.scheduleToStart));
        if (date <= new Date()) {
          locLive.push(room);
        } else {
          locUpcoming.push(room);
        }
      });
      locUpcoming.sort(dynamicSort("scheduleToStart"));
      setLive(locLive);
      setUpComing(locUpcoming);
      /* console.log("locLive:", locLive.length);
      console.log("locUpcoming:", locUpcoming.length); */
    } else {
      setLive([]);
      setUpComing([]);
    }
  };

  /* const setAllRoom = (rooms) => {
    setCurrentRooms(rooms);
    setRooms(rooms);
  }; */

  useEffect(() => {
    if (activeCampfire == 0) {
      setCurrentRooms(publicRoom);
      setBaseRoom(publicRoom);
    }
    if (activeCampfire == 1) {
      setCurrentRooms(privateRoom);
      setBaseRoom(privateRoom);
    }
    if (activeCampfire == 2) {
      setCurrentRooms(ownRoom);
      setBaseRoom(ownRoom);
    }
  }, [activeCampfire, publicRoom, privateRoom, ownRoom]);

  useEffect(() => {
    if (searchQuery) {
      setIsRefreshing(false);
      /* console.log("searchQuery:", searchQuery); */
      let searchRoom = currentRooms.filter((room) =>
        room.topic.toUpperCase().includes(searchQuery.toUpperCase())
      );
      setSearchCount(searchRoom.length);
      if (searchRoom) {
        setCurrentRooms(searchRoom);
      } else {
        setCurrentRooms(baseRoom);
      }
    } else {
      setCurrentRooms(baseRoom);
      setIsRefreshing(true);
    }
  }, [searchQuery]);

  useEffect(() => {
    if (currentRooms) {
      setRooms(currentRooms);
    }
  }, [currentRooms]);

  useEffect(() => {
    if (isRefreshing) {
      autoRefresh();
    } else {
      stopRefresh();
    }
  }, [isRefreshing]);

  useEffect(() => {
    onRefresh();
    setActiveCampfire(0);
    return () => {
      stopRefresh();
    };
  }, []);

  return (
    <View
      style={[
        styles.container,
        {
          paddingTop: Platform.OS === "ios" ? 50 : 0,
          backgroundColor: Colors.PRIMARY_COLOR,
        },
      ]}
    >
      <HeadSpacer />
      <View
        style={{
          position: "absolute",
          top: Platform.OS === "ios" ? 50 : 0,
          zIndex: 999,
          width: "100%",
        }}
      >
        <Header
          screen={"Campfire"}
          navigateTo={(section) => navigation.jumpTo(section)}
          toProfile={() => navigation.navigate("Profile")}
          navigation={navigation}
        />
      </View>
      <ScrollView
        style={[styles.container, { backgroundColor: Colors.GRAY100 }]}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <ImageBackground
          source={require("../../../assets/godtribe/campfireIMG.jpg")}
          style={{
            width: "100%",
            height: windowHeight / 3,
            alignItems: "center",
          }}
        >
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
              paddingBottom: 10,
            }}
          >
            <Image
              source={require("../../../assets/godtribe/campfireTitle.png")}
              resizeMode="contain"
              style={{
                flex: 1,
                alignItems: "center",
                justifyContent: "center",
                height: 100,
              }}
            />
            <View style={{ flexDirection: "row", marginHorizontal: 20 }}>
              <Searchbar
                placeholder="Search"
                onChangeText={onChangeSearch}
                value={searchQuery}
                style={{ borderRadius: 5, flex: 1, elevation: 0 }}
              />
              {0 > 0 && (
                <View
                  style={{
                    padding: 5,
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: "white",
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "white",
                      fontWeight: "bold",
                      fontSize: 16,
                      paddingVertical: 10,
                      paddingHorizontal: 15,
                      backgroundColor: Colors.ACCENT_COLOR,
                    }}
                  >
                    2
                  </Text>
                </View>
              )}
            </View>
          </View>
        </ImageBackground>
        <View style={{ paddingHorizontal: 20, paddingVertical: 5 }}>
          <TouchableOpacity onPress={() => setActiveCampfire(0)}>
            <Surface
              style={[
                styles.campSurface,
                activeCampfire == 0 && styles.activeSurface,
              ]}
            >
              <Title
                style={{
                  color: activeCampfire == 0 ? "white" : Colors.PRIMARY_COLOR,
                }}
              >
                Public Campfires
              </Title>
              <CounterSquare counter={publicRoom ? publicRoom.length : 0} />
            </Surface>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => setActiveCampfire(1)}>
            <Surface
              style={[
                styles.campSurface,
                activeCampfire == 1 && styles.activeSurface,
              ]}
            >
              <Title
                style={{
                  color: activeCampfire == 1 ? "white" : Colors.PRIMARY_COLOR,
                }}
              >
                Private Campfires
              </Title>
              <CounterSquare counter={privateRoom ? privateRoom.length : 0} />
            </Surface>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate("OwnTab")}>
            <Surface
              style={[
                styles.campSurface,
                activeCampfire == 2 && styles.activeSurface,
              ]}
            >
              <Title
                style={{
                  color: activeCampfire == 2 ? "white" : Colors.PRIMARY_COLOR,
                }}
              >
                My Own Campfires
              </Title>
              <CounterSquare counter={ownRoom ? ownRoom.length : 0} />
            </Surface>
          </TouchableOpacity>
        </View>
        {/* {currentRooms.length > 0 &&
          currentRooms
            .slice(0)
            .reverse()
            .map((room) => (
              <RoomCard
                key={room._id}
                room={room}
                joinRoom={joinRoom} 
                navigation={navigation}
              />
            ))} */}
        {live.length > 0 && (
          <View>
            <Title style={{ paddingLeft: 20, fontWeight: "bold" }}>
              <Title style={{ color: Colors.ACCENT_COLOR, fontWeight: "bold" }}>
                LIVE
              </Title>{" "}
              Campfires
            </Title>
            {live
              .slice(0)
              .reverse()
              .map((room) => {
                if (activeCampfire == 0) {
                  return (
                    <RoomCard
                      key={room._id}
                      room={room}
                      joinRoom={joinRoom}
                      /* navigation={navigation} */
                    />
                  );
                }
                if (activeCampfire == 1) {
                  return (
                    <PrivateRoomCard
                      key={room._id}
                      room={room}
                      /* joinRoom={joinRoom} */
                      /* navigation={navigation} */
                    />
                  );
                }
              })}
          </View>
        )}
        {live.length > 0 && upComing.length > 0 && (
          <Divider style={{ marginHorizontal: 20, marginVertical: 15 }} />
        )}
        {upComing.length > 0 && (
          <View>
            <Title style={{ paddingLeft: 20, fontWeight: "bold" }}>
              <Title style={{ color: Colors.GRAY700, fontWeight: "bold" }}>
                UPCOMING
              </Title>{" "}
              Campfires
            </Title>
            {upComing.map((room) => {
              if (activeCampfire == 0) {
                return (
                  <RoomCard
                    key={room._id}
                    room={room}
                    /* joinRoom={joinRoom} */
                    /* navigation={navigation} */
                  />
                );
              }
              if (activeCampfire == 1) {
                return (
                  <PrivateRoomCard
                    key={room._id}
                    room={room}
                    /* joinRoom={joinRoom} */
                    /* navigation={navigation} */
                  />
                );
              }
            })}
          </View>
        )}
        <View style={{ margin: 50 }}></View>
      </ScrollView>
      <FAB.Group
        open={open}
        style={styles.fab}
        icon={open ? "cog" : "plus"}
        actions={[
          {
            icon: "plus",
            label: "Create Campfire",
            onPress: () => {
              navigation.navigate("CreateCampfire");
            },
            small: false,
          },
          {
            icon: "refresh",
            label: "Refresh",
            onPress: () => onRefresh(),
            small: false,
          },
        ]}
        onStateChange={onStateChange}
        onPress={() => {
          if (open) {
            // do something if the speed dial is open
          }
        }}
      />
    </View>
  );
};

export default ListCampfireScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  campSurface: {
    elevation: 2,
    marginBottom: 5,
    paddingVertical: 5,
    paddingHorizontal: 15,
    justifyContent: "space-between",
    flexDirection: "row",
    borderRadius: 5,
  },
  activeSurface: {
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  fab: {
    position: "absolute",
    right: 0,
    bottom: 0,
    zIndex: 999,
  },
});
