import React, { useState, useEffect, useCallback } from "react";
import {
  View,
  Text,
  RefreshControl,
  ImageBackground,
  Image,
  Dimensions,
  StyleSheet,
  SafeAreaView,
} from "react-native";
import ParallaxScroll from "@monterosa/react-native-parallax-scroll";
import { Searchbar } from "react-native-paper";

import { useDispatch, useSelector } from "react-redux";
import {
  SET_CAMPFIRES,
  SET_OWN_CAMPFIRES,
} from "../../Common/Redux/Actions/campfire";

import * as Colors from "../../Common/Styles/Colors";
import PrivateRoomCard from "../../Common/Components/PrivateRoomCard";

const wait = (timeout) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

const PrivateTab = ({ joinRoom }) => {
  const [rooms, setRooms] = useState();
  const [isLoading, setIsLoading] = useState();
  const [count, setCount] = useState(0);

  // for refresh
  const [refreshing, setRefreshing] = useState(false);
  const currentUser = useSelector((state) => state.userState.currentUser);
  const dispatch = useDispatch();
  const onRefresh = useCallback(() => {
    setRefreshing(true);

    dispatch(SET_CAMPFIRES());
    if (currentUser) {
      console.log("[PrivateTab] current user id:", currentUser.id);
      dispatch(SET_OWN_CAMPFIRES(currentUser.id));
    }

    wait(2000).then(() => setRefreshing(false));
  }, []);

  const privateRoom = useSelector((state) => state.campfireState.privateRoom);

  const [searchQuery, setSearchQuery] = useState();
  const onChangeSearch = (query) => setSearchQuery(query);

  useEffect(() => {
    if (searchQuery) {
      let searchRoom = privateRoom.filter((room) =>
        room.topic.includes(searchQuery)
      );
      if (searchRoom) {
        setRooms(searchRoom);
      } else {
        setRooms(privateRoom);
      }
    } else {
      setRooms(privateRoom);
    }
  }, [searchQuery]);

  useEffect(() => {
    if (privateRoom) {
      setRooms(privateRoom);
    }
  }, []);

  useEffect(() => {
    if (privateRoom) {
      setRooms(privateRoom);
    }
  }, [privateRoom]);

  useEffect(() => {
    if (rooms) {
      setCount(rooms.length);
    }
  }, [rooms]);

  return (
    <View style={{ flex: 1, marginTop: Platform.OS === "ios" ? 50 : 0 }}>
      <ParallaxScroll
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
        headerBackgroundColor=""
        renderHeader={({ animatedValue }) => (
          <View
            animatedValue={animatedValue}
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
              paddingBottom: 10,
            }}
          >
            <Image
              source={require("../../../assets/godtribe/campfireTitle.png")}
              resizeMode="contain"
              style={{
                flex: 1,
                alignItems: "center",
                justifyContent: "center",
              }}
            />
            <View style={{ flexDirection: "row", marginHorizontal: 20 }}>
              <Searchbar
                placeholder="Search"
                onChangeText={onChangeSearch}
                value={searchQuery}
                style={{ borderRadius: 0, flex: 1, elevation: 0 }}
              />
              {count > 0 && (
                <View
                  style={{
                    padding: 5,
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: "white",
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      color: "white",
                      fontWeight: "bold",
                      fontSize: 16,
                      paddingVertical: 10,
                      paddingHorizontal: 15,
                      backgroundColor: Colors.ACCENT_COLOR,
                    }}
                  >
                    {count}
                  </Text>
                </View>
              )}
            </View>
          </View>
        )}
        headerHeight={(Dimensions.get("window").width * 9) / 16}
        isHeaderFixed={false}
        renderParallaxBackground={({ animatedValue }) => (
          <View
            animatedValue={animatedValue}
            style={{
              flex: 1,
            }}
          >
            <ImageBackground
              source={require("../../../assets/godtribe/campfireIMG.jpg")}
              resizeMode="cover"
              style={{
                flex: 1,
              }}
            >
              <View
                style={{
                  flex: 1,
                  backgroundColor: "rgba(0,0,0,0.2)",
                }}
              ></View>
            </ImageBackground>
          </View>
        )}
        parallaxBackgroundScrollSpeed={2.5}
        nestedScrollEnabled={true}
      >
        <View style={{ paddingTop: 2.5 }}>
          {rooms && rooms.length > 0 ? (
            rooms
              .slice(0)
              .reverse()
              .map((room) => (
                <PrivateRoomCard
                  key={room._id}
                  room={room}
                  joinRoom={joinRoom}
                />
              ))
          ) : (
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text style={{ color: Colors.WHITE03 }}>NO PRIVATE CAMPFIRE</Text>
            </View>
          )}
        </View>
        <View style={{ flex: 1, height: 300, width: 200 }} />
      </ParallaxScroll>
    </View>
  );
};

export default PrivateTab;

const styles = StyleSheet.create({});
