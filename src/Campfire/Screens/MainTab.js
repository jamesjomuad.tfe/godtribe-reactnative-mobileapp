import React, { useState, useEffect } from "react";
import { BottomNavigation } from "react-native-paper";
import { SafeAreaView, View, StatusBar, Platform } from "react-native";

import { useDispatch, useSelector } from "react-redux";
import {
  SET_CAMPFIRES,
  SET_OWN_CAMPFIRES,
} from "../../Common/Redux/Actions/campfire";
import * as Colors from "../../Common/Styles/Colors";

import Header from "../../Common/Components/Header";
import HeadSpacer from "../../Common/Components/HeadSpacer";

import PublicTab from "./PublicTab";
import PrivateTab from "./PrivateTab";
import OwnTab from "./OwnTab";

const MainTab = ({ navigation }) => {
  const [index, setIndex] = useState(0);
  const currentUser = useSelector((state) => state.userState.currentUser);

  const [routes] = useState([
    { key: "public", title: "Public", icon: "account-group-outline" },
    { key: "private", title: "Private", icon: "lock-check" },
    { key: "own", title: "My Own", icon: "clipboard-account" },
  ]);

  // standby for role management
  const [basicRoutes] = useState([
    { key: "public", title: "Public", icon: "account-group-outline" },
    { key: "private", title: "Private", icon: "lock-check" },
  ]);

  const renderScene = ({ route }) => {
    switch (route.key) {
      case "public":
        return (
          <PublicTab
            navigation={navigation}
            joinRoom={(room) => navigation.replace("ActiveRoom", room)}
          />
        );
      case "private":
        return (
          <PrivateTab
            navigation={navigation}
            joinRoom={(room) => navigation.replace("ActiveRoom", room)}
          />
        );
      case "own":
        return <OwnTab navigation={navigation} />;
      default:
        return null;
    }
  };

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(SET_CAMPFIRES());
    if (currentUser) {
      dispatch(SET_OWN_CAMPFIRES(currentUser.id));
    }
  }, []);

  return (
    <View style={{ flex: 1 }}>
      <StatusBar barStyle="reverse" />
      <HeadSpacer />
      <View
        style={{
          position: "absolute",
          top: Platform.OS === "ios" ? 50 : 0,
          zIndex: 999,
          width: "100%",
        }}
      >
        <Header
          screen={"Campfire"}
          navigateTo={(section) => navigation.jumpTo(section)}
        />
      </View>
      <BottomNavigation
        navigationState={{ index, routes }}
        onIndexChange={setIndex}
        renderScene={renderScene}
        shifting={true}
        barStyle={{ backgroundColor: Colors.PRIMARY_COLOR, elevation: 0 }}
        activeColor={"white"}
        sceneAnimationEnabled={false}
      />
    </View>
  );
};

export default MainTab;
