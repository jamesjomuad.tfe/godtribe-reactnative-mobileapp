import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, ScrollView, SafeAreaView } from "react-native";
import Member from "../../Common/Components/Member";
import {
  UPDATE_MEMBER_STATUS,
  DELETE_MEMBER,
  GET_CAMPFIRES_MEMBERS,
} from "../api";
import { useDispatch, useSelector } from "react-redux";
import {
  SET_CAMPFIRES,
  SET_OWN_CAMPFIRES,
} from "../../Common/Redux/Actions/campfire";

const MembersScreen = ({ route, navigation }) => {
  const [members, setMembers] = useState([]);
  const [acceptMbr, setAcceptMbr] = useState();
  const [acceptSuccess, setAcceptSuccess] = useState(false);

  const room = route.params.currentRoom;
  const currentUser = useSelector((state) => state.userState.currentUser);
  const dispatch = useDispatch();

  const callRefresh = () => {
    if (currentUser) {
      dispatch(SET_OWN_CAMPFIRES(currentUser.id));
    }
    dispatch(SET_CAMPFIRES());
  };

  const remove = (member) => {
    const tempMembers = members;
    let resMembers = tempMembers.filter((mbr) => mbr != member);
    setMembers(resMembers);

    const toDeleteMember = toDelete(member);

    const deleteCampfire = async (member) => {
      try {
        const res = await DELETE_MEMBER(member);
        if (res) {
          setMembers(resMembers);
          callRefresh();
        }
      } catch (error) {
        console.log("[MemberScreen] delete", error);
      }
    };

    deleteCampfire(toDeleteMember);
  };

  const toDelete = (member) => {
    let mbr = {
      uid: member.uid,
      id: member.campfire,
    };
    return mbr;
  };

  const treatMember = (member) => {
    if (member) {
      data = {
        status: "invited",
        uid: member.uid,
        id: room._id,
      };

      return data;
    } else {
      return null;
    }
  };

  useEffect(() => {
    if (acceptMbr && acceptSuccess) {
      let tmpMbr = acceptMbr;
      console.log(tmpMbr);
      tmpMbr["status"] = "invited";
      let tmp = members;
      var index = tmp.indexOf(acceptMbr);

      if (index !== -1) {
        tmp[index] = tmpMbr;
      }
      console.log("res", tmp);
      console.log("old", members);
      setMembers(tmp);
    }
  }, [acceptMbr, acceptSuccess]);

  const acceptMember = (data) => {
    const updateMember = async (data) => {
      try {
        const res = await UPDATE_MEMBER_STATUS(data);
        if (res) {
          callRefresh();
          setAcceptSuccess(true);
        } else {
          setAcceptSuccess(false);
        }
      } catch (error) {
        setAcceptSuccess(false);
        console.log("[MemberScreen] accept ", error);
      }
    };

    updateMember(data);
  };

  const accept = (member) => {
    setAcceptMbr(member);
    setAcceptMbr(member);
    console.log(acceptMbr);
    const treatData = treatMember(member);

    if (treatData) {
      acceptMember(treatData);
    }
  };

  var myVar;

  function autoRefresh() {
    var tme = Math.floor(Math.random() * 1001) + 1000;
    console.log("[MemberScreen] starting with interval of:", tme);
    myVar = setInterval(myTimer, tme);
  }

  function stopRefresh() {
    console.log("[MemberScreen] clearing interval");
    clearInterval(myVar);
  }

  function myTimer() {
    const getMembers = async () => {
      try {
        const res = await GET_CAMPFIRES_MEMBERS(room._id);
        if (res.length > 0) {
          setMembers(res);
        } else {
          setMembers([]);
        }
      } catch (error) {
        console.log("[MemberScreen]", error);
      }
    };
    getMembers();
  }

  useEffect(() => {
    console.log("[MemberScreen] memberScreen mount");
    autoRefresh();
    return () => {
      stopRefresh();
      console.log("[MemberScreen] memberScreen unmount");
    };
  }, []);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ScrollView style={{ backgroundColor: "white", flex: 1 }}>
        {members.length > 0 &&
          members
            .slice(0)
            .reverse()
            .map((member, index) => (
              <Member
                key={index}
                member={member}
                remove={remove}
                accept={accept}
              />
            ))}
      </ScrollView>
    </SafeAreaView>
  );
};

export default MembersScreen;

const styles = StyleSheet.create({});
