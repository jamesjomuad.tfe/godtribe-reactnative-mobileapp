import { StyleSheet, Text, View, FlatList } from "react-native";
import React, { useState, useEffect } from "react";
import { Searchbar } from "react-native-paper";

const FriendScreen = ({ navigation }) => {
  const [invited, setInvited] = useState([]);
  const [invitedTemp, setInvitedTemp] = useState([]);
  const [invitedBase, setInvitedBase] = useState([]);
  const [friends, setFriends] = useState([]);
  const [friendsTemp, setFriendsTemp] = useState([]);
  const [friendsBase, setFriendsBase] = useState([]);

  return (
    <View style={{ flex: 1 }}>
      <Searchbar style={{ width: "100%" }} />
      {(invited.length > 0 || invitedTemp.length > 0) && (
        <View style={{ width: "100%" }}>INVITED</View>
      )}
      {(friends.length > 0 || friendsBase.length > 0) && (
        <View style={{ width: "100%" }}>INVITED</View>
      )}
    </View>
  );
};

export default FriendScreen;

const styles = StyleSheet.create({});
