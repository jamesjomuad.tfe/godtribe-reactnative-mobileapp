import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  SafeAreaView,
  KeyboardAvoidingView,
  TouchableOpacity,
} from "react-native";
import {
  TextInput,
  Button,
  Switch,
  TouchableRipple,
  Paragraph,
  Title,
  Subheading,
  IconButton,
  Badge,
  Dialog,
  Portal,
  Headline,
  Caption,
  configureFonts,
} from "react-native-paper";
import * as Colors from "../../Common/Styles/Colors";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import NumericInput from "react-native-numeric-input";

import { useDispatch, useSelector } from "react-redux";
import {
  SET_CAMPFIRES,
  SET_OWN_CAMPFIRES,
} from "../../Common/Redux/Actions/campfire";
import { CREATE_CAMPFIRE } from "../api";

const CreateCampfire = ({ navigation }) => {
  const [choseDate, setChoseDate] = useState();
  const [choseTime, setChoseTime] = useState();
  const [datetime, setDatetime] = useState();
  const [mode, setMode] = useState("date");
  const [text, setText] = useState("");
  const [dateTxt, setDateTxt] = useState("DATE");
  const [timeTxt, setTimeTxt] = useState("TIME");
  const [isSwitchOn, setIsSwitchOn] = useState(false);
  const [status, setStatus] = useState("Everyone");
  const [topic, setTopic] = useState("");
  const [altTopic, setAltTopic] = useState("");
  const [description, setDescription] = useState("");
  const [errorMsg, setErrorMsg] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [success, setSuccess] = useState(false);
  const [visible, setVisible] = useState(false);
  const hideDialog = () => setVisible(false);
  const [group, setGroup] = useState(null);
  const [haveSched, setHaveSched] = useState(false);
  const [haveTime, setHaveTime] = useState(false);
  const [durationHour, setDurationHour] = useState(1);
  const [durationTime, setDurationTime] = useState(0);

  const currentUser = useSelector((state) => state.userState.currentUser);
  const dispatch = useDispatch();

  const onToggleSwitch = () => setIsSwitchOn(!isSwitchOn);

  useEffect(() => {
    if (isSwitchOn) {
      setStatus("Invite Only");
    } else {
      setStatus("Everyone");
    }
  }, [isSwitchOn]);

  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  const showDatePicker = () => {
    setMode("date");
    setDatePickerVisibility(true);
  };

  const showTimePicker = () => {
    setMode("time");
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    if (mode === "date") {
      setChoseDate(date);
      setDateTxt(date.toLocaleDateString("en-US").toString());
    } else {
      setChoseTime(date);
      setTimeTxt(getTimeAMPMFormat(date));
    }
    hideDatePicker();
  };

  const getTimeAMPMFormat = (date) => {
    let hours = date.getHours();
    let minutes = date.getMinutes();
    const ampm = hours >= 12 ? "PM" : "AM";
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    hours = hours < 10 ? "0" + hours : hours;
    // appending zero in the start if hours less than 10
    minutes = minutes < 10 ? "0" + minutes : minutes;
    return hours + ":" + minutes + " " + ampm;
  };

  const setSchedule = () => {
    let sched = null;
    if (choseDate && choseTime == undefined) {
      setDatetime(choseDate);
      sched = choseDate;
    } else if (choseDate == undefined && choseTime) {
      (sched = choseTime), setDatetime(choseTime);
    } else if (choseDate && choseTime) {
      sched = new Date(
        choseDate.getFullYear(),
        choseDate.getMonth(),
        choseDate.getDate(),
        choseTime.getHours(),
        choseTime.getMinutes(),
        choseTime.getSeconds()
      );
    }
    return sched;
  };

  const setDataForCampfire = () => {
    let sched = setSchedule();
    let campfire = {};
    if (currentUser) {
      let name = currentUser?.nickname
        ? currentUser.nickname
        : currentUser?.username
        ? currentUser.username
        : `${currentUser.firstName} ${currentUser.lastName}`;
      campfire = {
        topic: topic,
        altTopic: altTopic == "" ? topic : altTopic,
        description: description,
        creator: {
          uid: currentUser.id,
          profileUrl: currentUser.avatar,
          name: name,
        },
        openTo: status,
        hidden: false,
      };
      if (sched) {
        campfire.scheduleToStart = sched;
      }
    }

    return campfire;
  };

  const sendDataToAPI = (data) => {
    const sendData = async (data) => {
      setIsLoading(true);
      try {
        const res = await CREATE_CAMPFIRE(data);
        if (res) {
          dispatch(SET_CAMPFIRES());
          if (currentUser) {
            dispatch(SET_OWN_CAMPFIRES(currentUser.id));
          }
          setSuccess(true);
        }
        setIsLoading(false);
      } catch (error) {
        setSuccess(false);
        setIsLoading(false);
      }
    };

    sendData(data);
  };

  useEffect(() => {
    if (success) {
      navigation.goBack();
    }
  }, [success]);

  const createCampfire = () => {
    if (topic && description) {
      let campfire = setDataForCampfire();
      if (campfire) {
        console.log("we should create");
        sendDataToAPI(campfire);
      }
      setErrorMsg(null);
    } else {
      if (topic == "") {
        console.log("Topic must be filed!");
        setErrorMsg("Topic must be filed!");
      } else if (description == "") {
        console.log("Description must be filed!");
        setErrorMsg("Description must be filed!");
      } else {
      }
    }
  };

  const clearSched = () => {
    setChoseDate(null);
    setChoseTime(null);
    setDateTxt("DATE");
    setTimeTxt("TIME");
    setDatetime(null);
  };

  const clearAll = () => {
    clearSched();
    setTopic("");
    setAltTopic("");
    setDescription("");
    setErrorMsg(null);
    setIsSwitchOn(false);
    setGroup(null);
    setDurationHour(1);
    setDurationTime(0);
  };

  useEffect(() => {
    if (!haveSched) {
      clearSched();
    }
  }, [haveSched]);

  /* useEffect(() => {
    console.log(currentUser);
  }); */

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "margin"}
      style={{ flex: 1 }}
    >
      <SafeAreaView style={{ flex: 1 }}>
        <ScrollView style={styles.container}>
          {errorMsg ? (
            <Text style={{ textAlign: "center", color: Colors.ERROR_COLOR }}>
              {errorMsg}
            </Text>
          ) : (
            <Text></Text>
          )}
          <View>
            <TextInput
              label="Topic"
              value={topic}
              mode="outlined"
              onChangeText={(text) => setTopic(text)}
              style={styles.textInp}
              maxLength={24}
              right={<TextInput.Affix text="/24" />}
            />
            {/* <TextInput
              label="Alternative Topic"
              value={altTopic}
              mode="outlined"
              onChangeText={(text) => setAltTopic(text)}
              style={styles.textInp}
            /> */}
            <TextInput
              label="Description"
              value={description}
              mode="outlined"
              onChangeText={(text) => setDescription(text)}
              multiline={true}
              style={styles.textInp}
              numberOfLines={6}
            />
            {/* <View
              style={{
                flex: 1,
                flexDirection: "row",
                borderRadius: 5,
                backgroundColor: Colors.GRAY100,
                paddingVertical: 10,
                borderWidth: 1,
                borderColor: Colors.GRAY700,
                marginBottom: 10,
                marginTop: 5,
                justifyContent: "space-between",
                alignItems: "center",
                paddingHorizontal: 15,
              }}
            >
              <Subheading style={{ color: Colors.GRAY600 }}>Group: </Subheading>
              {group ? (
                <Subheading style={{ fontWeight: "bold" }}>{group}</Subheading>
              ) : (
                <View
                  style={{
                    flexDirection: "row",
                  }}
                >
                  <Subheading style={{}}>Create for a group</Subheading>
                  <Badge
                    style={{
                      alignSelf: "flex-start",
                      backgroundColor: Colors.GRAY300,
                    }}
                  >
                    6
                  </Badge>
                </View>
              )}
              <IconButton
                icon="plus"
                style={{ margin: 0, padding: 0, backgroundColor: Colors.GRAY300 }}
                onPress={() => setVisible(true)}
              />
            </View> */}
            <Title>Schedule</Title>
            <View
              style={{
                backgroundColor: Colors.PRIMARY_COLOR,
                padding: 10,
                borderRadius: 5,
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                  <Subheading style={{ color: "white", textAlign: "right" }}>
                    Schedule to Start :{"  "}
                  </Subheading>
                </View>
                <TouchableOpacity
                  onPress={() => setHaveSched(!haveSched)}
                  style={{ flex: 1 }}
                >
                  <Subheading
                    style={{
                      color: "white",
                      textDecorationLine: "underline",
                      fontWeight: "bold",
                    }}
                  >
                    {haveSched ? "ON SCHEDULE" : "IMMEDIATELY"}
                  </Subheading>
                </TouchableOpacity>
              </View>
              {haveSched && (
                <View style={{ flexDirection: "row", marginVertical: 5 }}>
                  <Button
                    style={styles.btn}
                    labelStyle={{ fontSize: 16, color: Colors.PRIMARY_COLOR }}
                    onPress={showDatePicker}
                    loading={false}
                    icon="calendar"
                  >
                    {dateTxt}
                  </Button>
                  <Text> </Text>
                  <Button
                    style={styles.btn}
                    labelStyle={{ fontSize: 16, color: Colors.PRIMARY_COLOR }}
                    onPress={showTimePicker}
                    loading={false}
                    icon="clock"
                  >
                    {timeTxt}
                  </Button>
                </View>
              )}
              <View
                style={{
                  flexDirection: "row",
                  marginBottom: 5,
                  alignItems: "center",
                }}
              >
                <View style={{ flex: 1 }}>
                  <Subheading style={{ color: "white", textAlign: "right" }}>
                    Open To :{"  "}
                  </Subheading>
                </View>
                <TouchableOpacity
                  onPress={onToggleSwitch}
                  style={{ flex: 1 }}
                  disabled={currentUser.role.includes("basic")}
                >
                  <Subheading
                    style={{
                      fontWeight: "bold",
                      color: "white",
                      textDecorationLine: currentUser.role.includes("basic")
                        ? "none"
                        : "underline",
                    }}
                  >
                    {isSwitchOn ? "PRIVATE" : "EVERYONE"}
                  </Subheading>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  marginBottom: 5,
                  alignItems: "center",
                }}
              >
                <View style={{ flex: 1 }}>
                  <Subheading style={{ color: "white", textAlign: "right" }}>
                    Duration :{"  "}
                  </Subheading>
                </View>
                <TouchableOpacity
                  onPress={() => setHaveTime(!haveTime)}
                  style={{ flex: 1, flexDirection: "row" }}
                >
                  <Subheading
                    style={{
                      fontWeight: "bold",
                      color: "white",
                      textDecorationLine: "underline",
                    }}
                  >
                    {durationHour < 10 ? `0${durationHour}` : durationHour}:
                    {durationTime < 10 ? `0${durationTime}` : durationTime}
                    {""}
                  </Subheading>
                  <Caption
                    style={{
                      color: "white",
                      textDecorationLine: "none",
                      alignSelf: "center",
                    }}
                  >
                    {"   (hh:mm)"}
                  </Caption>
                </TouchableOpacity>
              </View>
              {haveTime && (
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <NumericInput
                    value={durationHour}
                    onChange={(value) => setDurationHour(value)}
                    textColor="white"
                    totalHeight={40}
                  />
                  <NumericInput
                    value={durationTime}
                    onChange={(value) => setDurationTime(value)}
                    textColor="white"
                    totalHeight={40}
                  />
                  <Button mode="contained" onPress={() => setHaveTime(false)}>
                    SET
                  </Button>
                </View>
              )}
            </View>
            {/* <View
              style={{
                paddingVertical: 10,
                backgroundColor: Colors.GRAY100,
                borderRadius: 0,
                borderWidth: 1,
                borderColor: Colors.GRAY700,
              }}
            ></View> */}

            <DateTimePickerModal
              testID="dateTimePicker"
              value={new Date()}
              mode={mode}
              is24Hour={true}
              display="default"
              isVisible={isDatePickerVisible}
              onConfirm={handleConfirm}
              onCancel={hideDatePicker}
              minimumDate={new Date(Date.now())}
            />

            <Button
              style={[styles.createBtn, { backgroundColor: Colors.GRAY100 }]}
              labelStyle={{ color: Colors.PRIMARY_COLOR }}
              onPress={clearAll}
              loading={false}
              icon="close"
            >
              Clear
            </Button>
            <Text> </Text>
            <Button
              style={[
                styles.createBtn,
                { backgroundColor: Colors.PRIMARY_COLOR },
              ]}
              labelStyle={{ color: "white", fontWeight: "bold", padding: 5 }}
              onPress={createCampfire}
              disabled={isLoading}
              loading={isLoading}
            >
              CREATE CAMPFIRE
            </Button>
            <View style={{ marginBottom: 100 }} />
          </View>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

export default CreateCampfire;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    paddingHorizontal: 15,
  },
  textInp: {
    borderRadius: 0,
    marginBottom: 5,
  },
  btn: {
    padding: 5,
    marginBottom: 5,
    backgroundColor: Colors.ACCENT_COLOR_LIGHTER,
    flex: 1,
  },
  createBtn: {
    padding: 5,
    marginBottom: 5,
    backgroundColor: Colors.ACCENT_COLOR,
    flex: 1,
  },
  modalItem: {
    padding: 10,
    borderRadius: 5,
    marginBottom: 5,
  },
});
