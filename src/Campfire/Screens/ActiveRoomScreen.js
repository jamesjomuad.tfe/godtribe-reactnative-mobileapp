import React, { useState, useEffect, useRef } from "react";
import { StyleSheet, Text, View, SafeAreaView, Platform } from "react-native";
import { useSelector } from "react-redux";
import { WebView } from "react-native-webview";
import * as Colors from "../../Common/Styles/Colors";
import Title from "../../Common/Components/Title";
import { formatTimeURL } from "../../Common/Utils/Format";

const ActiveRoomScreen = ({ route, navigation }) => {
  const currentUser = useSelector((state) => state.userState.currentUser);
  const token = useSelector((state) => state.authState.currentAuth);
  const room = route.params.room;
  const [site, setSite] = useState(null);
  const [txt, seTxt] = useState(null);
  const [currentUrl, setCurrentUrl] = useState();
  const [whiteList, setWhiteList] = useState([]);

  const userAgentData =
    "Mozilla/5.0 (Linux; An33qdroid 10; Android SDK built for x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.185 Mobile Safari/537.36";

  let jsCode = `window.localStorage.setItem("active-campfire", "${room._id}");
                window.localStorage.setItem("access-token", "${token.token}");`;

  useEffect(() => {
    let tmpSite = `https://campfire.godtribe.com/campfires/active/${
      room._id
    }${formatTimeURL(1)}`;
    console.log(`\nSite Url: ${tmpSite}\n`);
    setSite(tmpSite);
    navigation.setOptions({
      title: room.topic,
      headerTintColor: "white",
      headerTitleAlign: "center",
    });

    if (Platform.OS === "ios") {
      setWhiteList([tmpSite, "https://campfire.godtribe.com/campfires/auth"]);
    } else {
      setWhiteList(["*"]);
    }
  }, []);

  useEffect(() => {
    if (whiteList.length > 0) {
      console.log(whiteList);
    }
  }, [whiteList]);

  useEffect(() => {
    if (currentUrl) {
      console.log(`\nCurrent Url: ${currentUrl}\n`);
      if (currentUrl !== site) {
        console.log(
          "\n[ActiveRoom] we need to go back because url is ",
          currentUrl
        );
        /*  Platform.OS === "android" && navigation.replace("Main"); */
        navigation.replace("Main");
      }
    }
  }, [currentUrl]);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
        {/* <Title
          onPress={() => navigation.replace("Main")}
          title={"Active Room"}
          style={{ position: "absolute", top: 0, left: 0 }}

          userAgent={userAgentData}

        /> */}
        {site && (
          <WebView
            style={{ opacity: 0.99 }}
            javaScriptEnabledAndroid={true}
            javaScriptEnabled={true}
            domStorageEnabled={true}
            injectedJavaScriptBeforeContentLoaded={jsCode}
            injectedJavaScript={jsCode}
            style={styles.container}
            incognito={true}
            useWebKit={true}
            allowsInlineMediaPlayback={true}
            mediaPlaybackRequiresUserAction={false}
            bounces={false}
            originWhitelist={["*"]}
            source={{
              uri: site,
            }}
            onNavigationStateChange={({ url }) => {
              setCurrentUrl(url);
              Platform.OS === "ios" && setCurrentUrl(url);
            }}
            onLoadProgress={({ nativeEvent }) => {
              setCurrentUrl(nativeEvent.url);
              Platform.OS === "ios" && setCurrentUrl(nativeEvent.url);
            }}
          />
        )}
      </View>
    </SafeAreaView>
  );
};

export default ActiveRoomScreen;

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: Colors.PRIMARY_COLOR },
});
