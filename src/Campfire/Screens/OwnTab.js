import React, { useState, useEffect, useCallback } from "react";
import {
  View,
  Text,
  ScrollView,
  RefreshControl,
  StyleSheet,
  SafeAreaView,
  Platform,
} from "react-native";
import { FAB } from "react-native-paper";
import * as Colors from "../../Common/Styles/Colors";
import FontAwesome from "react-native-vector-icons/FontAwesome";

import { useDispatch, useSelector } from "react-redux";
import {
  SET_CAMPFIRES,
  SET_OWN_CAMPFIRES,
} from "../../Common/Redux/Actions/campfire";

import OwnRoomCard from "../../Common/Components/OwnRoomCard";

const wait = (timeout) => {
  return new Promise((resolve) => setTimeout(resolve, timeout));
};

const OwnTab = ({ navigation }) => {
  const roomData = useSelector((state) => state.campfireState.ownRoom);

  const [rooms, setRooms] = useState([]);
  const [ownRoom, setOwnRoom] = useState();

  const [stopRefresh, setStopRefresh] = useState(false);

  // for refresh
  const [refreshing, setRefreshing] = useState(false);
  const currentUser = useSelector((state) => state.userState.currentUser);
  const dispatch = useDispatch();
  const onRefresh = useCallback(() => {
    setRefreshing(true);

    dispatch(SET_CAMPFIRES());
    if (currentUser) {
      console.log("[OwnTab] current user id:", currentUser.id);
      dispatch(SET_OWN_CAMPFIRES(currentUser.id));
    }

    wait(2000).then(() => setRefreshing(false));
  }, []);

  useEffect(() => {
    setOwnRoom(roomData);
  }, [roomData]);

  useEffect(() => {
    if (ownRoom) {
      setRooms(ownRoom);
    }
  }, [ownRoom]);

  const closeScreen = () => {
    setStopRefresh(true);
    setTimeout(function () {
      navigation.navigate("Main");
    }, 1000);
  };

  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <View style={{ marginLeft: 10 }}>
          <FontAwesome.Button
            name="long-arrow-left"
            size={25}
            backgroundColor={Colors.PRIMARY_COLOR}
            color="white"
            onPress={closeScreen}
          />
        </View>
      ),
    });
  }, []);

  return (
    <View style={{ flex: 1 }}>
      <ScrollView
        style={{
          flex: 1,
          paddingTop: 15,
          paddingHorizontal: 15,
          zIndex: 0,
        }}
        nestedScrollEnabled={true}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        {rooms && rooms.length > 0 ? (
          rooms
            .slice(0)
            .reverse()
            .map((room) => (
              <OwnRoomCard
                key={room._id}
                room={room}
                joinRoom={(room) => navigation.replace("ActiveRoom", { room })}
                style={{ elevation: 1 }}
                onMember={(currentRoom) =>
                  navigation.navigate("Members", { currentRoom })
                }
                closing={stopRefresh}
              />
            ))
        ) : (
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Text style={{ color: Colors.WHITE03 }}>NO OWNED CAMPFIRE</Text>
          </View>
        )}

        <View style={{ flex: 1, height: 300, width: 200 }} />
      </ScrollView>
      <FAB
        style={[
          styles.fab,
          {
            marginBottom: Platform.OS === "ios" ? 50 : 15,
            marginRight: Platform.OS === "ios" ? 20 : 15,
          },
        ]}
        small={false}
        icon="plus"
        color={"white"}
        onPress={() => navigation.navigate("CreateCampfire")}
      />
    </View>
  );
};

export default OwnTab;

const styles = StyleSheet.create({
  fab: {
    position: "absolute",
    right: 0,
    bottom: 0,
    zIndex: 999,
    margin: 15,
    backgroundColor: Colors.ACCENT_COLOR,
  },
});
