import { API_CAMPFIRE } from "@env";
export default {
  base: {
    dev: API_CAMPFIRE,
  },
  campfires: {
    base: "/campfires",
    members: "/members",
    addMember: "/member/push",
    updateStatus: "/member/set/status",
    deleteMember: "/member/pull",
    encrypt: "/user/encrypt",
  },
};
