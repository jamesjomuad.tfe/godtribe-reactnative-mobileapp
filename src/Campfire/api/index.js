import urls from "./urls";

export const CREATE_CAMPFIRE = async (campfire) => {
  /* console.log(campfire);
  console.log(`${urls.base.dev}${urls.campfires.base}`); */
  try {
    const res = await fetch(`${urls.base.dev}${urls.campfires.base}`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(campfire),
    });
    const json = await res.json();
    return json;
  } catch (error) {
    console.log("[API] campfire api: api error", error);
    return null;
  }
};

export const DELETE_CAMPFIRE = async (id) => {
  try {
    const res = await fetch(`${urls.base.dev}${urls.campfires.base}/${id}`, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });
    const json = await res.json();
    if (json.error) {
      console.log("[API] campfire api: ", json.message);
      return null;
    } else {
      return json;
    }
  } catch (error) {
    console.log("[API] campfire api: api error", error);
    return null;
  }
};

export const ADD_MEMBER_PUBLIC = async (member) => {
  try {
    const res = await fetch(`${urls.base.dev}${urls.campfires.addMember}`, {
      method: "PATCH",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(member),
    });
    const json = await res.json();
    if (json.error) {
      console.log("[API] campfire api: ", json.message);
      return null;
    } else {
      const update = updateMember(member, 1);
      if (update) {
        const rp = await UPDATE_MEMBER_STATUS(update);
        if (rp) {
          /* console.log(rp); */
          return json;
        } else {
          return null;
        }
      } else {
        return null;
      }
    }
  } catch (error) {
    console.log("[API] campfire api: api error", error);
    return null;
  }
};

export const ADD_MEMBER_PRIVATE = async (member) => {
  try {
    const res = await fetch(`${urls.base.dev}${urls.campfires.addMember}`, {
      method: "PATCH",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(member),
    });
    const json = await res.json();
    if (json.error) {
      console.log("[API] campfire api: ", json.message);
      return null;
    } else {
      return json;
    }
  } catch (error) {
    console.log("[API] campfire api: api error", error);
    return null;
  }
};

const updateMember = (member, val) => {
  // 1 - invited, -1 pending
  let data = {};
  let stats = val == 1 ? "invited" : "pending";
  if (member) {
    data = {
      status: stats,
      uid: member.member.uid,
      id: member.id,
    };

    return data;
  } else {
    return null;
  }
};

export const UPDATE_MEMBER_STATUS = async (member) => {
  try {
    const res = await fetch(`${urls.base.dev}${urls.campfires.updateStatus}`, {
      method: "PATCH",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(member),
    });
    const json = await res.json();
    if (json.error) {
      console.log("[API] campfire api: ", json.message);
      return null;
    } else {
      /* console.log("[API] campfire api: ", json); */
      return json;
    }
  } catch (error) {
    console.log("[API] campfire api: api error", error);
    return null;
  }
};

export const DELETE_MEMBER = async (member) => {
  try {
    const res = await fetch(`${urls.base.dev}${urls.campfires.deleteMember}`, {
      method: "PATCH",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(member),
    });
    if (res) {
      return res;
    } else {
      return null;
    }
  } catch (error) {
    console.log("[API] campfire api: api error", error);
    return null;
  }
};

export const ENCRYPT_DATA = async (txt) => {
  try {
    const res = await fetch(`${urls.base.dev}${urls.campfires.encrypt}`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(txt),
    });
    const json = await res.json();
    if (json) {
      /* console.log("[API] campfire api: ", json.data); */
      return json;
    } else {
      return null;
    }
  } catch (error) {
    console.log("[API] campfire api: api error", error);
    return null;
  }
};

export const GET_CAMPFIRES_MEMBERS = async (id) => {
  try {
    const res = await fetch(`${urls.base.dev}/campfires/${id}/member`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });

    const json = await res.json();
    return json;
  } catch (error) {
    console.log("[API] campfire api: api error", error);
    return null;
  }
};
