import axios from "axios";
import urls from "./urls";

const api = axios.create({
  baseURL: urls.base.dev,
});

api.interceptors.request.use(
  (config) => {
    config.timeout = 5000;
    config.headers["Content-Type"] = "application/json";
    config.headers.accept = "application/json";
    return config;
  },
  (error) => Promise.reject(error)
);

export const GET_CAMPFIRES = async () => {
  try {
    let data = await api.get(urls.campfires.base).then(({ data }) => data);
    return data;
  } catch (error) {
    console.log("[AXIOS] campfire api: api error", error);
    return null;
  }
};

export const GET_OWN_CAMPFIRES = async (uid) => {
  try {
    let data = await api
      .get(`campfires/owned?cid=${uid}`)
      .then(({ data }) => data);
    /* let treatedData = {
      randomID: Math.floor(Math.random() * 10000000),
      data: data,
    };
    console.log(treatedData.randomID, treatedData.data[0].members); */
    return data;
  } catch (error) {
    console.log("[AXIOS] campfire api: api error", error);
    return null;
  }
};

export const CREATE_CAMPFIRE = async (campfire) => {
  try {
    console.log(campfire);
    console.log(urls.campfires.base);
    let res = await api
      .post(urls.campfires.base, data)
      .then(({ data }) => data);
    return res;
  } catch (error) {
    console.log("[AXIOS] campfire api: api error", error);
    return null;
  }
};
