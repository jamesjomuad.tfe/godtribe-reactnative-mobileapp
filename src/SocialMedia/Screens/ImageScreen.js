import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  SafeAreaView,
  TouchableOpacity,
  Platform,
} from "react-native";
import Swiper from "react-native-swiper";
import * as Colors from "../../Common/Styles/Colors";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { Button } from "react-native-paper";
import { windowWidth, windowHeight } from "../../Common/Utils/Dimentions";
import { useSelector, useDispatch } from "react-redux";
import { createPosts } from "../../Common/Redux/Actions/index";
import {
  CREATE_POSTS,
  TRASH_IMAGE,
  UPDATE_POSTS,
} from "../../Common/api/index";

const ImageScreen = ({ route, navigation }) => {
  const images = route.params?.images;
  const item = route.params?.item;
  const [listImage, setListImage] = useState(images);
  const [imgIndex, setImgIndex] = useState(0);
  const [setterIndex, setSetterIndex] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [baseImages, setBaseImages] = useState([]);

  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.userState.currentUser);
  const auth = useSelector((state) => state.authState.currentAuth);

  const [post, setPost] = useState({
    user_id: currentUser.id,
    bp_media_ids: [],
    type: "activity_update",
    component: "activity",
    content: "",
    privacy: "public",
  });

  const saveArray = [11038, 11037, 11035, 11034, 11033];

  const removeItemArray = (imageId) => {
    let arr = post.bp_media_ids;
    arr = arr.filter((id) => id !== imageId);
    return arr;
  };

  /* const updatePost = () => {
    const savePosts = async () => {
      setIsLoading(true);
      try {
        //const res = await UPDATE_POSTS(item.id, post, auth);
        dispatch(createPosts(post));
        setIsLoading(false);
      } catch (error) {
        console.log("[ImageScreen] error", error);
        setIsLoading(false);
      }
    };
    savePosts();
  }; */

  const trashImage = (imageId) => {
    const trash = async () => {
      console.log("trashing image");
      setIsLoading(true);
      try {
        const res = await TRASH_IMAGE(imageId, auth);
        dispatch(createPosts(res));
        setIsLoading(false);
      } catch (error) {
        console.log("[ImageScreen] error", error);
        setIsLoading(false);
      }
    };
    trash();
  };

  const removeImage = () => {
    let newList = listImage;
    if (imgIndex > -1) {
      newList.splice(imgIndex, 1);
    }
    setListImage(newList);
  };

  const deleteImage = () => {
    let imageAttachmentId = images[imgIndex].data.attachment_id;
    let newList = removeItemArray(imageAttachmentId);
    removeImage();
    trashImage(imageAttachmentId);
    /* console.log(post); */
    /* console.log(newList); */
    setPost({ ...post, bp_media_ids: newList });
  };

  const settingIndex = (index) => {
    setTimeout(() => {
      setImgIndex(index);
    }, 10);
  };

  /* useEffect(() => {
    if (baseImages.length != post.bp_media_ids.length) {
      updatePost();
    } else {
      console.log(post);
    }
  }, [post]);
 */
  useEffect(() => {
    if (item) {
      let media_ids = [];
      if (item.bp_media_ids) {
        item.bp_media_ids.map((item) => {
          media_ids.push(item.attachment_id);
        });
      }

      setBaseImages(media_ids);

      setPost({
        ...post,
        user_id: item.user_id,
        content: item.content_stripped,
        privacy: item.privacy,
        bp_media_ids: media_ids,
      });
    }
  }, []);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View
        style={{
          backgroundColor: Colors.PRIMARY_COLOR,
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <FontAwesome.Button
          name="long-arrow-left"
          size={25}
          backgroundColor={Colors.PRIMARY_COLOR}
          color={"white"}
          onPress={() => navigation.goBack("Main")}
          style={{ borderRadius: 0, paddingHorizontal: 15 }}
        />
        {currentUser && currentUser.id == item.user_id && (
          <Button
            style={{
              backgroundColor: Colors.PRIMARY_COLOR,
              color: "white",
              elevation: 0,
              paddingLeft: 15,
            }}
            color={"white"}
            icon="trash-can-outline"
            labelStyle={{ fontSize: 14, color: "white" }}
            disabled={isLoading}
            loading={isLoading}
            onPress={() => {
              setIsLoading(true);
              deleteImage();
            }}
          >
            DELETE
          </Button>
        )}
      </View>
      <Swiper
        style={styles.wrapper}
        showsButtons
        loop={false}
        onIndexChanged={(index) => settingIndex(index)}
        index={imgIndex}
      >
        {listImage.length > 0 ? (
          listImage.map((image) => (
            <View
              testID={image.data.id.toString()}
              key={image.data.id}
              style={styles.slide}
            >
              <Image
                source={{ uri: image.uri }}
                style={{
                  resizeMode: "contain",
                  flex: 1,
                  aspectRatio: Platform.OS === "ios" ? 0.5 : 1,
                  width: windowWidth,
                }}
              />
            </View>
          ))
        ) : (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Text style={{ color: "white" }}>NO IMAGE</Text>
            <Text style={{ color: "white" }}></Text>
            <Text style={{ color: "white" }}></Text>
          </View>
        )}
      </Swiper>
    </SafeAreaView>
  );
};

export default ImageScreen;

const styles = StyleSheet.create({
  wrapper: {
    justifyContent: "center",
    alignItems: "center",
    /* paddingBottom: 80, */
    backgroundColor: Colors.PRIMARY_COLOR,
  },
  slide: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 50,
  },
});
