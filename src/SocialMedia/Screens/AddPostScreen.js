import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  SafeAreaView,
  Platform,
  KeyboardAvoidingView,
  ScrollView,
  FlatList,
  TouchableOpacity,
} from "react-native";
import * as Colors from "../../Common/Styles/Colors";
import FormInput from "../../Common/Components/FormInput";
import FormButton from "../../Common/Components/FormButton";
import { createPosts } from "../../Common/Redux/Actions/index";
import { CREATE_POSTS, UPDATE_POSTS } from "../../Common/api/index";
import { windowWidth, windowHeight } from "../../Common/Utils/Dimentions";
import { useSelector, useDispatch } from "react-redux";
import { UPLOAD_IMAGE } from "../../Common/api/index";
import { Camera } from "expo-camera";
import * as ImagePicker from "expo-image-picker";
import { Picker } from "@react-native-picker/picker";
import { Button, Menu, IconButton, Paragraph } from "react-native-paper";
import PhotoGrid from "react-native-thumbnail-grid";
import EmojiSelector from "react-native-emoji-selector";
import BottomDrawer from "react-native-bottom-drawer-view";
// import { AssetsSelector } from "expo-images-picker";
const AddPostScreens = ({ navigation, route }) => {
  const dispatch = useDispatch();
  const [hasGalleryPermission, setHasGalerryPermission] = useState(null);
  const [hasCameraPermission, setHasCameraPermission] = useState(null);
  const [camera, setCamera] = useState(null);
  const [image, setImage] = useState(null);
  const [images, setImages] = useState([]);
  const [camType, setCamType] = useState(Camera.Constants.Type.back);
  const currentUser = useSelector((state) => state.userState.currentUser);
  const auth = useSelector((state) => state.authState.currentAuth);
  const [edit, setEdit] = useState(false);
  const [toEdit, setToEdit] = useState();
  const [group, setGroup] = useState(null);
  const [userDetail, setUserDetail] = useState(null);

  const [isLoading, setIsLoading] = useState(false);
  const [privacyButton, setPrivacyButton] = useState("earth");
  let formdata = new FormData();

  const [post, setPost] = useState({
    user_id: currentUser.id,
    bp_media_ids: [],
    type: "activity_update",
    component: "activity",
    content: "",
    privacy: "public",
  });

  const [visible, setVisible] = useState(false);
  const openMenu = () => setVisible(true);
  const closeMenu = () => setVisible(false);

  const changePrivacy = (title) => {
    console.log(title);
    setPost({ ...post, privacy: title });
    closeMenu();
  };

  useEffect(() => {
    if (route.params?.group) {
      setGroup(route.params?.group);
      /* console.log(route.params?.group); */
    } else {
      setGroup(null);
    }
  }, [route.params?.group]);

  useEffect(() => {
    if (route.params?.photos) {
      setImages(route.params?.photos);
    } else {
      setImages([]);
    }
  }, [route.params?.photos]);

  useEffect(() => {
    if (route.params?.item) {
      setToEdit(route.params?.item);
      setEdit(true);
    } else {
      setToEdit(null);
      setEdit(false);
    }
  }, [route.params?.item]);

  useEffect(() => {
    if (route.params?.userDetail) {
      setUserDetail(route.params?.userDetail);
      /* console.log(route.params?.group); */
    } else {
      setGroup(null);
    }
  }, [route.params?.userDetail]);

  useEffect(() => {
    if (toEdit) {
      let media_ids = [];
      if (toEdit.bp_media_ids) {
        toEdit.bp_media_ids.map((item) => {
          media_ids.push(item.attachment_id);
        });
      }

      setPost({
        ...post,
        user_id: toEdit.user_id,
        content: toEdit.content_stripped,
        privacy: toEdit.privacy,
        bp_media_ids: media_ids,
      });
    }
  }, [toEdit]);

  /* useEffect(() => {
    console.log(post);
  }, [post]); */

  const postPosts = () => {
    var toPost = post;
    if (group) {
      toPost = {
        ...post,
        component: "groups",
        primary_item_id: group.id,
      };
    }

    if (userDetail) {
      toPost = {
        ...post,
        component: "members",
        primary_item_id: userDetail.id,
      };
    }

    /* console.log(toPost); */

    CREATE_POSTS(toPost, auth);
    dispatch(createPosts(post));
  };

  const postUpdatedPosts = () => {
    var toPost = post;
    if (group) {
      toPost = {
        ...post,
        component: "groups",
        primary_item_id: group.id,
      };
    }

    if (userDetail) {
      toPost = {
        ...post,
        component: "members",
        primary_item_id: userDetail.id,
      };
    }

    UPDATE_POSTS(toEdit.id, toPost, auth);
    dispatch(createPosts(post));
  };

  const newPost = () => {
    const savePosts = async () => {
      let length = images.length;
      console.log("[AddPost] posting");
      let flag = 0;
      if (post.content !== "" || length > 0) {
        if (length) {
          images.map(async (item) => {
            formdata.append("media", {
              uri: item.uri,
              name: item.name,
              type: item.type,
            });
            let imagePath = await UPLOAD_IMAGE(formdata, auth);
            /* console.log(imagePath); */
            post.bp_media_ids.push(imagePath.id);
            flag += 1;
            /* console.log("with lopping image", flag); */
            if (flag === length) {
              setIsLoading(false);
              /* CREATE_POSTS(post, auth);
              dispatch(createPosts(post)); */
              postPosts();

              /* console.log("[AddPost] with image", post); */
              navigation.goBack();
            }
          });
        } else {
          setIsLoading(false);
          /* CREATE_POSTS(post, auth);
          dispatch(createPosts(post)); */
          postPosts();

          /* console.log("[AddPost] without image", post); */
          navigation.goBack();
        }
        /* console.log("with something", post.content, length); */
      } else {
        setIsLoading(false);
        console.log("AddPostScreen/savepost no post to save");
      }
    };
    savePosts();
  };

  const updatePost = () => {
    const savePosts = async () => {
      let length = images.length;
      console.log("[AddPost] updating");
      let flag = 0;
      if (post.content !== "" || length > 0) {
        if (length) {
          images.map(async (item) => {
            formdata.append("media", {
              uri: item.uri,
              name: item.name,
              type: item.type,
            });
            let imagePath = await UPLOAD_IMAGE(formdata, auth);
            /* console.log(imagePath); */
            post.bp_media_ids.push(imagePath.id);
            flag += 1;
            /* console.log("with lopping image", flag); */
            if (flag === length) {
              /* const res = await UPDATE_POSTS(toEdit.id, post, auth);
              dispatch(createPosts(post)); */
              postUpdatedPosts();

              setIsLoading(false);
              console.log("[AddPost] with image", post);
              navigation.goBack();
            }
          });
        } else {
          setIsLoading(false);
          /* UPDATE_POSTS(toEdit.id, post, auth);
          dispatch(createPosts(post)); */
          postUpdatedPosts();

          console.log("[AddPost] without image", post);
          navigation.goBack("Home");
        }
        /* console.log("with something", post.content, length); */
      } else {
        setIsLoading(false);
        console.log("AddPostScreen/savepost no post to save");
      }
    };
    savePosts();
  };

  const savePosts = () => {
    if (
      post.content != "" ||
      images.length > 0 ||
      post.bp_media_ids.length > 0
    ) {
      if (edit) {
        updatePost();
      } else {
        newPost();
      }
    } else {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    if (post.privacy == "onlyme") {
      setPrivacyButton("lock");
    }
  }, [post]);

  /* const choosePhotoFromGallery = async () => {
    try {
      const galleryStatus =
        await ImagePicker.requestMediaLibraryPermissionsAsync();
      setHasGalerryPermission(galleryStatus.status === "granted");
      let imageList = [];
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 1,
      });
      if (!result.cancelled) {
        setImage(result.uri);
        formdata.append("media", {
          uri: result.uri,
          name: result.uri.substring(result.uri.lastIndexOf("/") + 1),
          type: result.uri.substring(result.uri.lastIndexOf(".") + 1),
        });
        imageList.push({
          uri: result.uri,
          name: result.uri.substring(result.uri.lastIndexOf("/") + 1),
          type: result.uri.substring(result.uri.lastIndexOf(".") + 1),
        });
        setImages(imageList);
      }
    } catch (error) {}
  }; */

  /* const takePhotoFromCamera = async () => {
    try {
      const cameraStatus = await Camera.requestPermissionsAsync();
      setHasCameraPermission(cameraStatus.status === "granted");
      if (camera) {
        const data = await camera.takePictureAsync(null);
        setImage(data.uri);
      }
    } catch (error) {}
  }; */

  const renderRow = ({ item }) => {
    return (
      <Image
        source={{ uri: item.uri }}
        width="100"
        height="100"
        style={styles.postImage}
      />
    );
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : null}
      style={{ flex: 1 }}
    >
      <View style={{ flex: 1, justifyContent: "flex-start" }}>
        <ScrollView>
          <View style={styles.userInfo}>
            {currentUser && (
              <Image
                source={{ uri: currentUser.avatar }}
                style={styles.userImg}
              />
            )}
            <View style={styles.userInfoText}>
              <Text style={styles.userName}>
                {currentUser
                  ? `${currentUser.firstName} ${currentUser.lastName}`
                  : "user"}
              </Text>
              <View style={styles.privacyContainer}>
                <View style={[styles.privacyPicker, { marginRight: 5 }]}>
                  <Paragraph style={styles.privacyText}>
                    Post in:{" "}
                    <Paragraph style={{ fontWeight: "bold" }}>
                      {group
                        ? group.name
                        : userDetail
                        ? userDetail.name
                        : post.privacy}
                    </Paragraph>
                  </Paragraph>
                </View>
                <View style={styles.privacyPicker}>
                  <TouchableOpacity
                    style={{ justifyContent: "center", flex: 1 }}
                    onPress={openMenu}
                  >
                    <Text style={styles.privacyText}>{post.privacy}</Text>
                  </TouchableOpacity>
                  <Menu
                    visible={visible}
                    onDismiss={closeMenu}
                    anchor={
                      <View
                        style={{
                          alignItems: "center",
                          justifyContent: "center",
                          marginLeft: 5,
                        }}
                      >
                        <IconButton
                          onPress={group ? () => {} : openMenu}
                          icon={group ? "account-group" : privacyButton}
                          color={Colors.PRIMARY_COLOR_LIGHTER}
                          size={20}
                          style={{ padding: 0, margin: 0, marginRight: 5 }}
                        />
                      </View>
                    }
                  >
                    <Menu.Item
                      icon="earth"
                      onPress={() => changePrivacy("public")}
                      title="public"
                    />
                    <Menu.Item
                      icon="lock"
                      onPress={() => changePrivacy("onlyme")}
                      title="onlyme"
                    />
                  </Menu>
                </View>
              </View>
              {/* {Platform.OS === "android" && (
              <Picker
                selectedValue={post.privacy}
                style={{ height: 30, width: 150 }}
                onValueChange={(itemValue, itemIndex) =>
                  setPost({ ...post, privacy: itemValue })
                }
              >
                <Picker.Item label="public" value="public" />
                <Picker.Item label="onlyme" value="onlyme" />
              </Picker>
            )} */}
            </View>
          </View>
          <View>
            <FormInput
              labelValue={post.content}
              onChangeText={(content) => setPost({ ...post, content: content })}
              placeholderText="What's on your mind?"
              withIcon={false}
              autoCapitalize="none"
              autoCorrect={false}
              multiline={true}
              numberOfLines={10}
              height={150}
              txtVertical="top"
            />
            <FlatList
              data={images}
              horizontal={true}
              renderItem={renderRow}
              keyExtractor={(item, index) => index.toString()}
              showsVerticalScrollIndicator={false}
              style={{ paddingBottom: 10 }}
            />
          </View>
          <View>
            <Camera
              ref={(ref) => setCamera(ref)}
              /* style={styles.camera} */
              type={camType}
              ratio={"1:1"}
            />
          </View>
          <View>
            <View
              style={{
                backgroundColor: Colors.ACCENT_COLOR_LIGHTER,
                padding: 0,
              }}
            >
              <Button
                style={{
                  backgroundColor: Colors.ACCENT_COLOR_LIGHTER,
                  color: Colors.ACCENT_COLOR_DARK,
                  padding: 5,
                  elevation: 0,
                }}
                color={Colors.ACCENT_COLOR_DARK}
                icon="camera"
                labelStyle={{ fontSize: 18, color: Colors.ACCENT_COLOR }}
                mode="contained"
                onPress={() => {
                  setImages([]),
                    navigation.navigate("ImageBrowserScreen", { item: toEdit });
                }}
              >
                Add Image
              </Button>
            </View>
            <View
              style={{
                backgroundColor: Colors.PRIMARY_COLOR,
                padding: 0,
                marginTop: 10,
              }}
            >
              <Button
                style={{
                  backgroundColor: Colors.PRIMARY_COLOR,
                  color: "white",
                  padding: 5,
                  elevation: 0,
                }}
                color={"white"}
                icon="send"
                labelStyle={{ fontSize: 18, color: "white" }}
                disabled={isLoading}
                loading={isLoading}
                onPress={() => {
                  setIsLoading(true);
                  savePosts();
                }}
              >
                POST
              </Button>
            </View>
          </View>
          <View style={{ marginTop: 100 }} />
        </ScrollView>
      </View>
    </KeyboardAvoidingView>
  );
};

export default AddPostScreens;

const styles = StyleSheet.create({
  userInfo: {
    flexDirection: "row",
    justifyContent: "flex-start",
    padding: 15,
  },
  userImg: {
    width: 50,
    height: 50,
    borderRadius: 5,
  },
  postImage: {
    width: 100,
    height: 100,
    marginLeft: 5,
    borderRadius: 5,
  },
  userInfoText: {
    flexDirection: "column",
    marginLeft: 10,
    marginRight: 15,
  },
  userName: {
    fontSize: 14,
    fontWeight: "bold",
    paddingRight: 5,
  },
  postImg: {
    width: windowWidth,
    height: windowWidth / 3,
  },
  camera: {
    flex: 1,
    aspectRatio: 1,
  },
  privacyPicker: {
    borderRadius: 2,
    borderWidth: 1,
    borderColor: Colors.GRAY400,
    justifyContent: "center",
    flexDirection: "row",
    paddingHorizontal: 5,
  },
  privacyContainer: {
    flex: 1,
    flexDirection: "row",
  },
  privacyText: {
    textAlignVertical: "center",
    alignSelf: "center",
    paddingHorizontal: 5,
  },
});
