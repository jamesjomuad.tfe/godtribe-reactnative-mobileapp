import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  ScrollView,
  FlatList,
  RefreshControl,
} from "react-native";
import { Headline, Paragraph, Title, Surface } from "react-native-paper";
import { formatGroupRole } from "../../Common/Utils/Format";
import CollapsibleHeader1 from "../../Common/Components/CollapsibleHeader1";
import { GET_GROUP_ACTIVITIES_BUDDYBOSS } from "../../Common/api/index";
import PostCard from "../../Common/Components/PostCard";
import { useSelector, useDispatch } from "react-redux";
import AddPostCard from "../../Common/Components/AddPostCard";

const GroupPage = ({ navigation, route }) => {
  const currentUser = useSelector((state) => state.userState.currentUser);
  const postState = useSelector((state) => state.userState.posts);
  const [group, setGroup] = useState(null);
  const [chosen, setChosen] = useState("ACTIVITY");
  const [isLoading, setIsLoading] = useState(false);
  const [activities, setActivities] = useState([]);
  const [page, setPage] = useState(1);

  const renderRow = ({ item }) => {
    return (
      <PostCard
        item={item}
        navigation={navigation}
        hidden={false}
        viewProfile={(id) => navigation.navigate("Profile", { id })}
        editPress={() => navigation.navigate("AddPost", { item, group: group })}
        feedLoad={decideToGetActivities}
        commentPress={() => navigation.navigate("AddComment", { item })}
        imgPress={(images) =>
          navigation.navigate("ImageScreen", { images: images, item })
        }
      />
    );
  };

  const renderFooter = () => {
    return (
      <Surface>
        <CollapsibleHeader1
          choices={["ACTIVITY", "CAMPFIRE", "DISCUSSIONS", "ACTIVE MEMBERS"]}
          chosen={chosen}
          setChosen={setChosen}
          withPicture={group?.avatar_urls?.full}
        />
      </Surface>
    );
  };

  const renderCover = () => {
    return (
      <View>
        <ImageBackground
          source={{
            uri:
              group?.cover_url ||
              "https://images.unsplash.com/photo-1612178537253-bccd437b730e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1374&q=80",
          }}
          resizeMode="cover"
          style={{
            width: "100%",
            height: 250,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <View
            style={{
              backgroundColor: "rgba(0, 0, 0, 0.5)",
              justifyContent: "center",
              margin: 10,
              padding: 10,
              borderRadius: 5,
              alignItems: "center",
            }}
          >
            <Title
              style={{
                fontSize: 32,
                fontWeight: "bold",
                color: "white",
              }}
            >
              {group?.name}
            </Title>
            <Paragraph
              style={{
                textAlign: "center",
                fontWeight: "bold",
                color: "white",
              }}
            >
              {group?.description.raw}
            </Paragraph>
            {formatGroupRole(group?.role) && (
              <View
                style={{
                  paddingHorizontal: 15,
                  paddingVertical: 0,
                  borderColor: "white",
                  borderWidth: 1,
                  borderRadius: 5,
                  marginTop: 20,
                }}
              >
                <Title
                  style={{
                    textAlign: "center",
                    fontWeight: "bold",
                    color: "white",
                  }}
                >
                  {formatGroupRole(group?.role)}
                </Title>
              </View>
            )}
          </View>
        </ImageBackground>
        {activities.length > 0 && (
          <AddPostCard
            user={currentUser}
            titlePlaceholder={`Tell everyone what's on your mind`}
            addPost={() => navigation.navigate("AddPost", { group: group })}
            viewProfile={() => console.log("Profile")}
          />
        )}
      </View>
    );
  };

  const getActivities = async () => {
    setIsLoading(true);
    try {
      const res = await GET_GROUP_ACTIVITIES_BUDDYBOSS(group?.id, page);
      if (res.success) {
        setActivities(res.success);
      }
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  const decideToGetActivities = () => {
    if ((group?.status == "public" || group?.role !== "") && group?.id) {
      getActivities();
    }
  };

  useEffect(() => {
    decideToGetActivities();
  }, [postState]);

  useEffect(() => {
    if (route.params?.groupDetail) {
      setGroup(route.params?.groupDetail);
      /* console.log(route.params?.groupDetail); */
    }
  }, [route.params?.groupDetail]);

  useEffect(() => {
    navigation.setOptions({
      title: group?.name,
      headerTintColor: "white",
    });

    decideToGetActivities();
  }, [group]);

  useEffect(() => {}, []);

  return (
    <View>
      <FlatList
        ListHeaderComponent={renderCover}
        nestedScrollEnable={true}
        data={activities}
        /* ListHeaderComponent={headerComponent} */
        renderItem={renderRow}
        keyExtractor={(item, index) => index.toString()}
        /* onRefresh={handleRefresh} */
        /* onEndReached={() => {
          !isRefresh && handleLoadMore();
        }} */

        /* onEndReached={() => {
          !isLoading && handleLoadMore();
        }}
        onEndReachedThreshold={0.5}
        ListFooterComponent={renderFooter} */
        /* onMomentumScrollBegin={() => setMomentum(false)} */
        ListFooterComponent={renderFooter}
        refreshControl={
          <RefreshControl refreshing={isLoading} onRefresh={() => {}} />
        }
      />
    </View>
  );
};

export default GroupPage;

const styles = StyleSheet.create({});
