import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  FlatList,
  Button,
  TouchableOpacity,
  Image,
  SafeAreaView,
  KeyboardAvoidingView,
  Platform,
} from "react-native";
import {
  GETCOMMENT_POSTS,
  POST_COMMENT,
  POST_REPLY_COMMENT,
} from "../../Common/api";
import { useSelector, useDispatch } from "react-redux";
import * as Colors from "../../Common/Styles/Colors";
import FormInput from "../../Common/Components/FormInput";
import CommentPostCard from "../../Common/Components/CommentPostCard";
import { Camera } from "expo-camera";
import * as ImagePicker from "expo-image-picker";
import { refreshComment } from "../../Common/Redux/Actions/post";

const AddCommentScreen = ({ route }) => {
  const item = route.params.item;
  const auth = useSelector((state) => state.authState.currentAuth);
  const currentUser = useSelector((state) => state.userState.currentUser);
  const commentState = useSelector((state) => state.userState.commenting);
  const [comment, setComment] = useState(null);
  const [page, setPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const [isRefresh, setIsRefresh] = useState(false);
  const [momentum, setMomentum] = useState(true);
  const [text, setText] = useState();
  const [replyID, setReplyID] = useState();
  const [image, setImage] = useState();
  const [hasGalleryPermission, setHasGalerryPermission] = useState(null);
  const [hasCameraPermission, setHasCameraPermission] = useState(null);
  const dispatch = useDispatch();

  useEffect(() => {
    getComments();
  }, []);

  const onReply = (value) => {
    setReplyID(value);
  };
  // on delete comment is unavailable - no api
  const onDeleteComment = async (value) => {
    await POST_COMMENT(value.id, text, auth);
  };
  const getComments = () => {
    try {
      const getComment = async () => {
        const data = await GETCOMMENT_POSTS(item.id, auth);
        if (data) {
          setComment(data);
          setIsRefresh(false);
        } else {
          setIsRefresh(false);
        }
      };
      getComment();
    } catch (error) {}
  };

  const renderRow = ({ item }) => {
    return (
      <CommentPostCard
        item={item}
        onReply={(value) => onReply(value)}
        onDeleteComment={(value) => {}}
      />
    );
  };

  const handleLoadMore = async () => {};
  const renderFooter = () => {
    return isLoading ? (
      <View style={styles.loader}>
        <ActivityIndicator
          size="small"
          animating={true}
          color={Colors.ACCENT_COLOR}
          style={{ padding: 5 }}
        />
      </View>
    ) : (
      <Text
        style={{
          alignSelf: "center",
          textAlign: "center",
          color: Colors.WHITE03,
          padding: 10,
        }}
      >
        {item ? "End of Comments ..." : "Loading Comments ..."}
      </Text>
    );
  };

  const handleRefresh = async () => {
    setIsRefresh(true);
    setMomentum(true);
    getComments();
  };

  const submitComment = () => {
    const upload = async () => {
      if (replyID) {
        const res = await POST_REPLY_COMMENT(item.id, replyID.id, text, auth);
        dispatch(refreshComment(res));
      } else if (text || image) {
        const res = await POST_COMMENT(item.id, text, auth);
        dispatch(refreshComment(res));
      } else {
      }
    };
    upload();
    resetInputs();
  };

  const resetInputs = () => {
    setImage(null);
    setReplyID(null);
    setText("");
  };

  const removeReplyID = () => {
    setReplyID(null);
  };

  const choosePhotoFromGallery = async () => {
    try {
      const galleryStatus =
        await ImagePicker.requestMediaLibraryPermissionsAsync();
      setHasGalerryPermission(galleryStatus.status === "granted");
      let imageList = [];
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 1,
      });
      if (!result.cancelled) {
        setImage(result.uri);
      }
    } catch (error) {}
  };

  useEffect(() => {
    handleRefresh();
  }, [commentState]);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
        <KeyboardAvoidingView
          behavior={Platform.OS === "ios" ? "padding" : null}
          style={{ flex: 1 }}
          keyboardVerticalOffset={Platform.select({ ios: 88, android: 500 })}
        >
          {/* {Platform.OS === "ios" && ( */}
          {/* <View>
              {replyID ? (
                <TouchableOpacity
                  style={styles.replyCard}
                  onPress={() => removeReplyID()}
                >
                  <Text>Replying to {replyID.name} </Text>
                  <Text style={{ color: Colors.ACCENT_COLOR }}>remove</Text>
                </TouchableOpacity>
              ) : null}

              <View style={styles.writeTaskWrapper}>
                <TextInput
                  value={text}
                  placeholder="Comment..."
                  style={styles.input}
                  onChangeText={(text) => {
                    setText(text);
                  }}
                  multiline={true}
                />
                <TouchableOpacity
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: Colors.ACCENT_COLOR,
                  }}
                  onPress={() => submitComment()}
                >
                  <Text
                    style={{ fontWeight: "bold", color: Colors.WHITE_COLOR }}
                  >
                    POST
                  </Text>
                </TouchableOpacity>
              </View>
            </View> */}
          {/* // )} */}
          <FlatList
            nestedScrollEnable={true}
            data={comment}
            renderItem={renderRow}
            keyExtractor={(item, index) => index.toString()}
            showsVerticalScrollIndicator={false}
            refreshing={isRefresh}
            onRefresh={handleRefresh}
            onEndReached={() => {
              !isRefresh && handleLoadMore();
            }}
            onEndReachedThreshold={0.5}
            ListFooterComponent={renderFooter}
            onMomentumScrollBegin={() => setMomentum(false)}
          />
          {/* {Platform.OS === "android" && ( */}
          <View>
            {replyID ? (
              <TouchableOpacity
                style={styles.replyCard}
                onPress={() => removeReplyID()}
              >
                <Text>Replying to {replyID.name} </Text>
                <Text style={{ color: Colors.ACCENT_COLOR }}>remove</Text>
              </TouchableOpacity>
            ) : null}

            <View style={styles.writeTaskWrapper}>
              <TextInput
                value={text}
                placeholder="Comment..."
                style={styles.input}
                onChangeText={(text) => {
                  setText(text);
                }}
                multiline={true}
              />
              <TouchableOpacity
                style={{
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center",
                  backgroundColor: Colors.ACCENT_COLOR,
                }}
                onPress={() => submitComment()}
              >
                <Text style={{ fontWeight: "bold", color: Colors.WHITE_COLOR }}>
                  POST
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          {/* )} */}
        </KeyboardAvoidingView>
      </View>
    </SafeAreaView>
  );
};

export default AddCommentScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  cardContainer: {
    backgroundColor: "#f8f8f8",
  },
  card: {
    flexDirection: "row",
    backgroundColor: "#f8f8f8",
    marginBottom: 5,
  },
  cardInner: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  userImg: {
    width: 50,
    height: 50,
    borderRadius: 5,
  },
  writeTaskWrapper: {
    flexDirection: "row",
    /* padding: 10, */
  },
  input: {
    width: "80%",
    paddingVertical: 10,
    paddingHorizontal: 15,
    backgroundColor: Colors.WHITE_COLOR,
    borderColor: "#C0C0C0",
    borderWidth: 1,
  },
  addWrapper: {
    width: 60,
    height: 60,
    backgroundColor: "#FFF",
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#C0C0C0",
    borderWidth: 1,
    borderRadius: 60,
  },
  replyCard: {
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 10,
    backgroundColor: Colors.WHITE_COLOR,
  },
  commentWrapper: {
    backgroundColor: "green",
  },
  commentImg: { width: 50, height: 50, borderRadius: 10 },
});
