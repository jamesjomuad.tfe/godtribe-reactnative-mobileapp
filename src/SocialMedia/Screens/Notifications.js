import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  RefreshControl,
} from "react-native";
import { useSelector } from "react-redux";
import { GET_NOTIFICATION } from "../../Common/api";
import RenderHtml from "react-native-render-html";
import { windowWidth } from "../../Common/Utils/Dimentions";
import { Divider, Paragraph } from "react-native-paper";
import * as Colors from "../../Common/Styles/Colors";

var RefreshVar;

const Notifications = ({ navigation }) => {
  const currentUser = useSelector((state) => state.userState.currentUser);
  const [notifications, setNotifications] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const getNotifs = async () => {
    try {
      const res = await GET_NOTIFICATION(currentUser.id, 1);
      if (res.success) {
        setNotifications(res.success);
        /* console.log(res.success); */
      }
    } catch (error) {}
  };
  const loadNotifs = async () => {
    setIsLoading(true);
    try {
      await getNotifs();
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };
  function autoRefresh() {
    var tme = Math.floor(Math.random() * 101) + 900;
    RefreshVar = setInterval(getNotifs, tme);
    /* console.log("refreshing start id:", RefreshVar); */
  }

  function stopRefresh() {
    /* console.log("stop fetching notifs"); */
    /* console.log("stopping refresh id:", RefreshVar); */
    clearInterval(RefreshVar);
  }

  useEffect(() => {
    loadNotifs();
    autoRefresh();
    return () => {
      stopRefresh();
    };
  }, []);

  return (
    <ScrollView
      contentContainerStyle={{ flex: 0 }}
      refreshControl={
        <RefreshControl refreshing={isLoading} onRefresh={loadNotifs} />
      }
      style={{ backgroundColor: "white" }}
    >
      {notifications.length > 0 ? (
        notifications.map((notif) => (
          <View
            key={notif.id}
            style={{
              width: windowWidth,
              alignItems: "center",
              paddingLeft: 20,
              paddingRight: 20,
            }}
          >
            <View
              style={{
                width: windowWidth,
                alignItems: "center",
                padding: 15,
              }}
            >
              <RenderHtml
                contentWidth={windowWidth}
                source={{ html: notif.description.rendered }}
                style={{ width: windowWidth - 20 }}
                tagsStyles={tagsStyles.contentHead}
                enableExperimentalMarginCollapsing={true}
              />
            </View>
            <Divider
              style={{
                marginBottom: 5,
                alignSelf: "stretch",
                backgroundColor: "#000",
              }}
            />
          </View>
        ))
      ) : (
        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <Paragraph style={{ textAlign: "center", color: Colors.GRAY500 }}>
            {isLoading ? "LOADING" : "NO"} NOTIFICATIONS {isLoading && "..."}
          </Paragraph>
        </View>
      )}
    </ScrollView>
  );
};

const tagsStyles = {
  contentHead: {
    body: {
      whiteSpace: "normal",
      color: "gray",
    },
    a: {
      color: "black",
      fontWeight: "bold",
      textDecorationLine: "none",
    },
  },
  contentRendered: {
    blockquote: {
      backgroundColor: Colors.GRAY200,
      margin: 0,
      padding: 10,
      fontStyle: "italic",
      fontSize: 20,
    },
    p: {
      color: "black",
    },
  },
};

const renderersProps = {
  a: {
    onPress: onPress,
  },
};

function onPress(event, href) {}

export default Notifications;

const styles = StyleSheet.create({});
