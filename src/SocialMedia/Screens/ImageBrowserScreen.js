import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  SafeAreaView,
} from "react-native";
import * as ImageManipulator from "expo-image-manipulator";
import { ImageBrowser } from "expo-image-picker-multiple";
import * as MediaLibrary from "expo-media-library";
const ImageBrowserScreen = ({ navigation, route }) => {
  const [toEdit, setToEdit] = useState();

  _getHeaderLoader = () => (
    <ActivityIndicator
      size="small"
      color={"#0580FF"}
      style={{ paddingRight: 15 }}
    />
  );

  useEffect(() => {
    if (route.params?.item) {
      setToEdit(route.params?.item);
    } else {
      setToEdit(null);
    }
  }, [route.params?.item]);

  const imagesCallback = (callback) => {
    navigation.setOptions({
      headerRight: () => _getHeaderLoader(),
    });

    callback
      .then(async (photos) => {
        const cPhotos = [];
        for (let photo of photos) {
          const pPhoto = await _processImageAsync(photo.uri);
          cPhotos.push({
            uri: pPhoto.uri,
            name: photo.filename,
            type: "image/jpg",
          });
        }
        navigation.navigate("AddPost", {
          item: toEdit ? toEdit : null,
          photos: cPhotos,
        });
      })
      .catch((e) => console.log(e));
  };
  _processImageAsync = async (uri) => {
    const file = await ImageManipulator.manipulateAsync(
      uri,
      [{ resize: { width: 1000 } }],
      { compress: 0.8, format: ImageManipulator.SaveFormat.JPEG }
    );
    return file;
  };
  _renderDoneButton = (count, onSubmit) => {
    if (!count) return null;
    return (
      <TouchableOpacity title={"Done"} onPress={onSubmit}>
        <Text
          onPress={onSubmit}
          style={{
            color: "white",
            fontSize: 18,
            fontWeight: "bold",
            marginRight: 15,
          }}
        >
          Done
        </Text>
      </TouchableOpacity>
    );
  };
  const updateHandler = (count, onSubmit) => {
    navigation.setOptions({
      title: `Selected ${count} files`,
      headerRight: () => _renderDoneButton(count, onSubmit),
    });
  };

  const renderSelectedComponent = (number) => (
    <View style={styles.countBadge}>
      <Text style={styles.countBadgeText}>{number}</Text>
    </View>
  );
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={[styles.flex, styles.container]}>
        <ImageBrowser
          onChange={updateHandler}
          callback={imagesCallback}
          renderSelectedComponent={renderSelectedComponent}
          mediaType={MediaLibrary.MediaType.photo}
        />
      </View>
    </SafeAreaView>
  );
};

export default ImageBrowserScreen;

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  container: {
    position: "relative",
  },
  emptyStay: {
    textAlign: "center",
  },
  countBadge: {
    paddingHorizontal: 8.6,
    paddingVertical: 5,
    borderRadius: 50,
    position: "absolute",
    right: 3,
    bottom: 3,
    justifyContent: "center",
    backgroundColor: "#0580FF",
  },
  countBadgeText: {
    fontWeight: "bold",
    alignSelf: "center",
    padding: "auto",
    color: "#ffffff",
  },
});
