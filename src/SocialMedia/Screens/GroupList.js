import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ScrollView,
  RefreshControl,
  TouchableOpacity,
} from "react-native";
import { GET_ALL_GROUPS_BUDDYBOSS } from "../../Common/api";
import GroupCard from "../../Common/Components/GroupCard";
import HeadSpacer from "../../Common/Components/HeadSpacer";
import Header from "../../Common/Components/Header";
import { generalStyle } from "../../Common/Styles/GeneralStyles";
import { ActivityIndicator, Badge, Paragraph } from "react-native-paper";
import { Colors } from "../../Common/Styles/Colors";
import { useSelector } from "react-redux";

const GroupList = ({ navigation, route }) => {
  const userGroups = useSelector((state) => state.userState.userGroups);
  const [page, setPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [groups, setGroups] = useState([]);
  const [baseUserGroups, setBaseUserGroups] = useState([]);
  const [baseGroups, setBaseGroups] = useState([]);
  const [isOwned, setIsOwned] = useState(null);
  const [isAllGroups, setIsAllGroups] = useState(true);

  /* const setAllGroupsAsCurrent = () => {
    setCurrentGroups([]);
    setCurrentGroups(groups);
  };
  const setMyGroupsAsCurrent = () => {
    setCurrentGroups([]);
    setCurrentGroups(userGroups);
  }; */

  const setAllGroups = async () => {
    setIsLoading(true);
    setGroups([]);
    setBaseGroups([]);
    try {
      const res = await GET_ALL_GROUPS_BUDDYBOSS(1);

      if (res.success) {
        setGroups(res.success);
        setBaseGroups(res.success);
        setPage(1);
      }
    } catch (error) {}
    setIsLoading(false);
  };

  const refreshList = () => {
    setAllGroups();
  };

  const handleLoadMore = async () => {
    setIsRefreshing(true);
    try {
      const res = await GET_ALL_GROUPS_BUDDYBOSS(page + 1);

      if (res.success) {
        const allNewGroups = groups.concat(res.success);
        setGroups(allNewGroups);
        setBaseGroups(allNewGroups);
        setPage(page + 1);
      }
    } catch (error) {}
    setIsRefreshing(false);
  };

  const renderRow = ({ item }) => {
    return (
      <GroupCard
        item={item}
        navigation={navigation}
        setAllGroups={setAllGroups}
      />
    );
  };

  const renderFooter = () => {
    return isRefreshing ? (
      <View>
        <ActivityIndicator
          size="small"
          animating={true}
          color={Colors.ACCENT_COLOR}
          style={{ padding: 5, marginBottom: 10 }}
        />
      </View>
    ) : (
      <Text
        style={{
          alignSelf: "center",
          textAlign: "center",
          color: Colors.WHITE01,
          padding: 5,
          marginBottom: 10,
        }}
      >
        End of Posts ...
      </Text>
    );
  };

  useEffect(() => {
    setBaseUserGroups(userGroups);
  }, [userGroups]);

  useEffect(() => {
    if (route?.params?.isOwned) {
      setIsOwned(true);
    } else {
      setIsOwned(true);
    }
  }, [route?.params?.isOwned]);

  useEffect(() => {
    if (isOwned) {
    } else {
      setAllGroups(0);
    }
  }, [isOwned]);

  return (
    <View style={generalStyle.wrapper}>
      <View style={generalStyle.container}>
        <HeadSpacer />
        <View
          style={{ position: "absolute", top: 0, zIndex: 999, width: "100%" }}
        >
          <Header
            screen={"Groups"}
            navigateTo={(section) => navigation.jumpTo(section)}
            navigation={navigation}
          />
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-around",
            width: "100%",
            marginVertical: 15,
          }}
        >
          <TouchableOpacity
            style={{
              flexDirection: "row",
            }}
            onPress={() => setIsAllGroups(true)}
          >
            <Paragraph
              style={{ color: isAllGroups ? "black" : Colors.GRAY400 }}
            >
              All Groups{" "}
            </Paragraph>
            <Badge
              style={{
                backgroundColor: isAllGroups
                  ? Colors.ACCENT_COLOR
                  : Colors.GRAY600,
              }}
            >
              {groups?.length}
            </Badge>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setIsAllGroups(false)}
            style={{
              flexDirection: "row",
            }}
          >
            <Paragraph
              style={{ color: !isAllGroups ? "black" : Colors.GRAY400 }}
            >
              My Groups{" "}
            </Paragraph>
            <Badge
              style={{
                backgroundColor: !isAllGroups
                  ? Colors.ACCENT_COLOR
                  : Colors.GRAY600,
              }}
            >
              {userGroups?.length}
            </Badge>
          </TouchableOpacity>
        </View>
        {isAllGroups ? (
          <FlatList
            nestedScrollEnable={true}
            data={groups}
            /* ListHeaderComponent={headerComponent} */
            renderItem={renderRow}
            keyExtractor={(item, index) => index.toString()}
            /* onRefresh={handleRefresh} */
            /* onEndReached={() => {
            !isRefresh && handleLoadMore();
          }} */

            onEndReached={() => {
              !isLoading && handleLoadMore();
            }}
            onEndReachedThreshold={0.5}
            ListFooterComponent={renderFooter}
            /* onMomentumScrollBegin={() => setMomentum(false)} */

            refreshControl={
              <RefreshControl refreshing={isLoading} onRefresh={refreshList} />
            }
          />
        ) : (
          <FlatList
            nestedScrollEnable={true}
            data={userGroups}
            /* ListHeaderComponent={headerComponent} */
            renderItem={renderRow}
            keyExtractor={(item, index) => index.toString()}
            /* onRefresh={handleRefresh} */
            /* onEndReached={() => {
          !isRefresh && handleLoadMore();
        }} */

            /* onEndReached={() => {
          !isLoading && handleLoadMore();
        }}
        onEndReachedThreshold={0.5}
        ListFooterComponent={renderFooter} */
            /* onMomentumScrollBegin={() => setMomentum(false)} */

            /* refreshControl={
          <RefreshControl refreshing={isLoading} onRefresh={refreshList} />
        } */
          />
        )}
      </View>
    </View>
  );
};

export default GroupList;

const styles = StyleSheet.create({});
