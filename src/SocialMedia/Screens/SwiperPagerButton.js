import React, { useRef, useState } from "react";
import {
  Animated,
  Dimensions,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import {
  IconButton,
  Surface,
  Provider,
  Portal,
  Menu,
  Divider,
  Button,
  Subheading,
} from "react-native-paper";
import ProfileContent from "../../Common/Components/ProfileContent";
import * as Colors from "../../Common/Styles/Colors";

const { width } = Dimensions.get("window");

export default function SwiperPagerButton({ userDetail, isOwned, navigation }) {
  /*  console.log("from swiper", userDetail); */
  const scrollX = useRef(new Animated.Value(0)).current;
  const buttons = ["ACTIVITY", "PROFILE", "FRIENDS", "GROUPS"];
  const onCLick = (i) => this.scrollView.scrollTo({ x: i * width });
  return (
    <View style={styles.container}>
      <View style={{ padding: 5, paddingTop: 0 }}>
        <ButtonContainer
          buttons={buttons}
          onClick={onCLick}
          scrollX={scrollX}
        />
      </View>
      <ScrollView
        ref={(e) => (this.scrollView = e)}
        horizontal
        pagingEnabled
        decelerationRate="fast"
        showsHorizontalScrollIndicator={false}
        nestedScrollEnabled
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { x: scrollX } } }],
          { useNativeDriver: false }
        )}
      >
        {buttons.map((x) => (
          <ProfileContent
            key={x}
            contentKey={x}
            userDetail={userDetail}
            isOwned={isOwned}
            navigation={navigation}
          />
        ))}
      </ScrollView>
    </View>
  );
}

function ButtonContainer({ buttons, onClick, scrollX }) {
  // for More OPTIONS
  const [visible, setVisible] = useState(false);
  const openMenu = () => setVisible(true);
  const closeMenu = () => setVisible(false);

  const [btnContainerWidth, setWidth] = useState(0);
  const btnWidth = btnContainerWidth / buttons.length;
  const translateX = scrollX.interpolate({
    inputRange: [0, width],
    outputRange: [0, btnWidth],
  });
  const translateXOpposit = scrollX.interpolate({
    inputRange: [0, width],
    outputRange: [0, -btnWidth],
  });
  return (
    <Surface
      style={{
        elevation: 3,
        flexDirection: "row",
        flex: 1,
        borderRadius: 10,
        marginVertical: 5,
        alignItems: "center",
      }}
    >
      <View
        style={styles.btnContainer}
        onLayout={(e) => setWidth(e.nativeEvent.layout.width)}
      >
        {buttons.map((btn, i) => (
          <TouchableOpacity
            key={btn}
            style={styles.btn}
            onPress={() => onClick(i)}
          >
            <Text>{btn}</Text>
          </TouchableOpacity>
        ))}

        <Animated.View
          style={[
            styles.animatedBtnContainer,
            { width: btnWidth, transform: [{ translateX }] },
          ]}
        >
          {buttons.map((btn) => (
            <Animated.View
              key={btn}
              style={[
                styles.animatedBtn,
                {
                  width: btnWidth,
                  transform: [{ translateX: translateXOpposit }],
                },
              ]}
            >
              <Text style={styles.btnTextActive}>{btn}</Text>
            </Animated.View>
          ))}
        </Animated.View>
      </View>
      {/* <IconButton
        icon="dots-vertical"
        onPress={() => {}}
        style={{ margin: 0, padding: 0, alignSelf: "center" }}
      >
        <Text>:</Text>
      </IconButton> */}
      {/* <Menu
        visible={visible}
        onDismiss={closeMenu}
        anchor={
          <IconButton
            onPress={openMenu}
            icon="dots-vertical"
            style={{ margin: 0, padding: 0, alignSelf: "center" }}
          ></IconButton>
        }
      >
        <Menu.Item onPress={closeMenu} title="REPORT" />
        <Menu.Item onPress={closeMenu} title="BLOCK" />
      </Menu> */}
    </Surface>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 5,
  },
  btnContainer: {
    height: 40,
    borderRadius: 10,
    overflow: "hidden",
    flexDirection: "row",
    backgroundColor: "white",
    flex: 1,
  },
  btn: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  animatedBtnContainer: {
    height: 40,
    flexDirection: "row",
    position: "absolute",
    overflow: "hidden",
    backgroundColor: "#444",
  },
  animatedBtn: {
    height: 40,
    justifyContent: "center",
    alignItems: "center",
  },
  btnTextActive: {
    color: "#fff",
    fontWeight: "bold",
  },
  card: {
    width: width - 10,
    height: 500,
    marginHorizontal: 5,
    borderRadius: 5,
    backgroundColor: Colors.WHITE02,
  },
});
