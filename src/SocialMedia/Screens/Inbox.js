import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  RefreshControl,
  FlatList,
} from "react-native";
import { GET_MESSAGES } from "../../Common/api";
import ArrowLeft from "../../Common/Components/ArrowLeft";
import ChatCard from "../../Common/Components/ChatCard";
import * as Colors from "../../Common/Styles/Colors";

var RefreshVar;

const Inbox = ({ navigation }) => {
  const [messages, setMessages] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const getInboxChat = async () => {
    try {
      const res = await GET_MESSAGES();
      if (res.success) {
        setMessages(res.success);
        /*  console.log(res.success[0].avatar[0].full); */
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getMsgs = async () => {
    setIsLoading(true);
    try {
      await getInboxChat();
    } catch (error) {}
    setIsLoading(false);
  };

  function autoRefresh() {
    var tme = Math.floor(Math.random() * 101) + 4900;
    RefreshVar = setInterval(getInboxChat, tme);
  }

  function stopRefresh() {
    console.log("stop fetching inbox chats");
    console.log("stopping refresh id:", RefreshVar);
    clearInterval(RefreshVar);
  }

  useEffect(() => {
    getMsgs();
    autoRefresh();
    return () => {
      stopRefresh();
    };
  }, []);

  const renderRow = ({ item }) => {
    return <ChatCard chat={item} navigation={navigation} />;
  };

  return (
    <View style={styles.container}>
      <FlatList
        nestedScrollEnable={true}
        data={messages}
        renderItem={renderRow}
        keyExtractor={(item, index) => index.toString()}
        /* onEndReached={() => {
              !isLoading && handleLoadMore();
            }} */
        /* onEndReachedThreshold={0.5} */
        /* ListFooterComponent={renderFooter} */
        refreshControl={
          <RefreshControl refreshing={isLoading} onRefresh={getMsgs} />
        }
      />
    </View>
  );
};

export default Inbox;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 5,
    paddingRight: 5,
  },
  header: {
    backgroundColor: Colors.PRIMARY_COLOR,
    flexDirection: "row",
    paddingVertical: 5,
  },
  headerTitle: {
    justifyContent: "center",
  },
  headerText: {
    color: "white",
    fontWeight: "bold",
    textAlignVertical: "center",
    fontSize: 20,
  },
  contentContainer: {
    flex: 1,
    paddingHorizontal: 15,
  },
});
