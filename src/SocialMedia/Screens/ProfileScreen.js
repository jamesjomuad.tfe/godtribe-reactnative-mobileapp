import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  ScrollView,
  SafeAreaView,
  Linking,
  TouchableOpacity,
  FlatList,
  RefreshControl,
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import * as Colors from "../../Common/Styles/Colors";
import * as api from "../../Common/api/axios";
import RenderHtml from "react-native-render-html";
import { windowWith } from "../../Common/Utils/Dimentions";
import LoadingProfile from "../../Common/Components/LoadingProfile";
import {
  Button,
  Paragraph,
  Title,
  Surface,
  Divider,
  FAB,
  Portal,
  IconButton,
  Caption,
  Menu,
} from "react-native-paper";
import urls from "../../Common/api/urls";
import CollapsibleHeader1 from "../../Common/Components/CollapsibleHeader1";
import {
  CANCEL_FRIENDSHIP,
  DELETE_FRIENDSHIP,
  FETCH_USERS_DATA,
  FOLLOW_USER,
  GET_MyACTIVITIES_BUDDYBOSS,
  POST_FRIENDSHIP,
  UPDATE_FRIENDSHIP,
} from "../../Common/api";
import PostCard from "../../Common/Components/PostCard";
import AddPostCard from "../../Common/Components/AddPostCard";
import ProfileImage from "../../Common/Components/ProfileImage";
import SwiperPagerButton from "./SwiperPagerButton";
import ProfileDetails from "../../Common/Components/ProfileDetails";
import ArrowLeft from "../../Common/Components/ArrowLeft";

const ProfileScreen = ({ route, navigation }) => {
  const passId = route.params == undefined ? false : route.params.id;
  const currentUser = useSelector((state) => state.userState.currentUser);
  const userDetail = useSelector((state) => state.userState.userDetail);
  const userActivities = useSelector((state) => state.userState.activities);
  const auth = useSelector((state) => state.authState.currentAuth);

  const [detail, setDetail] = useState();
  const [isOwned, setIsOwned] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  // for PENCIL indicator
  const [chosen, setChosen] = useState("PROFILE");
  const [yAxis, setYAxis] = useState(0);

  const [isFriend, setIsFriend] = useState(false);
  const [friendshipStatus, setFriendshipStatus] = useState(null);
  const [frLoading, setFrLoading] = useState(false);
  const [following, setFollowing] = useState(false);
  const [showImages, setShowImages] = useState(true);

  /* useEffect(() => {
    console.log(yAxis);
  }, [yAxis]); */

  const [state, setState] = useState({ open: false });
  const onStateChange = ({ open }) => setState({ open });
  const { open } = state;

  const disableChoice = () => {
    setIsMyActivity(false);
    setIsProfile(false);
  };

  const getUserDetail = async (passId) => {
    try {
      const res = await FETCH_USERS_DATA(passId);
      /* console.log("getUserDetail", res); */
      if (res) {
        setDetail(res);
      }
    } catch (error) {
      console.log("[ProfileScreen]", error);
    }
  };

  const postAddFriend = async () => {
    setFrLoading(true);
    if (friendshipStatus) {
      var res;

      try {
        if (friendshipStatus == "Add Friend") {
          res = await POST_FRIENDSHIP(currentUser.id, detail.id);
        }
        if (friendshipStatus == "Confirm") {
          res = await UPDATE_FRIENDSHIP(detail.friendship_id);
        }
        if (friendshipStatus == "Friends") {
          res = await DELETE_FRIENDSHIP(detail.id);
        }
        if (friendshipStatus == "Pending") {
          res = await CANCEL_FRIENDSHIP(detail.friendship_id);
          console.log(res);
        }

        if (res.success) {
          console.log(res);
          getUserDetail(passId);
        }
      } catch (error) {
        console.log("[ProfileScreen]", error);
      }
    }
    setFrLoading(false);
  };

  const postFollowing = async () => {
    setFrLoading(true);
    if (friendshipStatus) {
      try {
        const res = await FOLLOW_USER(
          detail.id,
          following ? "unfollow" : "follow"
        );

        if (res.success) {
          console.log(res);
          getUserDetail(passId);
        }
      } catch (error) {
        console.log("[ProfileScreen]", error);
      }
    }
    setFrLoading(false);
  };

  const loadInBrowser = () => {
    Linking.openURL(`${detail.link}`).catch((err) =>
      console.error("Couldn't load page", err)
    );
  };

  const onRefresh = async () => {
    setRefreshing(true);
    try {
      await getUserDetail(isOwned ? currentUser.id : detail.id);
    } catch (error) {
      console.log(error);
    }
    setRefreshing(false);
  };

  useEffect(() => {
    /* console.log("pass ID", passId, "current", currentUser.id); */
    if (passId) {
      if (passId == currentUser.id) {
        setIsOwned(true);
        /* getUserActivity(currentUser.id); */
        setDetail(userDetail);
      } else {
        setIsOwned(false);
        getUserDetail(passId);
      }
    } else {
      setIsOwned(true);
      /* getUserActivity(currentUser.id); */
      setDetail(userDetail);
    }
  }, []);

  /* console.log(detail); */
  /*  useEffect(() => {
    console.log("user activities", userActivities.length);
  }, [userActivities]); */

  useEffect(() => {
    if (detail) {
      /* console.log(
        "user detail",
        detail.friendship_status,
        detail.friendship_id,
        detail
      ); */

      if (detail.friendship_status == "not_friends") {
        setFriendshipStatus("Add Friend");
        setIsFriend(false);
      } else if (detail.friendship_status == "pending") {
        setFriendshipStatus("Pending");
        setIsFriend(false);
      } else if (detail.friendship_status == "awaiting_response") {
        setFriendshipStatus("Confirm");
        setIsFriend(false);
      } else if (detail.friendship_status == "is_friend") {
        // is_friend
        setFriendshipStatus("Friends");
        setIsFriend(true);
      } else {
        setFriendshipStatus("Error");
      }
      setFollowing(detail.is_following);
    }
  }, [detail]);

  const toggleImages = () => {
    setShowImages(!showImages);
  };

  /* useEffect(() => {
    if (friendshipStatus == "Friends") {
      setFollowing(true);
    } else setFollowing(false);
  }, [friendshipStatus]); */

  useEffect(() => {
    /* navigation.setOptions({
      headerLeft: () => (
        <ArrowLeft
          onPress={() => {
            setShowImages(false);
            navigation.goBack();
          }}
        />
      ),
    }); */
  }, []);

  if (detail) {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <ScrollView
          nestedScrollEnabled
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
          style={{
            flex: 1,
            backgroundColor: Colors.WHITE01,

            paddingTop: 10,
          }}
        >
          {/* {isOwned && yAxis > 500 && chosen == "MY ACTIVITY" && (
            <FAB
              style={styles.fab}
              icon="pencil"
              onPress={() => navigation.navigate("AddPost")}
            />
          )} */}
          <View style={{ paddingHorizontal: 5 }}>
            <Surface
              style={{
                elevation: 4,
                width: "100%",
                borderRadius: 10,
                paddingBottom: 20,
              }}
            >
              <View
                style={{
                  width: "100%",
                  borderTopLeftRadius: 10,
                  borderTopRightRadius: 10,
                  position: "relative",
                }}
              >
                {showImages && detail?.cover_url ? (
                  <ImageBackground
                    imageStyle={{
                      borderTopLeftRadius: 10,
                      borderTopRightRadius: 10,
                    }}
                    source={{ uri: detail?.cover_url }}
                    resizeMode="cover"
                    style={{
                      width: "100%",
                      height: 150,
                      marginBottom: 80,
                    }}
                  ></ImageBackground>
                ) : (
                  <View
                    style={{
                      borderTopLeftRadius: 10,
                      borderTopRightRadius: 10,
                      width: "100%",
                      height: 150,
                      marginBottom: 80,
                      backgroundColor: Colors.WHITE03,
                    }}
                  />
                )}
                <View
                  style={{
                    padding: 3,
                    backgroundColor: "white",
                    zIndex: 999,
                    alignSelf: "center",
                    borderRadius: 10,
                    position: "absolute",
                    top: "50%",
                    minHeight: 100,
                    minWidth: 100,
                  }}
                >
                  {showImages && <ProfileImage userData={detail} size={100} />}
                </View>
                {isOwned && (
                  <Button
                    mode="outlined"
                    icon="pen"
                    compact
                    style={{
                      borderColor: Colors.PRIMARY_COLOR,
                      position: "absolute",
                      top: 5,
                      right: 5,
                      zIndex: 999,
                    }}
                    labelStyle={{ color: Colors.PRIMARY_COLOR }}
                    /* onPress={() => console.log("edit profile")} */
                    onPress={loadInBrowser}
                  >
                    EDIT
                  </Button>
                )}
              </View>
              <View
                style={{
                  backgroundColor: "white",
                  alignItems: "center",
                  marginTop: 0,
                }}
              >
                <Text
                  style={{
                    fontWeight: "bold",
                    fontSize: 24,
                    color: Colors.PRIMARY_COLOR,
                  }}
                >
                  {detail?.profile_name}
                </Text>
                <View>
                  <Caption
                    style={{
                      fontSize: 20,
                    }}
                  >
                    {detail.xprofile.groups[1].fields[1] == undefined
                      ? ""
                      : detail.xprofile.groups[1].fields[1].value.raw}{" "}
                    {detail.xprofile.groups[1].fields[2] == undefined
                      ? ""
                      : detail.xprofile.groups[1].fields[2].value.raw}
                  </Caption>
                </View>
              </View>
              {!isOwned && (
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-evenly",
                    marginTop: 10,
                    paddingHorizontal: 10,
                  }}
                >
                  <TouchableOpacity
                    onPress={postAddFriend}
                    disabled={frLoading}
                    style={{
                      padding: 10,
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <IconButton
                      icon="account-outline"
                      color={
                        friendshipStatus == "Add Friend"
                          ? Colors.PRIMARY_COLOR
                          : "white"
                      }
                      style={{
                        backgroundColor:
                          friendshipStatus == "Add Friend"
                            ? Colors.WHITE02
                            : friendshipStatus == "Friends"
                            ? Colors.ACCENT_COLOR
                            : Colors.GTBLUE,
                      }}
                      size={40}
                      disabled={frLoading}
                      onPress={postAddFriend}
                    />
                    <Paragraph style={{ fontWeight: "bold", fontSize: 16 }}>
                      {friendshipStatus}
                    </Paragraph>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={postFollowing}
                    disabled={frLoading}
                    style={{
                      padding: 10,
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <IconButton
                      disabled={frLoading}
                      icon="swap-horizontal"
                      color={following ? "white" : Colors.PRIMARY_COLOR}
                      style={{
                        backgroundColor: following
                          ? Colors.ACCENT_COLOR
                          : Colors.WHITE02,
                      }}
                      size={40}
                      onPress={postFollowing}
                    />
                    <Paragraph style={{ fontWeight: "bold", fontSize: 16 }}>
                      {following ? "Following" : "Follow"}
                    </Paragraph>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() =>
                      navigation.push("ChatScreen", { userData: detail })
                    }
                    /* onPress={() => console.log(currentUser.id, detail.id)} */
                    disabled={!isFriend}
                    style={{
                      padding: 10,
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <IconButton
                      disabled={!isFriend}
                      icon="inbox-outline"
                      color={isFriend ? "white" : Colors.GRAY500}
                      style={{
                        backgroundColor: isFriend
                          ? Colors.ACCENT_COLOR
                          : Colors.WHITE02,
                      }}
                      size={40}
                      onPress={() =>
                        navigation.push("ChatScreen", { userData: detail })
                      }
                      /* onPress={() => console.log(currentUser.id, detail.id)} */
                    />
                    <Paragraph
                      style={{
                        fontWeight: "bold",
                        fontSize: 16,
                        color: isFriend ? Colors.PRIMARY_COLOR : Colors.GRAY400,
                      }}
                    >
                      Message
                    </Paragraph>
                  </TouchableOpacity>
                </View>
              )}
              <View style={styles.followers}>
                <Paragraph style={{ color: Colors.WHITE04, fontSize: 16 }}>
                  <Paragraph
                    style={{
                      fontWeight: "bold",
                      color: Colors.WHITE04,
                      fontSize: 16,
                    }}
                  >{`${detail?.followers}`}</Paragraph>{" "}
                  Followers
                </Paragraph>
                <Paragraph
                  style={{
                    color: Colors.WHITE04,
                    marginHorizontal: 10,
                    fontSize: 16,
                  }}
                >
                  |
                </Paragraph>
                <Paragraph style={{ color: Colors.WHITE04, fontSize: 16 }}>
                  <Paragraph
                    style={{
                      fontWeight: "bold",
                      color: Colors.WHITE04,
                      fontSize: 16,
                    }}
                  >{`${detail?.following}`}</Paragraph>{" "}
                  Following
                </Paragraph>
              </View>
            </Surface>
          </View>
          <View style={{ flex: 1 }}>
            <SwiperPagerButton
              userDetail={detail}
              isOwned={isOwned}
              navigation={navigation}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  } else {
    return <LoadingProfile />;
  }
};

const tagsStyles = {
  p: {
    margin: 0,
    padding: 0,
    fontSize: 16,
    textAlign: "justify",
  },
};

export default ProfileScreen;

const styles = StyleSheet.create({
  HeadTitle: {
    paddingVertical: 10,
    fontSize: 18,
    color: Colors.BLUE02,
    fontWeight: "bold",
  },
  detailContent: {
    fontSize: 16,
    color: Colors.PRIMARY_COLOR,
    marginBottom: 5,
    marginLeft: 15,
  },
  detailTxt: {
    marginBottom: 7,
    color: Colors.PRIMARY_COLOR_LIGHTER,
  },
  followers: {
    flexDirection: "row",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
    padding: 10,
    marginVertical: 5,
  },
  choices: {
    paddingVertical: 5,
  },
  fab: {
    position: "absolute",
    margin: 16,
    right: 0,
    bottom: 0,
    zIndex: 999,
  },
});
