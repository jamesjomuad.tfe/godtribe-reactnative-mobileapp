import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Platform,
  ScrollView,
  RefreshControl,
} from "react-native";
import { Caption, Searchbar, Subheading } from "react-native-paper";
import { GET_SEARCH_ACTIVITIES, GET_SEARCH_MEMBERS } from "../../Common/api";
import ArrowLeft from "../../Common/Components/ArrowLeft";
import PostCard from "../../Common/Components/PostCard";
import FriendCard from "../../Common/Components/FriendCard";
import MemberCard from "../../Common/Components/MemberCard";
import * as Colors from "../../Common/Styles/Colors";

const SearchScreen = ({ navigation }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [searchQuery, setSearchQuery] = useState("");
  const onChangeSearch = (query) => setSearchQuery(query);
  const [displayMessage, setDisplayMessage] = useState("");

  const [members, setMembers] = useState([]);
  const [activities, setActivities] = useState([]);
  const [membersCount, setMembersCount] = useState(null);
  const [activitiesCount, setActivitiesCount] = useState(null);

  const getMembers = async (query, more) => {
    try {
      const p = more ? page : 1;
      const res = await GET_SEARCH_MEMBERS(query, p);
      /* console.log("GET_SEARCH_MEMBERS", res); */
      if (res.success) {
        if (res.success?.length > 0) {
          if (more) {
            setMembers((members) => [...members, res.success]);
          } else {
            setMembers(res.success);
          }
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getActivities = async (query, more) => {
    try {
      const p = more ? page : 1;
      const res = await GET_SEARCH_ACTIVITIES(query, p);
      /* console.log("GET_SEARCH_ACTIVITIES", res); */
      if (res.success) {
        if (res.success?.length > 0) {
          if (more) {
            setActivities((activities) => [...activities, res.success]);
          } else {
            setActivities(res.success);
          }
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    const timeOutId = setTimeout(() => setDisplayMessage(searchQuery), 500);
    return () => clearTimeout(timeOutId);
  }, [searchQuery]);

  const querySearchCall = async (msg, more) => {
    setIsLoading(true);
    try {
      await getMembers(msg, more);
      await getActivities(msg, more);
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  const refreshData = () => {
    if (displayMessage) {
      querySearchCall(displayMessage, false);
    }
  };

  useEffect(() => {
    if (displayMessage) {
      /* console.log("displayMessage", displayMessage); */
      querySearchCall(displayMessage, false);
    } else {
      setActivities([]);
      setMembers([]);
    }
  }, [displayMessage]);

  return (
    <View style={{ flex: 1 }}>
      <View
        style={{
          width: "100%",
          backgroundColor: Colors.PRIMARY_COLOR,
          paddingTop: Platform.OS === "ios" ? 50 : 0,
          alignItems: "center",
        }}
      >
        <View
          style={{
            flexDirection: "row",
            backgroundColor: "white",
            alignItems: "center",
          }}
        >
          <ArrowLeft onPress={() => navigation.goBack()} whiteBg={true} />
          <Searchbar
            placeholder="Search"
            onChangeText={onChangeSearch}
            value={searchQuery}
            style={{ flex: 1, elevation: 0 }}
          />
        </View>
      </View>
      <ScrollView
        style={{ flex: 1 }}
        refreshControl={
          <RefreshControl refreshing={isLoading} onRefresh={refreshData} />
        }
      >
        {members?.length < 1 && activities?.length < 1 && (
          <Caption style={{ alignSelf: "center", margin: 10 }}>
            NO RESULTS
          </Caption>
        )}
        {members?.length > 0 && (
          <Subheading style={{ marginLeft: 10 }}>Members</Subheading>
        )}
        {members.length > 0 &&
          members.map((member, index) => (
            <View style={{ alignItems: "center", width: "100%" }} key={index}>
              <MemberCard navigation={navigation} friend={member} />
            </View>
          ))}
        {activities?.length > 0 && (
          <Subheading style={{ marginLeft: 10 }}>Activities</Subheading>
        )}
        {activities?.length > 0 &&
          activities.map((item, index) => (
            <PostCard
              key={index}
              item={item}
              commentPress={() => navigation.push("AddComment", { item })}
              imgPress={(images) =>
                navigation.push("ImageScreen", {
                  images: images,
                  item,
                })
              }
              hidden={true}
              /* onDelete={() => console.log("handleRefresh")} */
              viewProfile={(id) => navigation.push("Profile", { id })}
              editPress={() => navigation.push("AddPost", { item })}
              /* feedLoad={() => console.log("setIsRefresh")} */
            />
          ))}
        <View style={{ margin: 50 }}></View>
      </ScrollView>
    </View>
  );
};

export default SearchScreen;

const styles = StyleSheet.create({});
