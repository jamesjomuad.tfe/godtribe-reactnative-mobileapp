import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  KeyboardAvoidingView,
  FlatList,
  RefreshControl,
  Platform,
} from "react-native";
import MessageCard from "../../Common/Components/MessageCard";
import {
  Subheading,
  TextInput,
  IconButton,
  Button,
  Paragraph,
} from "react-native-paper";
import * as Colors from "../../Common/Styles/Colors";
import { useSelector } from "react-redux";
import {
  GET_MESSAGES,
  POST_MESSAGE,
  GET_SEARCH_THREAD,
  POST_NEW_THREAD,
} from "../../Common/api";

var RefreshVar;

const ChatScreen = ({ navigation, route }) => {
  const currentUser = useSelector((state) => state.userState.currentUser);

  const pChat = route.params?.chat;
  const pName = route.params?.name;
  const pUserData = route.params?.userData;

  const [chat, setChat] = useState(pChat);
  const [name, setName] = useState(pName);
  const [userData, setUserData] = useState(pUserData);

  /* let msgs = [...messages]; */
  /* console.log("Chat ID: ", chat.id); */

  const [chats, setChats] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [message, setMessage] = useState(null);
  const [toSend, setToSend] = useState([]);
  const [chatID, setChatID] = useState(null);

  const [isNew, setIsNew] = useState(false);
  const [auto, setAuto] = useState(false);

  const getMessages = async () => {
    try {
      const res = await GET_MESSAGES(chatID);
      if (res.success) {
        setChats(res.success.messages.slice(0).reverse());
      }
    } catch (error) {}
  };

  const forceGetMessage = async (id) => {
    try {
      const res = await GET_MESSAGES(id);
      if (res.success) {
        setChats(res.success.messages.slice(0).reverse());
      }
    } catch (error) {}
  };

  /* const setMessages = async () => {
    setIsLoading(true);
    try {
      await getMessages();
    } catch (error) {}
    setIsLoading(false);
  }; */

  const pushChat = async (msg) => {
    if (msg && currentUser?.id) {
      if (isNew) {
        if (userData) {
          setMessage(null);
          console.log("post new message thread");
          const res = await POST_NEW_THREAD(
            msg,
            [currentUser.id, userData.id],
            currentUser.id
          );
          console.log(res.success);
          if (res.success?.id) {
            setChat(res.success);
            await forceGetMessage(res.success?.id);
          }
        }
      }
      if (chatID) {
        console.log("send message");
        try {
          setMessage(null);
          const res = await POST_MESSAGE({
            id: chatID,
            message: msg,
            sender_id: currentUser.id,
          });

          if (res.success) {
            /* console.log(res); */
            await forceGetMessage(chatID);
          }

          /* var index = toSend.indexOf({ message: msg });
          var array = toSend.slice(0).splice(index, 1);
          if (counter < 1) {
            setToSend([]);
          } else {
            setToSend(array);
          }
          counter--; */
        } catch (error) {
          console.log(error);
        }
      }
    }
  };

  const sendChat = async () => {
    if (message) {
      console.log("sendChat:", message);
      /* counter++;
      var arr = [...toSend, { message: message }];
      setToSend(arr); */
      /* pushChat(message, arr); */
      pushChat(message);
    }
  };

  function autoRefresh() {
    var tme = Math.floor(Math.random() * 101) + 500;
    RefreshVar = setInterval(getMessages, tme);
  }

  function stopRefresh() {
    console.log("stop fetching chats, id:", chat?.id);
    console.log("stopping refresh id:", RefreshVar);
    clearInterval(RefreshVar);
  }

  useEffect(() => {
    if (chatID) {
      forceGetMessage(chatID);
      if (RefreshVar === undefined) {
        autoRefresh();
        setAuto(true);
      }
    }
  }, [chatID]);

  const startFromInbox = () => {
    if (chat) {
      setIsNew(false);
      var msgs = chat?.messages;
      if (msgs) {
        setChats(msgs.slice(0).reverse());
      }
      setChatID(chat.id);
    }
  };

  const searchThread = async (user_id, recipient_id) => {
    try {
      if (user_id && recipient_id) {
        console.log("chatScreen - searchThread", user_id, recipient_id);
        const res = await GET_SEARCH_THREAD(user_id, recipient_id);
        /* console.log(res); */
        if (res.success?.id) {
          console.log("chatScreen - searchThread", res.success?.id);
          setChat(res.success);
        } else {
          setChatID(null);
          setIsNew(true);
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  const startFromIProfile = () => {
    if (userData) {
      searchThread(currentUser.id, userData.id);
    }
  };

  useEffect(() => {
    if (chat) {
      startFromInbox();
    }
  }, [chat]);

  useEffect(() => {
    /* setMessages(); */

    if (userData) {
      startFromIProfile();
    } else {
      startFromInbox();
    }

    navigation.setOptions({
      headerTitle: () => chatHeader(),
    });

    return () => {
      stopRefresh();
    };
  }, []);

  const chatHeader = () => {
    return (
      <View>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "flex-start",
            marginLeft: -25,
          }}
        >
          {userData && userData?.avatar_urls ? (
            <Image
              style={styles.profileImage}
              source={{ uri: userData?.avatar_urls?.full }}
            />
          ) : chat?.avatar[0].full ? (
            <Image
              style={styles.profileImage}
              source={{ uri: chat.avatar[0].full }}
            />
          ) : (
            <Image
              source={require("../../../assets/default-img.jpg")}
              style={styles.profileImage}
            />
          )}
          <Subheading style={styles.chatNameTwo}>
            {name || userData?.profile_name}
          </Subheading>
        </View>
      </View>
    );
  };

  const renderRow = ({ item }) => {
    return <MessageCard messageData={item} usersData={chat.recipients} />;
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "margin"}
      keyboardVerticalOffset={Platform.OS == "ios" ? 95 : 0}
      style={{ flex: 1, marginBottom: 10 }}
    >
      {isNew && userData && (
        <Paragraph
          style={{
            margin: 50,
            alignSelf: "center",
            textAlign: "center",
            color: Colors.GRAY400,
          }}
        >
          Say something to start new conversation!
        </Paragraph>
      )}
      <FlatList
        nestedScrollEnable={true}
        data={chats}
        renderItem={renderRow}
        keyExtractor={(item, index) => index.toString()}
        inverted
        /* onEndReached={() => {
              !isLoading && handleLoadMore();
            }} */
        /* onEndReachedThreshold={0.5} */
        /* ListFooterComponent={renderFooter} */
        refreshControl={<RefreshControl refreshing={isLoading} />}
      />
      {/* {toSend.length > 0 &&
        toSend.map((data, index) => (
          <MessageCard
            key={index}
            messageData={{
              sender_id: currentUser.id,
              message: { raw: data.message },
            }}
            usersData={chat.recipients}
          />
        ))} */}
      <View style={styles.inputContainer}>
        <TextInput
          mode="outlined"
          dense={true}
          multiline={true}
          style={{ width: "82%" }}
          value={message}
          onChangeText={(text) => setMessage(text)}
        />
        <View style={{ alignItems: "center" }}>
          <Button
            labelStyle={Colors.ACCENT_COLOR}
            mode="contained"
            compact={true}
            style={{
              width: "100%",
              height: 46,
              alignSelf: "center",
              justifyContent: "center",
            }}
            onPress={sendChat}
            disabled={message ? false : true}
          >
            SEND
          </Button>
        </View>
      </View>
    </KeyboardAvoidingView>
  );
};

export default ChatScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15,
  },
  inputContainer: {
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-around",
    paddingHorizontal: 5,
    marginBottom: Platform.OS === "ios" ? 10 : 0,
  },
  leftItem: {
    flexDirection: "row",
    justifyContent: "center",
    flex: 8,
  },
  profileImage: {
    width: 40,
    height: 40,
    resizeMode: "cover",
    borderColor: Colors.PRIMARY_COLOR_DARK,
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 5,
  },
  chatDetails: {
    paddingLeft: 10,
    justifyContent: "center",
    width: "100%",
  },
  chatName: {
    color: "white",
    fontWeight: "bold",
    margin: 0,
    textAlignVertical: "center",
    fontSize: 18,
  },
  chatNameTwo: {
    color: "white",
    fontWeight: "bold",
    margin: 0,
    textAlignVertical: "center",
    fontSize: 18,
    paddingLeft: 10,
  },
  headerContainer: {
    justifyContent: "center",
  },
  profileHolder: {
    width: 50,
    justifyContent: "center",
    alignItems: "center",
  },
});
