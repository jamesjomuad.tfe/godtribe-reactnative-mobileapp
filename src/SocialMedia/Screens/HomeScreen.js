import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  RefreshControl,
  Image,
  Dimensions,
  SafeAreaView,
  Button,
} from "react-native";

import { ActivityIndicator, Modal, Portal, FAB } from "react-native-paper";
import { useSelector, useDispatch } from "react-redux";

import * as Colors from "../../Common/Styles/Colors";
import { GET_ACTIVITIES_BUDDYBOSS } from "../../Common/api";
/* import Carousel from 'react-native-looped-carousel-improved'; */
const { width, height } = Dimensions.get("window");

import PostCard from "../../Common/Components/PostCard";
import AddPostCard from "../../Common/Components/AddPostCard";
/* import { fetchUserData } from "../../Common/Redux/Actions"; */
import { windowHeight } from "../../Common/Utils/Dimentions";
import HeadSpacer from "../../Common/Components/HeadSpacer";
import Header from "../../Common/Components/Header";
import LoadingPlaceholder from "../../Common/Components/LoadingPlaceholder";
import { FETCH_USER_DATA } from "../../Common/Redux/Actions/user";

const HomeScreen = ({ navigation }) => {
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.userState.currentUser);
  const auth = useSelector((state) => state.authState.currentAuth);
  const postState = useSelector((state) => state.userState.posts);
  const deletingState = useSelector((state) => state.userState.deleting);

  const [posts, setPosts] = useState(null);
  const [page, setPage] = useState(2);
  const [isLoading, setIsLoading] = useState(false);
  const [isRefresh, setIsRefresh] = useState(false);
  const [momentum, setMomentum] = useState(true);
  const [visible, setVisible] = useState(false);
  const [imgURL, setImgURL] = useState();

  const [yAxis, setYAxis] = useState(0);

  const showModal = (url) => {
    setImgURL(url);
    setVisible(true);
  };
  const hideModal = () => setVisible(false);
  const containerStyle = {
    marginHorizontal: 20,
    marginVertical: 40,
    flexDirection: "row",
    backgroundColor: "red",
  };

  const headerComponent = () => {
    return (
      currentUser && (
        <AddPostCard
          user={currentUser}
          addPost={() => navigation.navigate("AddPost")}
          viewProfile={() => navigation.push("Profile")}
        />
      )
    );
  };
  const renderRow = ({ item }) => {
    return (
      <PostCard
        item={item}
        commentPress={() => navigation.push("AddComment", { item })}
        imgPress={(images) =>
          navigation.push("ImageScreen", { images: images, item })
        }
        onDelete={handleRefresh}
        viewProfile={(id) => navigation.push("Profile", { id })}
        editPress={() => navigation.push("AddPost", { item })}
        feedLoad={setIsRefresh}
      />
    );
  };

  const renderFooter = () => {
    return isLoading ? (
      <View style={styles.loader}>
        <ActivityIndicator
          size="small"
          animating={true}
          color={Colors.ACCENT_COLOR}
          style={{ padding: 5, marginBottom: 10 }}
        />
      </View>
    ) : posts ? (
      <Text
        style={{
          alignSelf: "center",
          textAlign: "center",
          color: Colors.WHITE01,
          padding: 5,
          marginBottom: 10,
        }}
      >
        End of Posts ...
      </Text>
    ) : (
      <LoadingPlaceholder />
    );
  };

  const handleLoadMore = async () => {
    /* if (!momentum) {
      console.log("\n[HomeScreen] handlemore NOT MOMENTOM!\n");
      try {
        setPage(page + 1);
        setIsLoading(true);
        let acts = await GET_ACTIVITIES_BUDDYBOSS(page, auth);
        setPosts([...posts, ...acts]);
        setMomentum(true);
        setIsRefresh(false);
        setMomentum(true);
      } catch (error) {
        console.log("[HomeScreen] handlemore error", error);
      }
    } else {
      console.log("\n[HomeScreen] handlemore MOMENTOM!\n");
      try {
        setPage(2);
        let acts = await GET_ACTIVITIES_BUDDYBOSS(2, auth);
        setPosts(acts);
        setIsRefresh(false);
      } catch (error) {
        console.log("[HomeScreen] handlemore 2 error", error);
      }
    } */
    /* setIsLoading(true);
    try {
      setPage(page + 1);

      let acts = await GET_ACTIVITIES_BUDDYBOSS(page, auth);
      setPosts([...posts, ...acts]);
      setIsRefresh(false);
    } catch (error) {
      setIsRefresh(false);
      console.log("[HomeScreen] handlemore error", error);
    } */
    setIsLoading(true);
    try {
      let acts = await GET_ACTIVITIES_BUDDYBOSS(page, auth);
      let newActs = posts.concat(acts);
      setPosts(newActs);
      setIsLoading(false);
      setPage(page + 1);
    } catch (error) {
      setIsLoading(false);
      console.log("[HomeScreen] handlemore error", error);
    }
  };

  const handleRefresh = () => {
    const refresh = async () => {
      setIsRefresh(true);
      try {
        setPage(2);
        let acts = await GET_ACTIVITIES_BUDDYBOSS(1, auth);
        setPosts([]);

        setPosts(acts);
      } catch (error) {
        console.log("[HomeScreen] handlemore 2 error", error);
      }
      setIsRefresh(false);
    };

    refresh();
  };

  useEffect(() => {
    handleRefresh();
  }, [postState]);

  useEffect(() => {
    handleRefresh();
  }, [deletingState]);

  useEffect(() => {
    if (currentUser.id) {
      dispatch(FETCH_USER_DATA(currentUser.id));
    }
    handleRefresh();
  }, []);

  return (
    <View
      style={{
        flex: 1,
        paddingTop: Platform.OS === "ios" ? 50 : 0,
        backgroundColor: Colors.PRIMARY_COLOR,
      }}
    >
      <View style={{ backgroundColor: Colors.WHITE03, flex: 1 }}>
        <HeadSpacer />
        <View
          style={{ position: "absolute", top: 0, zIndex: 999, width: "100%" }}
        >
          <Header
            screen={"Home"}
            navigateTo={(section) => navigation.jumpTo(section)}
            toProfile={() => navigation.navigate("Profile")}
            navigation={navigation}
          />
        </View>
        <Portal>
          <Modal
            transparent={false}
            visible={visible}
            onDismiss={hideModal}
            contentContainerStyle={containerStyle}
          >
            <Image
              source={{ uri: imgURL }}
              style={{
                resizeMode: "contain",
                flex: 1,
                aspectRatio: 0.7,
              }}
            />
          </Modal>
        </Portal>
        <FlatList
          initialNumToRender={10}
          nestedScrollEnable={true}
          data={posts}
          ListHeaderComponent={headerComponent}
          renderItem={renderRow}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          /* onRefresh={handleRefresh} */
          /* onEndReached={() => {
            !isRefresh && handleLoadMore();
          }} */

          onScroll={(e) => {
            setYAxis(parseInt(e.nativeEvent.contentOffset.y));
          }}
          onEndReached={() => {
            !isRefresh && handleLoadMore();
          }}
          onEndReachedThreshold={0.5}
          ListFooterComponent={renderFooter}
          /* onMomentumScrollBegin={() => setMomentum(false)} */

          refreshControl={
            <RefreshControl refreshing={isRefresh} onRefresh={handleRefresh} />
          }
        />
      </View>
      {yAxis > 0 && (
        <FAB
          style={styles.fab}
          icon="pencil"
          onPress={() => navigation.navigate("AddPost")}
        />
      )}
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  fab: {
    position: "absolute",
    margin: 16,
    right: 0,
    bottom: 0,
  },
});
