import "react-native-gesture-handler";

import React from "react";
import { StyleSheet, Text, View, LogBox } from "react-native";
import Providers from "./src/Authentication/Navigation";

/* console.ignoredYellowBox = ["Warning: Each", "Warning: Failed"]; */
LogBox.ignoreLogs([
  "Warning: Can't perform a React state update on an unmounted component.",
]);

export default function App() {
  return <Providers />;
}
